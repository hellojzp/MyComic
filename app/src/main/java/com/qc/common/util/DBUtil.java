package com.qc.common.util;

import android.content.Context;

import com.qc.common.MyBaseApplication;
import com.qc.common.en.data.Data;

import org.litepal.LitePal;
import org.litepal.LitePalApplication;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import top.luqichuang.common.model.Entity;
import top.luqichuang.common.model.EntityInfo;
import top.luqichuang.common.model.Source;
import top.luqichuang.common.util.DateUtil;
import top.luqichuang.mycomic.model.Comic;
import top.luqichuang.mycomic.model.ComicInfo;
import top.luqichuang.mynovel.model.Novel;
import top.luqichuang.mynovel.model.NovelInfo;
import top.luqichuang.myvideo.model.Video;
import top.luqichuang.myvideo.model.VideoInfo;

/**
 * @author LuQiChuang
 * @desc
 * @date 2021/6/9 19:07
 * @ver 1.0
 */
public class DBUtil {

    public static final String TAG = "DBUtil";

    public static final int SAVE_ONLY = 0;
    public static final int SAVE_CUR = 1;
    public static final int SAVE_ALL = 2;

    public static final int STATUS_HIS = 0;
    public static final int STATUS_FAV = 1;
    public static final int STATUS_ALL = 2;

    private static final ExecutorService POOL = Executors.newFixedThreadPool(3);

    private static void init() {
        try {
            LitePalApplication.getContext();
        } catch (Exception e) {
            LitePal.initialize(MyBaseApplication.getInstance());
        }
    }

    public static void createDirs(String path) {
        createDirs(new File(path));
    }

    public static void createDirs(File file) {
        if (file != null && !file.exists()) {
            file.mkdirs();
        }
    }

    public static File createFile(String path) {
        return createFile(new File(path));
    }

    private static File createFile(String path, String fileName) {
        File file = new File(path, fileName);
        return createFile(file);
    }

    public static File createFile(File file) {
        try {
            if (file != null) {
                createDirs(file.getParentFile());
                if (!file.exists()) {
                    file.createNewFile();
                }
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return file;
    }

    public static void saveOnly(Entity entity) {
        save(entity, SAVE_ONLY);
    }

    public static void saveCur(Entity entity) {
        save(entity, SAVE_CUR);
    }

    public static void saveAll(Entity entity) {
        save(entity, SAVE_ALL);
    }

    public static void save(Entity entity, int mode) {
        if (entity != null) {
            saveEntityData(entity);
            if (mode == SAVE_CUR) {
                saveInfoData(entity.getInfo());
            } else if (mode == SAVE_ALL) {
                for (EntityInfo info : entity.getInfoList()) {
                    saveInfoData(info);
                }
            }
        }
    }

    public static void saveEntityData(Entity entity) {
        if (entity != null) {
            init();
            POOL.execute(() -> {
                if (entity.getTitle() == null) {
                    if (entity.getInfo() != null && entity.getInfo().getTitle() != null) {
                        entity.setTitle(entity.getInfo().getTitle());
                        entity.saveOrUpdate("title = ?", entity.getTitle());
                    }
                } else {
                    entity.saveOrUpdate("title = ?", entity.getTitle());
                }
            });
        }
    }

    public static void saveInfoData(EntityInfo info) {
        if (info != null) {
            init();
            POOL.execute(() -> {
                if (Data.contentCode == Data.COMIC_CODE || Data.contentCode == Data.VIDEO_CODE) {
                    if (info.getTitle() != null) {
                        info.saveOrUpdate("title = ? and sourceId = ?", info.getTitle(), String.valueOf(info.getSourceId()));
                    }
                } else if (Data.contentCode == Data.NOVEL_CODE) {
                    if (info.getTitle() != null && info.getAuthor() != null) {
                        info.saveOrUpdate("title = ? and nSourceId = ? and author = ?", info.getTitle(), String.valueOf(info.getSourceId()), info.getAuthor());
                    }
                }
            });
        }
    }

    public static void deleteData(Entity entity) {
        if (entity != null) {
            init();
            POOL.execute(() -> {
                entity.delete();
                for (EntityInfo info : entity.getInfoList()) {
                    File file = new File(ImageUtil.getLocalImgUrl(info.getId()));
                    if (file.exists()) {
                        file.delete();
                    }
                }
            });
        }
    }

    public static List<? extends Entity> findListByStatus(int status) {
        String order = "priority DESC, date DESC";
        init();
        List<? extends Entity> list;
        if (Data.contentCode == Data.COMIC_CODE) {
            if (status == EntityUtil.STATUS_ALL) {
                list = LitePal.order(order).find(Comic.class);
            } else {
                list = LitePal.where("status = ?", String.valueOf(status)).order(order).find(Comic.class);
            }
        } else if (Data.contentCode == Data.NOVEL_CODE) {
            if (status == EntityUtil.STATUS_ALL) {
                list = LitePal.order(order).find(Novel.class);
            } else {
                list = LitePal.where("status = ?", String.valueOf(status)).order(order).find(Novel.class);
            }
        } else {
            if (status == EntityUtil.STATUS_ALL) {
                list = LitePal.order(order).find(Video.class);
            } else {
                list = LitePal.where("status = ?", String.valueOf(status)).order(order).find(Video.class);
            }
        }
        List<Entity> dList = new ArrayList<>();
        for (Entity entity : list) {
            List<? extends EntityInfo> infoList;
            if (Data.contentCode == Data.COMIC_CODE) {
                infoList = LitePal.where("title = ?", entity.getTitle()).find(ComicInfo.class);
            } else if (Data.contentCode == Data.NOVEL_CODE) {
                infoList = LitePal.where("title = ?", entity.getTitle()).find(NovelInfo.class);
            } else {
                infoList = LitePal.where("title = ?", entity.getTitle()).find(VideoInfo.class);
            }
            for (EntityInfo info : infoList) {
                Source<EntityInfo> source = SourceUtil.getSource(info.getSourceId());
                if (!SourceUtil.getSourceList().contains(source)) {
                    source = null;
                }
                if (source != null && source.isValid()) {
                    EntityUtil.addInfo(entity, info);
                    if (entity.getSourceId() == info.getSourceId()) {
                        entity.setInfo(info);
                    }
                    //更改detailUrl
                    String url = info.getDetailUrl();
                    String index = source.getIndex();
                    if (!url.startsWith(index)) {
                        String tmp = url.substring(url.indexOf('/', url.indexOf('.')));
                        url = index + tmp;
                        info.setDetailUrl(url);
                        saveInfoData(info);
                    }
                    //end
                }
            }
            if (entity.getInfo() == null) {
                if (entity.getInfoList().isEmpty()) {
                    dList.add(entity);
                } else {
                    entity.setInfo(entity.getInfoList().get(0));
                    entity.setSourceId(entity.getInfoList().get(0).getSourceId());
                }
            }
        }
        if (dList.size() > 0) {
            list.removeAll(dList);
        }
        return list;
    }

    public static void autoBackup(Context context) {
        if (!existAuto()) {
            backupData(context, getAutoName());
        }
        deleteAuto();
    }

    public static String getAutoName() {
        return Data.getAutoSavePath() + "/自动备份#" + DateUtil.formatAutoBackup(new Date());
    }

    public static boolean existAuto() {
        return new File(getAutoName()).exists();
    }

    public static boolean deleteAuto() {
        try {
            File file = new File(Data.getAutoSavePath());
            File[] files = file.listFiles();
            while (files.length > 5) {
                int old = 0;
                long oldVal = files[0].lastModified();
                for (int i = 1; i < files.length; i++) {
                    File f = files[i];
                    if (f.lastModified() < oldVal) {
                        old = i;
                        oldVal = f.lastModified();
                    }
                }
                files[old].delete();
                files = file.listFiles();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public static boolean deleteShelfImg() {
        String path = Data.getImgPath();
        File file = new File(path);
        if (file.exists()) {
            return file.delete();
        }
        return false;
    }

    public static boolean backupData(Context context) {
        return backupData(context, Data.getSavePathName());
    }

    public static boolean backupData(Context context, String path) {
        File dbFile = context.getDatabasePath(Data.DB_FILE_NAME);
        File backupFile = createFile(path);
        fileCopy(dbFile, backupFile);
        return true;
    }

    public static boolean restoreData(Context context) {
        return restoreData(context, Data.getSavePathName());
    }

    public static boolean restoreData(Context context, String path) {
        File dbFile = context.getDatabasePath(Data.DB_FILE_NAME);
        File backupFile = createFile(path);
        File tmpFile = createFile(Data.getAppPath(), "tmp.db");
        fileCopy(dbFile, tmpFile);
        try {
            fileCopy(backupFile, dbFile);
            EntityUtil.initEntityList(EntityUtil.STATUS_ALL);
            tmpFile.delete();
        } catch (Exception e) {
            LitePal.getDatabase().close();
            fileCopy(tmpFile, dbFile);
            tmpFile.delete();
            return false;
        }
        return true;
    }

    private static void fileCopy(File oFile, File toFile) {
        try (FileChannel inChannel = new FileInputStream(oFile).getChannel(); FileChannel outChannel = new FileOutputStream(toFile).getChannel()) {
            inChannel.transferTo(0, inChannel.size(), outChannel);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
