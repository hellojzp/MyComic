package com.qc.common.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.PixelFormat;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.load.model.GlideUrl;
import com.bumptech.glide.load.model.Headers;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.CustomTarget;
import com.bumptech.glide.request.target.Target;
import com.bumptech.glide.request.transition.Transition;
import com.qc.common.MyBaseApplication;
import com.qc.common.en.data.Data;
import com.qc.common.self.ImageConfig;
import com.qc.mycomic.R;
import com.qmuiteam.qmui.util.QMUIDisplayHelper;
import com.qmuiteam.qmui.widget.QMUIProgressBar;

import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.Set;

import io.reactivex.android.schedulers.AndroidSchedulers;
import the.one.base.Interface.GlideProgressListener;
import the.one.base.util.glide.GlideProgressInterceptor;
import top.luqichuang.common.model.Content;
import top.luqichuang.common.model.Entity;

/**
 * @author 18472
 * @desc
 * @date 2024/2/25 10:30
 * @ver 1.0
 */
public class ImageUtil {

    private static final Map<String, Integer> STATE_MAP = new HashMap<>();
    private static final Map<String, Integer> PROGRESS_MAP = new HashMap<>();
    private static final Set<String> CACHE_SET = new HashSet<>();
    private static final Queue<ImageConfig> PRELOAD_QUEUE = new LinkedList<>();

    private static final int CACHE_NUM = 5;

    public static final int LOAD_ING = 1;
    public static final int LOAD_SUCCESS = 2;
    public static final int LOAD_FAIL = 3;

    private static final int MATCH_PARENT = ViewGroup.LayoutParams.MATCH_PARENT;
    private static final int WRAP_CONTENT = ViewGroup.LayoutParams.WRAP_CONTENT;

    public static ImageConfig getDefaultConfig(Context context, String url, RelativeLayout layout) {
        ImageConfig config = new ImageConfig(url, layout);
        config.setDefaultBitmapId(R.drawable.ic_image_none);
        config.setErrorBitmapId(R.drawable.ic_image_none);
        config.setDrawableId(R.drawable.ic_image_background);
        config.setScaleType(ImageView.ScaleType.FIT_XY);
        config.setWidth(MATCH_PARENT);
        config.setHeight(MATCH_PARENT);
        return config;
    }

    public static ImageConfig getReaderConfig(Context context, String url, RelativeLayout layout) {
        ImageConfig config = new ImageConfig(url, layout);
        config.setDefaultBitmapId(0);
        config.setErrorBitmapId(R.drawable.ic_image_error_24);
        config.setDrawableId(R.drawable.ic_image_reader_background);
        config.setScaleType(ImageView.ScaleType.CENTER);
        config.setWidth(getScreenWidth(context));
        config.setHeight(QMUIDisplayHelper.dp2px(context, 300));
        return config;
    }

    private static boolean initLayout(ImageConfig config) {
        RelativeLayout layout = config.getLayout();
        if (layout != null) {
            config.setImageView(layout.findViewById(R.id.imageView));
            config.setProgressBar(layout.findViewById(R.id.progressBar));
            config.setTextView(layout.findViewById(R.id.textView));
            if (config.getImageView() != null) {
                config.getImageView().setTag(config.getUrl());
                config.getProgressBar().setTag(config.getUrl());
                config.getTextView().setTag(config.getUrl());
                config.getProgressBar().setVisibility(View.INVISIBLE);
                config.getTextView().setVisibility(View.INVISIBLE);
                return true;
            }
        }
        return false;
    }

    public static void loadImage(Context context, ImageConfig config) {
        if (!initLayout(config)) {
            return;
        }
        if (Glide.with(context).isPaused()) {
            Glide.with(context).onStart();
        }
        if (config.getUrl() == null || config.getUrl().isEmpty()) {
            setLP(config.getLayout(), config.getWidth(), config.getHeight());
            setLP(config.getImageView(), config.getWidth(), config.getHeight());
            config.getImageView().setImageBitmap(getBitmap(context, config.getErrorBitmapId()));
            config.getImageView().setScaleType(config.getScaleType());
            setProgress(config, 100);
            return;
        }
        if (!config.isForce() && config.isSave() && loadImageLocal(config)) {
            return;
        }
        loadImageNet(context, config);
    }

    public static boolean loadImageLocal(ImageConfig config) {
        if (config.getSaveKey() != null) {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inPreferredConfig = Bitmap.Config.RGB_565;
            File file = new File(getLocalImgUrl(config.getSaveKey()));
            if (file.exists()) {
                Bitmap bitmap = BitmapFactory.decodeFile(file.getPath(), options);
                if (bitmap != null) {
                    config.getImageView().setImageBitmap(bitmap);
                    return true;
                }
            }
        }
        return false;
    }

    public static void loadImageNet(Context context, ImageConfig config) {
        String url = config.getUrl();
        GlideUrl glideUrl = getGlideUrl(config);
        addInterceptor(config);

        ImageView imageView = config.getImageView();

        Glide.with(context)
                .asBitmap()
                .load(glideUrl)
                .dontAnimate()
                .priority(Priority.HIGH)
                .addListener(getRequestListener(context, config))
                .into(new CustomTarget<Bitmap>() {
                    @Override
                    public void onLoadStarted(@Nullable Drawable placeholder) {
                        if (url.equals(imageView.getTag())) {
                            if (config.getDefaultBitmapId() != 0) {
                                imageView.setImageBitmap(getBitmap(context, config.getDefaultBitmapId()));
                                imageView.setScaleType(ImageView.ScaleType.FIT_XY);
                            } else {
                                imageView.setImageBitmap(null);
                                imageView.setBackground(getDrawable(context, config.getDrawableId()));
                                imageView.setScaleType(config.getScaleType());
                            }
                            setLP(config.getLayout(), config.getWidth(), config.getHeight());
                            setLP(imageView, config.getWidth(), config.getWidth());
                        }
                        setProgress(config);
                        STATE_MAP.put(url, LOAD_ING);
                    }

                    @Override
                    public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                        if (url.equals(imageView.getTag())) {
                            setLP(config.getLayout(), MATCH_PARENT, MATCH_PARENT);
                            setLP(context, imageView, resource);
                            imageView.setImageBitmap(resource);
                            imageView.setScaleType(ImageView.ScaleType.FIT_XY);
                        }
                        if (config.isSave() && config.getSaveKey() != null) {
                            saveBitmapBackPath(resource, config.getSaveKey());
                        }
                        setProgress(config, 100);
                        STATE_MAP.put(url, LOAD_SUCCESS);
                    }

                    @Override
                    public void onLoadFailed(@Nullable Drawable errorDrawable) {
                        if (url.equals(imageView.getTag())) {
                            setLP(config.getLayout(), config.getWidth(), config.getHeight());
                            setLP(imageView, config.getWidth(), config.getHeight());
                            imageView.setImageBitmap(getBitmap(context, config.getErrorBitmapId()));
                            imageView.setScaleType(config.getScaleType());
                        }
                        setProgress(config, 100);
                        STATE_MAP.put(url, LOAD_FAIL);
                    }

                    @Override
                    public void onLoadCleared(@Nullable Drawable placeholder) {
                        onLoadFailed(placeholder);
                    }
                });
    }

    public static void preloadImage(Context context, ImageConfig config) {
        if (config != null) {
            PRELOAD_QUEUE.offer(config);
            preloadImageStart(context);
        }
    }

    public static void clearCacheMap() {
        PROGRESS_MAP.clear();
        STATE_MAP.clear();
        CACHE_SET.clear();
        PRELOAD_QUEUE.clear();
        Glide.with(MyBaseApplication.getInstance()).pauseAllRequests();
    }

    /**
     * 根据url获得图片加载状态
     *
     * @param content content
     * @return int
     */
    public static int getLoadStatus(Content content) {
        if (content != null) {
            String url = content.getUrl();
            Integer status = STATE_MAP.get(url);
            if (status != null) {
                return status;
            }
        }
        return LOAD_ING;
    }

    public static void setSaveKey(Entity entity, ImageConfig config) {
        if (entity.getInfoId() == 0) {
            config.setSaveKey(null);
        } else {
            if (Data.contentCode == Data.COMIC_CODE) {
                config.setSaveKey("C_" + entity.getTitle() + "_" + entity.getSourceId());
            } else if (Data.contentCode == Data.NOVEL_CODE) {
                config.setSaveKey("N_" + entity.getTitle() + "_" + entity.getSourceId());
            } else {
                config.setSaveKey("V_" + entity.getTitle() + "_" + entity.getSourceId());
            }
        }
    }

    public static void pauseLoad(Context context) {
        Glide.with(context).pauseRequests();
    }

    public static void resumeLoad(Context context) {
        Glide.with(context).resumeRequests();
    }

    /*=============================================================================*/
    private static void setProgress(ImageConfig config) {
        Integer progress = PROGRESS_MAP.get(config.getUrl());
        if (progress == null) {
            progress = 0;
        }
        setProgress(config, progress, false);
    }

    private static void setProgress(ImageConfig config, int progress) {
        setProgress(config, progress, true);
    }

    private static void setProgress(ImageConfig config, int progress, boolean animated) {
        String url = config.getUrl();
        QMUIProgressBar progressBar = config.getProgressBar();
        TextView textView = config.getTextView();
        if (url.equals(progressBar.getTag())) {
            if (progress == 100) {
                progressBar.setVisibility(View.INVISIBLE);
                textView.setVisibility(View.INVISIBLE);
            } else {
                progressBar.setVisibility(View.VISIBLE);
                textView.setVisibility(View.VISIBLE);
                String text = progress + "%";
                AndroidSchedulers.mainThread().scheduleDirect(() -> {
                    progressBar.setProgress(progress, animated);
                    textView.setText(text);
                });
            }
        }
    }

    private static void addInterceptor(ImageConfig config) {
        String url = config.getUrl();
        GlideProgressInterceptor.addListener(url, new GlideProgressListener() {
            @Override
            public void onProgress(int progress, boolean success) {
                progress = Math.max(progress, 1);
                progress = Math.min(progress, 100);
                PROGRESS_MAP.put(url, progress);
                if (config.getProgressBar() == null) {
                    return;
                }
                if (url.equals(config.getProgressBar().getTag())) {
                    setProgress(config, progress);
                }
            }
        });
    }

    private static GlideUrl getGlideUrl(ImageConfig config) {
//        String url = new java.util.Random().nextBoolean() ? config.getUrl() : "error_url";
        String url = config.getUrl();
        return new GlideUrl(url, () -> {
            if (config.getHeaders() != null) {
                return config.getHeaders();
            } else {
                return Headers.DEFAULT.getHeaders();
            }
        });
    }

    private static <T> RequestListener<T> getRequestListener(Context context, ImageConfig config) {
        String url = config.getUrl();
        return new RequestListener<T>() {
            @Override
            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<T> target, boolean isFirstResource) {
                onLoadEnd();
                return false;
            }

            @Override
            public boolean onResourceReady(T resource, Object model, Target<T> target, DataSource dataSource, boolean isFirstResource) {
                onLoadEnd();
                return false;
            }

            private void onLoadEnd() {
                PROGRESS_MAP.remove(url);
                GlideProgressInterceptor.removeListener(url);
                if (!CACHE_SET.isEmpty()) {
                    CACHE_SET.remove(url);
                    PRELOAD_QUEUE.remove(config);
                    preloadImageStart(context);
                }
            }
        };
    }

    private static void preloadImageStart(Context context) {
        if (!PRELOAD_QUEUE.isEmpty() && CACHE_SET.size() < CACHE_NUM) {
            ImageConfig config = PRELOAD_QUEUE.poll();
            if (config != null) {
                CACHE_SET.add(config.getUrl());
                addInterceptor(config);
                GlideUrl glideUrl = getGlideUrl(config);
                Glide.with(context)
                        .load(glideUrl)
                        .priority(Priority.NORMAL)
                        .addListener(getRequestListener(context, config))
                        .preload();
            }
        }
    }

    /*=============================================================================*/

    private static void setLP(Context context, View view, Bitmap bitmap) {
        ViewGroup.LayoutParams lp = view.getLayoutParams();
        int bWidth = bitmap.getWidth();
        int bHeight = bitmap.getHeight();
        int sWidth = getScreenWidth(context);
        int sHeight = bHeight * sWidth / bWidth;
        lp.width = sWidth;
        lp.height = sHeight;
        view.setLayoutParams(lp);
    }

    private static void setLP(View view, int width, int height) {
        ViewGroup.LayoutParams lp = view.getLayoutParams();
        lp.width = width;
        lp.height = height;
        view.setLayoutParams(lp);
    }

    /**
     * @param context context
     * @return int
     * @desc 获取屏幕宽度
     */
    private static int getScreenWidth(Context context) {
        return QMUIDisplayHelper.getScreenWidth(context);
    }

    /**
     * 保存bitmap到本地并返回地址
     *
     * @param bm  bm
     * @param key key
     * @return String
     */
    private static String saveBitmapBackPath(Bitmap bm, Object key) {
        String path = null;
        File savedFile = DBUtil.createFile(getLocalImgUrl(key));
        BufferedOutputStream bos = null;
        try {
            bos = new BufferedOutputStream(new FileOutputStream(savedFile));
            bm.compress(Bitmap.CompressFormat.JPEG, 80, bos);
            path = savedFile.getAbsolutePath();
        } catch (Exception e) {
            savedFile.delete();
            e.printStackTrace();
        } finally {
            try {
                if (bos != null) {
                    bos.flush();
                    bos.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return path;
    }

    /*=============================================================================*/

    /**
     * 根据key获得本地图片地址
     *
     * @param key key
     * @return String
     */
    public static String getLocalImgUrl(Object key) {
        return Data.getImgPath() + "/img_" + key.toString();
    }

    /**
     * 根据id获得Drawable
     *
     * @param context    context
     * @param drawableId drawableId
     * @return Drawable
     */
    public static Drawable getDrawable(Context context, int drawableId) {
        return ContextCompat.getDrawable(context, drawableId);
    }

    /**
     * 根据id获得Bitmap
     *
     * @param context  context
     * @param bitmapId bitmapId
     * @return Bitmap
     */
    public static Bitmap getBitmap(Context context, int bitmapId) {
        return drawableToBitmap(getDrawable(context, bitmapId));
    }

    /**
     * Drawable -> Bitmap
     *
     * @param drawable drawable
     * @return Bitmap
     */
    public static Bitmap drawableToBitmap(Drawable drawable) {
        if (drawable instanceof BitmapDrawable) {
            return ((BitmapDrawable) drawable).getBitmap();
        } else {
            return drawableToBitmapByCanvas(drawable);
        }
    }

    /**
     * Bitmap -> Drawable
     *
     * @param context context
     * @param bitmap  bitmap
     * @return Drawable
     */
    public static Drawable bitmapToDrawable(Context context, Bitmap bitmap) {
        return new BitmapDrawable(context.getResources(), bitmap);
    }

    /**
     * Drawable -> Bitmap
     *
     * @param drawable drawable
     * @return Bitmap
     */
    public static Bitmap drawableToBitmapByCanvas(Drawable drawable) {
        int width = drawable.getIntrinsicWidth() > 0 ? drawable.getIntrinsicWidth() : 100;
        int height = drawable.getIntrinsicHeight() > 0 ? drawable.getIntrinsicHeight() : 100;
        Bitmap bitmap = Bitmap.createBitmap(width, height,
                drawable.getOpacity() != PixelFormat.OPAQUE ? Bitmap.Config.ARGB_8888 : Bitmap.Config.RGB_565);
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
        drawable.draw(canvas);
        return bitmap;
    }

    /**
     * byte[] -> Bitmap
     *
     * @param bytes      bytes
     * @param isCompress isCompress
     * @return Bitmap
     */
    private static Bitmap bytesToBitmap(byte[] bytes, boolean isCompress) {
        Bitmap bitmap;
        if (isCompress) {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inPreferredConfig = Bitmap.Config.RGB_565;
            options.inSampleSize = 2;
            bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length, options);
        } else {
            bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
        }
        //Log.i("TAG", "bytesToBitmap: bitmap.cSize = " + bitmap.getByteCount() / 1024 + "KB");
        return bitmap;
    }

    /**
     * 图片压缩
     *
     * @param bitmap bitmap
     * @return Bitmap
     */
    private static Bitmap compressBitmap(Bitmap bitmap) {
        int length = bitmap.getByteCount();
        if (length / 1024 > 2000) {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inPreferredConfig = Bitmap.Config.RGB_565;
            options.inSampleSize = 2;
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 80, baos);
            ByteArrayInputStream isBm = new ByteArrayInputStream(baos.toByteArray());
            bitmap = BitmapFactory.decodeStream(isBm, null, options);
        }
        return bitmap;
    }

    /**
     * 计算bitmap大小
     *
     * @param bitmap bitmap
     * @return int
     */
    public static int getBitmapSize(Bitmap bitmap) {
        return bitmap.getByteCount();
    }

}
