package com.qc.common.util;


import com.qc.common.en.data.Data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import top.luqichuang.common.en.CSourceEnum;
import top.luqichuang.common.en.NSourceEnum;
import top.luqichuang.common.en.VSourceEnum;
import top.luqichuang.common.model.Content;
import top.luqichuang.common.model.Entity;
import top.luqichuang.common.model.EntityInfo;
import top.luqichuang.common.model.Source;
import top.luqichuang.common.util.MapUtil;
import top.luqichuang.mycomic.model.Comic;
import top.luqichuang.mycomic.model.ComicInfo;
import top.luqichuang.mynovel.model.Novel;
import top.luqichuang.mynovel.model.NovelInfo;
import top.luqichuang.myvideo.model.Video;
import top.luqichuang.myvideo.model.VideoInfo;

/**
 * @author LuQiChuang
 * @desc
 * @date 2021/6/10 15:51
 * @ver 1.0
 */
public class SourceUtil {

    private static final Map<Integer, Source<? extends EntityInfo>> COMIC_MAP = CSourceEnum.getMAP();
    private static final Map<Integer, Source<? extends EntityInfo>> NOVEL_MAP = NSourceEnum.getMAP();
    private static final Map<Integer, Source<? extends EntityInfo>> VIDEO_MAP = VSourceEnum.getMAP();

    private static final Map<Integer, String> COMIC_NAME_MAP = new HashMap<>();
    private static final Map<Integer, String> NOVEL_NAME_MAP = new HashMap<>();
    private static final Map<Integer, String> VIDEO_NAME_MAP = new HashMap<>();

    private static final List<Source<? extends EntityInfo>> COMIC_SOURCE_LIST = new ArrayList<>();
    private static final List<Source<? extends EntityInfo>> NOVEL_SOURCE_LIST = new ArrayList<>();
    private static final List<Source<? extends EntityInfo>> VIDEO_SOURCE_LIST = new ArrayList<>();

    private static final List<String> COMIC_SOURCE_NAME_LIST = new ArrayList<>();
    private static final List<String> NOVEL_SOURCE_NAME_LIST = new ArrayList<>();
    private static final List<String> VIDEO_SOURCE_NAME_LIST = new ArrayList<>();

    static {
        for (Source<? extends EntityInfo> source : COMIC_MAP.values()) {
            COMIC_NAME_MAP.put(source.getSourceId(), source.getSourceName());
            COMIC_SOURCE_LIST.add(source);
            COMIC_SOURCE_NAME_LIST.add(source.getSourceName());
        }
        for (Source<? extends EntityInfo> source : NOVEL_MAP.values()) {
            NOVEL_NAME_MAP.put(source.getSourceId(), source.getSourceName());
            NOVEL_SOURCE_LIST.add(source);
            NOVEL_SOURCE_NAME_LIST.add(source.getSourceName());
        }
        for (Source<? extends EntityInfo> source : VIDEO_MAP.values()) {
            VIDEO_NAME_MAP.put(source.getSourceId(), source.getSourceName());
            VIDEO_SOURCE_LIST.add(source);
            VIDEO_SOURCE_NAME_LIST.add(source.getSourceName());
        }
    }

    public static void init() {
        COMIC_SOURCE_LIST.clear();
        NOVEL_SOURCE_LIST.clear();
        VIDEO_SOURCE_LIST.clear();
        COMIC_SOURCE_LIST.addAll(COMIC_MAP.values());
        NOVEL_SOURCE_LIST.addAll(NOVEL_MAP.values());
        VIDEO_SOURCE_LIST.addAll(VIDEO_MAP.values());
    }

    public static Source<EntityInfo> getSource(int sourceId) {
        if (Data.contentCode == Data.COMIC_CODE) {
            return (Source<EntityInfo>) COMIC_MAP.get(sourceId);
        } else if (Data.contentCode == Data.NOVEL_CODE) {
            return (Source<EntityInfo>) NOVEL_MAP.get(sourceId);
        } else {
            return (Source<EntityInfo>) VIDEO_MAP.get(sourceId);
        }
    }

    public static String getSourceName(int sourceId) {
        Source<EntityInfo> source = getSource(sourceId);
        if (source != null) {
            return source.getSourceName();
        }
        return null;
    }

    public static Integer getSourceId(String name) {
        if (Data.contentCode == Data.COMIC_CODE) {
            return MapUtil.getKeyByValue(COMIC_NAME_MAP, name);
        } else if (Data.contentCode == Data.NOVEL_CODE) {
            return MapUtil.getKeyByValue(NOVEL_NAME_MAP, name);
        } else {
            return MapUtil.getKeyByValue(VIDEO_NAME_MAP, name);
        }
    }

    public static List<Source<EntityInfo>> getSourceList() {
        if (Data.contentCode == Data.COMIC_CODE) {
            return (List<Source<EntityInfo>>) (List) COMIC_SOURCE_LIST;
        } else if (Data.contentCode == Data.NOVEL_CODE) {
            return (List<Source<EntityInfo>>) (List) NOVEL_SOURCE_LIST;
        } else {
            return (List<Source<EntityInfo>>) (List) VIDEO_SOURCE_LIST;
        }
    }

    public static List<String> getSourceNameList() {
        if (Data.contentCode == Data.COMIC_CODE) {
            return COMIC_SOURCE_NAME_LIST;
        } else if (Data.contentCode == Data.NOVEL_CODE) {
            return NOVEL_SOURCE_NAME_LIST;
        } else {
            return VIDEO_SOURCE_NAME_LIST;
        }
    }

    public static int size() {
        if (Data.contentCode == Data.COMIC_CODE) {
            return COMIC_SOURCE_LIST.size();
        } else if (Data.contentCode == Data.NOVEL_CODE) {
            return NOVEL_SOURCE_LIST.size();
        } else {
            return VIDEO_SOURCE_LIST.size();
        }
    }

    public static EntityInfo getInfo() {
        if (Data.contentCode == Data.COMIC_CODE) {
            return new ComicInfo();
        } else if (Data.contentCode == Data.NOVEL_CODE) {
            return new NovelInfo();
        } else {
            return new VideoInfo();
        }
    }

    public static Entity getEntity() {
        if (Data.contentCode == Data.COMIC_CODE) {
            return new Comic();
        } else if (Data.contentCode == Data.NOVEL_CODE) {
            return new Novel();
        } else {
            return new Video();
        }
    }

    public static Entity getEntity(EntityInfo info) {
        if (Data.contentCode == Data.COMIC_CODE) {
            return new Comic((ComicInfo) info);
        } else if (Data.contentCode == Data.NOVEL_CODE) {
            return new Novel((NovelInfo) info);
        } else {
            return new Video((VideoInfo) info);
        }
    }

    /*--------------------------------------------------------------------------------------------*/
    public static List<EntityInfo> getInfoList(Source<EntityInfo> source, String html) {
        try {
            return source.getInfoList(html);
        } catch (Exception e) {
            return new ArrayList<>();
        }
    }

    public static void setInfoDetail(Source<EntityInfo> source, EntityInfo info, String html, Map<String, Object> map) {
        try {
            source.setInfoDetail(info, html, map);
        } catch (Exception ignored) {
        }
    }

    public static List<Content> getContentList(Source<EntityInfo> source, String html, int chapterId, Map<String, Object> map) {
        try {
            return source.getContentList(html, chapterId, map);
        } catch (Exception e) {
            return new ArrayList<>();
        }
    }

    public static List<EntityInfo> getRankInfoList(Source<EntityInfo> source, String html) {
        try {
            return source.getRankInfoList(html);
        } catch (Exception e) {
            return new ArrayList<>();
        }
    }

}
