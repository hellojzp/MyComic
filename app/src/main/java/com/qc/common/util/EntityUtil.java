package com.qc.common.util;


import com.qc.common.en.SettingEnum;
import com.qc.common.en.data.Data;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import top.luqichuang.common.en.CSourceEnum;
import top.luqichuang.common.model.ChapterInfo;
import top.luqichuang.common.model.Content;
import top.luqichuang.common.model.Entity;
import top.luqichuang.common.model.EntityInfo;
import top.luqichuang.common.model.Source;
import top.luqichuang.common.util.DateUtil;

/**
 * @author LuQiChuang
 * @desc
 * @date 2021/6/9 18:51
 * @ver 1.0
 */
public class EntityUtil {

    public static final int STATUS_HIS = 0;
    public static final int STATUS_FAV = 1;
    public static final int STATUS_ALL = 2;

    private static List<Entity> hisEntityList;
    private static List<Entity> favEntityList;
    private static List<Entity> entityList;

    private static void setSourceSettingEnum(Collection<Integer> ids, SettingEnum sourceOpen, SettingEnum sourceTotal) {
        Collection<Integer> totalIds = (Collection<Integer>) SettingUtil.getSettingKey(sourceTotal);
        Collection<Integer> allIds = new HashSet<>(CSourceEnum.getMAP().keySet());
        allIds.removeAll(totalIds);
        if (!allIds.isEmpty()) {
            ids.addAll(allIds);
            SettingUtil.putSetting(sourceOpen, ids);
        }
    }

    public static List<Entity> initEntityList(int status) {
        entityList = null;
        SourceUtil.init();
        List<Source<EntityInfo>> sourceList = SourceUtil.getSourceList();
        Collection<Integer> ids;
        Collection<Integer> totalIds;
        Collection<Integer> allIds;
        if (Data.contentCode == Data.COMIC_CODE) {
            ids = new LinkedHashSet<>((Collection<Integer>) SettingUtil.getSettingKey(SettingEnum.COMIC_SOURCE_OPEN));
            totalIds = new LinkedHashSet<>((Collection<Integer>) SettingUtil.getSettingKey(SettingEnum.COMIC_SOURCE_TOTAL));
            allIds = new LinkedHashSet<>(CSourceEnum.getMAP().keySet());
            allIds.removeAll(totalIds);
            if (!allIds.isEmpty()) {
                ids.addAll(allIds);
                SettingUtil.putSetting(SettingEnum.COMIC_SOURCE_OPEN, ids);
            }
        } else if (Data.contentCode == Data.NOVEL_CODE) {
            ids = new LinkedHashSet<>((Collection<Integer>) SettingUtil.getSettingKey(SettingEnum.NOVEL_SOURCE_OPEN));
            totalIds = new LinkedHashSet<>((Collection<Integer>) SettingUtil.getSettingKey(SettingEnum.NOVEL_SOURCE_TOTAL));
            allIds = new LinkedHashSet<>(CSourceEnum.getMAP().keySet());
            allIds.removeAll(totalIds);
            if (!allIds.isEmpty()) {
                ids.addAll(allIds);
                SettingUtil.putSetting(SettingEnum.NOVEL_SOURCE_OPEN, ids);
            }
        } else {
            ids = new LinkedHashSet<>((Collection<Integer>) SettingUtil.getSettingKey(SettingEnum.VIDEO_SOURCE_OPEN));
            totalIds = new LinkedHashSet<>((Collection<Integer>) SettingUtil.getSettingKey(SettingEnum.VIDEO_SOURCE_TOTAL));
            allIds = new LinkedHashSet<>(CSourceEnum.getMAP().keySet());
            allIds.removeAll(totalIds);
            if (!allIds.isEmpty()) {
                ids.addAll(allIds);
                SettingUtil.putSetting(SettingEnum.VIDEO_SOURCE_OPEN, ids);
            }
        }
        Iterator<Source<EntityInfo>> iterator = sourceList.iterator();
        while (iterator.hasNext()) {
            int n = iterator.next().getSourceId();
            if (!ids.contains(n)) {
                iterator.remove();
            }
        }
        return getEntityList(status);
    }

    public static List<Entity> getEntityList(int status) {
        if (entityList == null) {
            entityList = (List<Entity>) DBUtil.findListByStatus(STATUS_ALL);
            hisEntityList = new ArrayList<>();
            favEntityList = new ArrayList<>();
            for (Entity entity : entityList) {
                if (entity.getStatus() == STATUS_HIS) {
                    hisEntityList.add(entity);
                }
                if (entity.getStatus() == STATUS_FAV) {
                    favEntityList.add(entity);
                }
            }
        }
        if (status == STATUS_HIS) {
            return hisEntityList;
        } else if (status == STATUS_FAV) {
            return favEntityList;
        } else {
            return entityList;
        }
    }

    public static List<Entity> getHisEntityList() {
        return getEntityList(STATUS_HIS);
    }

    public static List<Entity> getFavEntityList() {
        return getEntityList(STATUS_FAV);
    }

    public static List<Entity> getEntityList() {
        return getEntityList(STATUS_ALL);
    }

    public static void removeEntity(Entity entity) {
        List<Entity> list = getEntityList(entity.getStatus());
        list.remove(entity);
    }

    /**
     * 将entity置于链表第一个
     *
     * @param entity entity
     * @return void
     */
    public static void first(Entity entity) {
        List<Entity> list = getEntityList(entity.getStatus());
        list.remove(entity);
        entity.setDate(new Date());
        if (entity.isUpdate()) {
            list.add(0, entity);
        } else {
            if (list.isEmpty() || list.get(list.size() - 1).getPriority() != 0) {
                list.add(entity);
            } else {
                for (int i = 0; i < list.size(); i++) {
                    if (list.get(i).getPriority() == 0) {
                        list.add(i, entity);
                        break;
                    }
                }
            }
        }
        DBUtil.save(entity, DBUtil.SAVE_CUR);
    }

    /*-----------------------------------------------------------------------------------------*/

    public static void addInfo(Entity entity, EntityInfo entityInfo) {
        ((List<EntityInfo>) entity.getInfoList()).add(entityInfo);
    }

    public static int sourceSize(Entity entity) {
        return entity.getInfoList().size();
    }

    public static String sourceName(Entity entity) {
        return SourceUtil.getSource(entity.getSourceId()).getSourceName();
    }

    public static boolean changeInfo(Entity entity, String[] ss) {
        int id = Integer.parseInt(ss[0]);
        int sourceId = Integer.parseInt(ss[1]);
        String author = "";
        if (ss.length > 2) {
            author = ss[2];
        }
        for (EntityInfo info : entity.getInfoList()) {
            if (info.getId() == id && info.getSourceId() == sourceId) {
                if (info.getAuthor() == null || Objects.equals(info.getAuthor(), author)) {
                    entity.setSourceId(sourceId);
                    entity.setInfo(info);
                    return true;
                }
            }
        }
        return false;
    }

    public static boolean changeInfo(Entity entity, int sourceId) {
        for (EntityInfo info : entity.getInfoList()) {
            if (info.getSourceId() == sourceId) {
                entity.setInfo(info);
                entity.setSourceId(sourceId);
                return true;
            }
        }
        return false;
    }

    public static String toStringView(Entity entity) {
        if (entity.getInfo() != null) {
            return "标题：" + entity.getTitle() +
                    "\n漫画源：" + sourceName(entity) +
                    "\n作者：" + entity.getAuthor() +
                    "\n上次阅读：" + DateUtil.format(entity.getDate()) +
                    "\n状态：" + entity.getInfo().getUpdateStatus() +
                    "\n简介：" + entity.getInfo().getIntro();
        } else {
            return "标题：" + entity.getTitle() +
                    "\n漫画源：" + sourceName(entity);
        }
    }

    /*-----------------------------------------------------------------------------------------*/

    public static boolean canLoad(EntityInfo entityInfo, boolean isLoadNext) {
        int id;
        if (isLoadNext) {
            id = entityInfo.getCurChapterId() + 1;
        } else {
            id = entityInfo.getCurChapterId() - 1;
        }
        boolean flag = checkChapterId(entityInfo, id);
        if (flag) {
            initChapterId(entityInfo, id);
        }
        return flag;
    }

    public static boolean checkChapterId(EntityInfo entityInfo, int chapterId) {
        return chapterId >= 0 && chapterId < entityInfo.getChapterInfoList().size();
    }

    public static void updateChapterId(Entity entity) {
        EntityInfo info = entity.getInfo();
        String curTitle = entity.getCurChapterTitle();
        int curId = info.getCurChapterId();
        if (!checkChapterId(info, curId)) {
            info.setCurChapterId(0);
            DBUtil.saveInfoData(info);
            return;
        }
        String title = info.getChapterInfoList().get(getPosition(info)).getTitle();
        if (!Objects.equals(curTitle, title)) {
            int x = 20;
            for (int i = curId - x; i < curId + x; i++) {
                if (checkChapterId(info, i)) {
                    String s = info.getChapterInfoList().get(getPosition(info, i)).getTitle();
                    if (Objects.equals(curTitle, s)) {
                        info.setCurChapterId(i);
                        DBUtil.saveInfoData(info);
                        return;
                    }
                }
            }
        }
    }

    /**
     * 获得当前章节id的chapterList position
     *
     * @return int
     */
    public static int getPosition(EntityInfo entityInfo) {
        return chapterIdToPosition(entityInfo, entityInfo.getCurChapterId());
    }

    /**
     * 获得指定章节id的chapterList position
     *
     * @param chapterId chapterId
     * @return int
     */
    public static int getPosition(EntityInfo entityInfo, int chapterId) {
        return chapterIdToPosition(entityInfo, chapterId);
    }

    /**
     * 设置chapterList章节position
     *
     * @param position position
     * @return void
     */
    public static void setPosition(EntityInfo entityInfo, int position) {
        entityInfo.setCurChapterId(positionToChapterId(entityInfo, position));
        initChapterTitle(entityInfo, position);
    }

    public static void newestChapter(EntityInfo entityInfo) {
        initChapterId(entityInfo, entityInfo.getChapterInfoList().size() - 1);
    }

    public static void initChapterId(EntityInfo entityInfo, int chapterId) {
        entityInfo.setCurChapterId(chapterId);
        entityInfo.setChapterNum(0);
        initChapterTitle(entityInfo, chapterIdToPosition(entityInfo, chapterId));
    }

    private static void initChapterTitle(EntityInfo entityInfo, int position) {
        if (checkChapterId(entityInfo, position)) {
            entityInfo.setCurChapterTitle(entityInfo.getChapterInfoList().get(position).getTitle());
        }
    }

    /**
     * 章节position 转 entityInfo.getCurChapterId()
     *
     * @param position position
     * @return int
     */
    public static int positionToChapterId(EntityInfo entityInfo, int position) {
        return entityInfo.getChapterInfoList().get(position).getId();
    }

    /**
     * entityInfo.getCurChapterId() 转 章节position
     *
     * @param chapterId chapterId
     * @return int
     */
    public static int chapterIdToPosition(EntityInfo entityInfo, int chapterId) {
        int position;
        if (entityInfo.getOrder() == EntityInfo.DESC) {
            position = entityInfo.getChapterInfoList().size() - chapterId - 1;
        } else {
            position = chapterId;
        }
        return position;
    }

    public static int getNextChapterId(EntityInfo entityInfo) {
        return entityInfo.getCurChapterId() + 1;
    }

    public static int getPrevChapterId(EntityInfo entityInfo) {
        return entityInfo.getCurChapterId() - 1;
    }

    public static int getPosition(List<ChapterInfo> list, int listId, int order) {
        if (order == EntityInfo.DESC) {
            return list.size() - listId - 1;
        } else {
            return listId;
        }
    }

    public static int getListPositionById(List<ChapterInfo> list, int chapterId, int order) {
        int listId = getListId(list, chapterId, order);
        return getPosition(list, listId, order);
    }

    public static int getListId(List<ChapterInfo> list, int chapterId, int order) {
        return chapterId - getFirstChapterId(list, order);
    }

    public static int getChapterId(List<ChapterInfo> list, int listPosition) {
        return list.get(listPosition).getId();
    }

    public static int getFirstChapterId(List<ChapterInfo> list, int order) {
        return list.get(getPosition(list, 0, order)).getId();
    }

    public static int getLastChapterId(List<ChapterInfo> list, int order) {
        return list.get(getPosition(list, list.size() - 1, order)).getId();
    }

    public static boolean checkChapterId(List<ChapterInfo> list, int chapterId, int order) {
        return chapterId >= getFirstChapterId(list, order) && chapterId <= getLastChapterId(list, order);
    }

    /*-----------------------------------------------------------------------------------------*/

    public static String toStringProgress(Content content) {
        return String.format(Locale.CHINA, "%d/%d", content.getCur() + 1, content.getTotal());
    }

    public static String toStringProgressDetail(Content content) {
        return String.format(Locale.CHINA, "%d-%d/%d", content.getChapterId(), content.getCur() + 1, content.getTotal());
    }
}
