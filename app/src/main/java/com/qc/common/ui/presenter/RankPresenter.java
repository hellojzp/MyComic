package com.qc.common.ui.presenter;


import com.qc.common.ui.view.RankView;
import com.qc.common.util.SourceUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.android.schedulers.AndroidSchedulers;
import okhttp3.Request;
import the.one.base.ui.presenter.BasePresenter;
import top.luqichuang.common.model.Entity;
import top.luqichuang.common.model.EntityInfo;
import top.luqichuang.common.model.Source;
import top.luqichuang.common.self.CommonCallback;
import top.luqichuang.common.util.NetUtil;
import top.luqichuang.common.util.SourceHelper;

/**
 * @author LuQiChuang
 * @desc
 * @date 2021/6/11 11:46
 * @ver 1.0
 */
public class RankPresenter extends BasePresenter<RankView> {

    private Source<EntityInfo> source;

    private final Map<String, List<Entity>> map = new HashMap<>();

    public RankPresenter(Source<EntityInfo> source) {
        this.source = source;
    }

    public void clearCache(String url) {
        String key = url;
        while (map.containsKey(key)) {
            map.remove(key);
            key = SourceHelper.getNextUrl(key);
        }
    }

    public void load(String url) {
        RankView view = getView();
        if (url == null) {
            AndroidSchedulers.mainThread().scheduleDirect(() -> {
                if (view != null) {
                    view.loadComplete(new ArrayList<>());
                }
            });
        } else {
            List<Entity> list = map.get(url);
            if (list != null) {
                AndroidSchedulers.mainThread().scheduleDirect(() -> {
                    if (view != null) {
                        view.loadComplete(list);
                    }
                });
            } else {
                loadWithNet(url);
            }
        }
    }

    private void loadWithNet(String url) {
        Request request = source.getRankRequest(url);
        NetUtil.startLoad(request, new CommonCallback(source, Source.RANK) {
            @Override
            public void onFailure(String errorMsg) {
                onEnd(null);
            }

            @Override
            public void onResponse(String html, Map<String, Object> map) {
                List<EntityInfo> infoList = SourceUtil.getRankInfoList(source, html);
                List<Entity> entityList = getEntityList(infoList);
                RankPresenter.this.map.put(url, entityList);
                onEnd(entityList);
            }

            private void onEnd(List<Entity> entityList) {
                RankView view = getView();
                AndroidSchedulers.mainThread().scheduleDirect(() -> {
                    if (view != null) {
                        view.loadComplete(entityList);
                    }
                });
            }
        });
    }

    public List<Entity> getEntityList(List<EntityInfo> infoList) {
        List<Entity> entityList = new ArrayList<>();
        if (infoList != null) {
            for (EntityInfo info : infoList) {
                if (info.getTitle() != null) {
                    Entity entity = SourceUtil.getEntity(info);
                    entityList.add(entity);
                }
            }
        }
        return entityList;
    }

}