package com.qc.common.ui.presenter;

import com.qc.common.ui.view.UpdateView;
import com.qc.common.util.VersionUtil;

import java.util.Map;

import io.reactivex.android.schedulers.AndroidSchedulers;
import the.one.base.ui.presenter.BasePresenter;
import the.one.base.util.ToastUtil;
import top.luqichuang.common.jsoup.JsoupNode;
import top.luqichuang.common.self.CommonCallback;
import top.luqichuang.common.util.NetUtil;


/**
 * @author LuQiChuang
 * @desc
 * @date 2020/8/12 15:25
 * @ver 1.0
 */
public class UpdatePresenter extends BasePresenter<UpdateView> {

    private String url = "https://gitee.com/luqichuang/MyComic/releases";

    public void checkUpdate() {
        NetUtil.startLoad(url, new CommonCallback() {
            @Override
            public void onFailure(String errorMsg) {
                onEnd(null, null);
            }

            @Override
            public void onResponse(String html, Map<String, Object> map) {
                JsoupNode node = new JsoupNode(html);
                node.init(node.html("div.release-tag-item"));
                String versionTag = node.ownText("div.tag-name span");
                String href = node.href("div.item a");
                onEnd(versionTag, href);
            }

            private void onEnd(String versionTag, String href) {
                UpdateView view = getView();
                AndroidSchedulers.mainThread().scheduleDirect(() -> {
                    if (view != null) {
                        view.getVersionTag(versionTag, href);
                    }
                });
            }
        });
    }

    public void checkApkUpdate() {
        NetUtil.startLoad(url, new CommonCallback() {
            @Override
            public void onFailure(String errorMsg) {
            }

            @Override
            public void onResponse(String html, Map<String, Object> map) {
                AndroidSchedulers.mainThread().scheduleDirect(() -> {
                    JsoupNode node = new JsoupNode(html);
                    String versionTag = node.ownText("div.tag-name span");
                    if (existUpdate(versionTag, VersionUtil.versionName)) {
                        String title = "存在新版本" + versionTag + "，快去更新吧！";
                        ToastUtil.show(title);
                    }
                });
            }
        });
    }

    public boolean existUpdate(String updateTag, String localTag) {
        boolean flag = false;
        if (updateTag != null && localTag != null && !updateTag.equals(localTag)) {
            String[] tags = updateTag.replace("v", "").split("\\.");
            String[] locals = localTag.replace("v", "").split("\\.");
            try {
                for (int i = 0; i < tags.length; i++) {
                    int tag = Integer.parseInt(tags[i]);
                    int local = Integer.parseInt(locals[i]);
                    if (tag > local) {
                        flag = true;
                        break;
                    }
                    if (tag < local) {
                        break;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return flag;
    }

}
