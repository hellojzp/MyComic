package com.qc.common.ui.fragment;

import android.content.Intent;
import android.net.Uri;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.qc.common.en.SettingEnum;
import com.qc.common.en.TextEnum;
import com.qc.common.en.data.Data;
import com.qc.common.en.data.Text;
import com.qc.common.self.ImageConfig;
import com.qc.common.ui.presenter.ChapterPresenter;
import com.qc.common.ui.view.ChapterView;
import com.qc.common.util.AnimationUtil;
import com.qc.common.util.DBUtil;
import com.qc.common.util.EntityUtil;
import com.qc.common.util.ImageUtil;
import com.qc.common.util.PopupUtil;
import com.qc.common.util.SettingUtil;
import com.qc.common.util.SourceUtil;
import com.qc.mycomic.R;
import com.qmuiteam.qmui.qqface.QMUIQQFaceView;
import com.qmuiteam.qmui.widget.QMUIRadiusImageView;
import com.qmuiteam.qmui.widget.dialog.QMUIBottomSheet;
import com.qmuiteam.qmui.widget.popup.QMUIPopup;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import the.one.base.ui.fragment.BaseFragment;
import the.one.base.ui.fragment.BaseTabFragment;
import the.one.base.util.QMUIDialogUtil;
import the.one.base.util.QMUIPopupUtil;
import the.one.base.util.ToastUtil;
import top.luqichuang.common.model.ChapterInfo;
import top.luqichuang.common.model.Entity;
import top.luqichuang.common.model.EntityInfo;
import top.luqichuang.common.model.Source;
import top.luqichuang.common.util.MapUtil;
import top.luqichuang.common.util.StringUtil;

/**
 * @author LuQiChuang
 * @desc
 * @date 2021/6/24 12:27
 * @ver 1.0
 */
public class ChapterFragment extends BaseTabFragment implements ChapterView {

    private Entity entity;
    private ChapterPresenter presenter = new ChapterPresenter();
    private int count = 0;
    private int size = SourceUtil.size();

    private QMUIPopup mSettingPopup;
    private String[] mMenus = (String[]) TextEnum.CHAPTER_MENUS.getValue();
    private ImageButton ibMenu;
    private ImageButton ibSwap;

    private QMUIRadiusImageView imageView;
    private RelativeLayout relativeLayout;
    private TextView tvTitle;
    private TextView tvSource;
    private TextView tvSourceSize;
    private TextView tvUpdateTime;
    private TextView tvUpdateChapter;
    private TextView tvRead;
    private TextView tvReadNum;
    private LinearLayout favLayout;
    private ImageView ivFav;
    private TextView tvFav;
    private View llIndicator;

    public ChapterFragment() {
        this.entity = Data.getEntity();
    }

    @Override
    public void onResume() {
        super.onResume();
        //后退返回此页面时刷新数据
        if (entity != null && !fragments.isEmpty()) {
            for (BaseFragment fragment : fragments) {
                ((ChapterItemFragment) fragment).updateData();
            }
            setValue();
        }
    }

    @Override
    protected boolean isTabFromNet() {
        return true;
    }

    @Override
    protected void initView(View rootView) {
        super.initView(rootView);
        showLoadingPage();
        mMagicIndicator = rootView.findViewById(R.id.indicator);
        mViewPager = rootView.findViewById(R.id.viewPager);
        mViewPager.setOffscreenPageLimit(100);
        QMUIQQFaceView mTitle = mTopLayout.setTitle(Text.DETAIL);
        mTopLayout.setTitleGravity(Gravity.CENTER);
        mTitle.setTextColor(getColor(R.color.qmui_config_color_gray_1));
        mTitle.getPaint().setFakeBoldText(true);
        addTopBarBackBtn();
        addView();
        setListener();
        if (Data.toStatus != Data.TO_IMPORT) {
            loadDialog();
            setValue();
            startInit();
        }
        requestServer();
    }

    @Override
    protected void onLazyInit() {
    }

    private void addView() {
        ibMenu = mTopLayout.addRightImageButton(R.drawable.ic_baseline_menu_24, R.id.topbar_right_button1);
        ibSwap = mTopLayout.addRightImageButton(R.drawable.ic_baseline_swap_vert_24, R.id.topbar_right_button2);
        relativeLayout = mRootView.findViewById(R.id.imageRelativeLayout);
        imageView = mRootView.findViewById(R.id.imageView);
        tvTitle = mRootView.findViewById(R.id.tvTitle);
        tvSource = mRootView.findViewById(R.id.tvSource);
        tvSourceSize = mRootView.findViewById(R.id.tvSourceSize);
        tvUpdateTime = mRootView.findViewById(R.id.tvUpdateTime);
        tvUpdateChapter = mRootView.findViewById(R.id.tvUpdateChapter);
        tvRead = mRootView.findViewById(R.id.tvRead);
        tvReadNum = mRootView.findViewById(R.id.tvReadNum);
        favLayout = mRootView.findViewById(R.id.favLayout);
        ivFav = favLayout.findViewById(R.id.ivFav);
        tvFav = favLayout.findViewById(R.id.tvFav);
        llIndicator = mRootView.findViewById(R.id.llIndicator);
    }

    private void setListener() {
        //图片长按监听
        imageView.setOnLongClickListener(v -> {
            ImageConfig config = ImageUtil.getDefaultConfig(getContext(), entity.getInfo().getImgUrl(), relativeLayout);
            config.setForce(true);
            config.setSave(true);
            ImageUtil.setSaveKey(entity, config);
            Source<EntityInfo> source = SourceUtil.getSource(entity.getSourceId());
            config.setHeaders(source.getImageHeaders());
            ImageUtil.loadImage(getContext(), config);
            return false;
        });

        //菜单按钮: 更新源、查看信息
        ibMenu.setOnClickListener(v -> {
            if (null == mSettingPopup) {
                mSettingPopup = QMUIPopupUtil.createListPop(_mActivity, mMenus, (adapter, view, position) -> {
                    mSettingPopup.dismiss();
                    if (position == 0) {
                        if (count != 0) {
                            ToastUtil.show(Text.TOAST_LOAD_ING);
                            progressDialog.show();
                            return;
                        }
                        String msg = String.format(Locale.CHINA, Text.FORMAT_SOURCE_UPDATE_PROGRESS, count, size);
                        initProgressDialog();
                        showProgressDialog(count, size, msg);
                        presenter.updateSource(entity);
                    } else if (position == 1) {
                        QMUIDialogUtil.showSimpleDialog(getContext(), Text.INFO, EntityUtil.toStringView(entity)).show();
                    } else if (position == 2) {
                        String url = entity.getInfo().getDetailUrl();
                        Uri uri = Uri.parse(url);
                        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                        startActivity(intent);
                    } else if (position == 3) {
                        Data.isGrid = !Data.isGrid;
                        SettingUtil.putSetting(SettingEnum.IS_GRID, Data.isGrid);
                        for (BaseFragment fragment : fragments) {
                            ((ChapterItemFragment) fragment).changeAdapter();
                        }
                    }
                });
            }
            mSettingPopup.show(ibMenu);
        });

        //更换顺序
        ibSwap.setOnClickListener(v -> {
            if (checkNotEmpty()) {
                showLoadingPage();
                EntityInfo entityInfo = entity.getInfo();
                entityInfo.setOrder(entityInfo.getOrder() == EntityInfo.ASC ? EntityInfo.DESC : EntityInfo.ASC);
                DBUtil.saveInfoData(entityInfo);
                loadComplete(null);
            } else {
                showFailTips(Text.TIP_NO_CHAPTER);
            }
        });

        //改变源
        TextView tvSource = mRootView.findViewById(R.id.tvSource);
        tvSource.setOnClickListener(v -> {
            Map<String, String> map = PopupUtil.getMap(entity.getInfoList());
            String key = PopupUtil.getKey(entity);
            PopupUtil.showSimpleBottomSheetList(getContext(), map, key, Text.SOURCE_CHANGE, new QMUIBottomSheet.BottomListSheetBuilder.OnSheetItemClickListener() {
                @Override
                public void onClick(QMUIBottomSheet dialog, View itemView, int position, String tag) {
                    dialog.dismiss();
                    String key = MapUtil.getKeyByValue(map, tag);
                    String[] ss = key.split("#");
                    if (EntityUtil.changeInfo(entity, ss)) {
                        setValue();
                        requestServer();
                        DBUtil.save(entity, DBUtil.SAVE_ONLY);
                    }
                }
            });
        });

        //阅读最新章节
        TextView tvUpdateChapter = mRootView.findViewById(R.id.tvUpdateChapter);
        tvUpdateChapter.setOnClickListener(v -> {
            if (checkNotEmpty()) {
                EntityUtil.newestChapter(entity.getInfo());
                ((ChapterItemFragment) fragments.get(INDEX)).start();
            } else {
                showFailTips(Text.TIP_NO_CHAPTER);
            }
        });

        //收藏
        LinearLayout favLayout = mRootView.findViewById(R.id.favLayout);
        favLayout.setOnClickListener(v -> {
            boolean isFav = entity.getStatus() != EntityUtil.STATUS_FAV;
            setFavLayout(isFav, true);
            EntityUtil.removeEntity(entity);
            entity.setStatus(isFav ? EntityUtil.STATUS_FAV : EntityUtil.STATUS_HIS);
            EntityUtil.first(entity);
        });

        //开始阅读
        LinearLayout llRead = mRootView.findViewById(R.id.llRead);
        llRead.setOnClickListener(v -> {
            if (checkNotEmpty()) {
                if (fragments.size() <= INDEX) {
                    INDEX = 0;
                }
                ((ChapterItemFragment) fragments.get(INDEX)).startId(entity.getInfo().getCurChapterId());
            } else {
                showFailTips(Text.TIP_NO_CHAPTER);
            }
        });

        //阅读下一章
        TextView tvReadNext = mRootView.findViewById(R.id.tvReadNext);
        tvReadNext.setOnClickListener(v -> {
            if (checkNotEmpty()) {
                if (fragments.size() <= INDEX) {
                    INDEX = 0;
                }
                ((ChapterItemFragment) fragments.get(INDEX)).startId(entity.getInfo().getCurChapterId() + 1);
            } else {
                showFailTips(Text.TIP_NO_CHAPTER);
            }
        });
    }

    private void checkEntity() {
        List<Entity> entityList = EntityUtil.getEntityList();
        int index = entityList.indexOf(entity);
        if (index != -1) {
            Entity myEntity = entityList.get(index);
            for (EntityInfo entityInfo : entity.getInfoList()) {
                if (!myEntity.getInfoList().contains(entityInfo)) {
                    EntityUtil.addInfo(myEntity, entityInfo);
                    DBUtil.saveInfoData(entityInfo);
                }
            }
            entity = myEntity;
            Data.setEntity(entity);
            setValue();
        }
    }

    private void loadDialog() {
        if (Data.toStatus == Data.TO_IMPORT && entity.getInfo().getTitle() != null) {
            Data.toStatus = Data.RANK_TO_CHAPTER;
            List<Entity> entityList = EntityUtil.getEntityList();
            entity.setTitle(entity.getInfo().getTitle());
            int index = entityList.indexOf(entity);
            if (index != -1) {
                Entity e = entityList.get(index);
                if (!EntityUtil.changeInfo(e, entity.getSourceId())) {
                    EntityUtil.addInfo(e, entity.getInfo());
                }
                Data.setEntity(entity);
                this.entity = entityList.get(index);
            } else {
                DBUtil.save(entity, DBUtil.SAVE_ALL);
                EntityUtil.initEntityList(EntityUtil.STATUS_ALL);
            }
        }
        if (Data.toStatus == Data.RANK_TO_CHAPTER) {
            Data.toStatus = Data.NORMAL;
            checkEntity();
            if (entity.getInfoList().size() > 1) {
                return;
            }
            QMUIDialogUtil.showPositiveDialog(getContext(), Text.SOURCE, Text.SOURCE_UPDATE_TIP,
                    Text.OPTION_CANCEL, (dialog, index) -> {
                        dialog.dismiss();
                    }, Text.OPTION_YES, (dialog, index) -> {
                        dialog.dismiss();
                        if (count != 0) {
                            ToastUtil.show(Text.TOAST_LOAD_ING);
                            progressDialog.show();
                            return;
                        }
                        String msg = String.format(Locale.CHINA, Text.FORMAT_SOURCE_UPDATE_PROGRESS, count, size);
                        initProgressDialog();
                        showProgressDialog(count, size, msg);
                        presenter.updateSource(entity);
                    });
        }
    }

    private void setValue() {
        ImageConfig config = ImageUtil.getDefaultConfig(getContext(), entity.getInfo().getImgUrl(), relativeLayout);
        config.setSave(true);
        ImageUtil.setSaveKey(entity, config);
        Source<EntityInfo> source = SourceUtil.getSource(entity.getSourceId());
        config.setHeaders(source.getImageHeaders());
        ImageUtil.loadImage(getContext(), config);
        tvTitle.setText(entity.getInfo().getTitle());
        tvSource.setText(EntityUtil.sourceName(entity));
        tvSourceSize.setText(String.format(Locale.CHINA, Text.FORMAT_CHAPTER_SOURCE_COUNT, EntityUtil.sourceSize(entity)));
        tvUpdateChapter.setText(entity.getInfo().getUpdateChapter());
        tvUpdateTime.setText(entity.getInfo().getUpdateTime());
        if (entity.getCurChapterTitle() != null) {
            tvRead.setText(Text.READ_CONTINUE);
            tvReadNum.setText(String.format(Locale.CHINA, Text.FORMAT_CHAPTER_READ_NUM, entity.getCurChapterTitle(), entity.getInfo().getChapterNum() + 1));
            tvReadNum.setVisibility(View.VISIBLE);
        } else {
            tvRead.setText(Text.READ_START);
            tvReadNum.setVisibility(View.GONE);
        }
        setFavLayout(entity.getStatus() == EntityUtil.STATUS_FAV);
    }

    public void setFavLayout(boolean isFav) {
        setFavLayout(isFav, false);
    }

    public void setFavLayout(boolean isFav, boolean needAnimation) {
        if (isFav) {
            AnimationUtil.changeDrawable(ivFav, getDrawable(R.drawable.ic_baseline_favorite_24), needAnimation);
            tvFav.setText(Text.FAV_YES);
        } else {
            AnimationUtil.changeDrawable(ivFav, getDrawable(R.drawable.ic_baseline_favorite_border_24), needAnimation);
            tvFav.setText(Text.FAV_NO);
        }
    }

    @Override
    protected void requestServer() {
        itemLoading();
        Map<String, List<ChapterInfo>> map = entity.getInfo().getChapterInfoMap();
        if (map == null || map.size() == 0 || entity.getInfo().getChapterInfoList().isEmpty()) {
            presenter.load(entity);
        } else {
            loadComplete(null);
        }
    }

    @Override
    public ChapterPresenter getPresenter() {
        return presenter;
    }

    @Override
    protected void startInit() {
        super.startInit();
        Map<String, List<ChapterInfo>> map = entity.getInfo().getChapterInfoMap();
        if (!map.isEmpty() && !entity.getInfo().getChapterInfoList().isEmpty()) {
            int i = 0;
            for (List<ChapterInfo> list : map.values()) {
                ChapterItemFragment fragment = (ChapterItemFragment) fragments.get(i);
                fragment.setList(list);
                if (list != null && !list.isEmpty()) {
                    int firstId = EntityUtil.getFirstChapterId(list, entity.getInfo().getOrder());
                    int lastId = EntityUtil.getLastChapterId(list, entity.getInfo().getOrder());
                    int curChapterId = entity.getInfo().getCurChapterId();
                    if (curChapterId >= firstId && curChapterId <= lastId) {
                        mViewPager.setCurrentItem(i, false);
                    }
                }
                i++;
            }
        } else {
            loadComplete(Text.TIP_CHAPTER_FAIL);
        }
    }

    @Override
    public void loadComplete(String errorMsg) {
        if (errorMsg != null) {
            loadFail(errorMsg);
            return;
        }
        List<ChapterInfo> list = entity.getInfo().getChapterInfoList();
        if (isNeedSwap(list, entity.getInfo().getOrder())) {
            StringUtil.swapList(list);
            for (List<ChapterInfo> value : entity.getInfo().getChapterInfoMap().values()) {
                if (list != value) {
                    StringUtil.swapList(value);
                }
            }
        }
        loadDialog();
        setValue();
        startInit();
    }

    public void loadFail(String errorMsg) {
        for (int i = 0; i < fragments.size(); i++) {
            fragments.get(i).showEmptyPage(errorMsg, v -> {
                requestServer();
            });
        }
        if (Data.toStatus == Data.TO_IMPORT) {
            Data.toStatus = Data.NORMAL;
            showEmptyPage(Text.TIP_INPUT_FAIL, v -> {
                showLoadingPage();
                Data.toStatus = Data.TO_IMPORT;
                requestServer();
            });
        }
    }

    private boolean isNeedSwap(List<ChapterInfo> list, int order) {
        if (list != null && !list.isEmpty()) {
            int firstId = list.get(0).getId();
            int lastId = list.get(list.size() - 1).getId();
            if (firstId > lastId) {
                return order != EntityInfo.DESC;
            } else if (firstId < lastId) {
                return order != EntityInfo.ASC;
            } else {
                return false;
            }
        }
        return false;
    }

    @Override
    public void updateSourceComplete(List<EntityInfo> infoList) {
        count++;
        if (infoList != null) {
            for (EntityInfo info : infoList) {
                updateEntityInfo(info);
            }
        }
        if (count == size) {
            count = 0;
            hideProgressDialog();
            showSuccessTips(Text.SEARCH_COMPLETE);
            DBUtil.save(entity, DBUtil.SAVE_ALL);
        } else {
            String msg = String.format(Locale.CHINA, Text.FORMAT_SOURCE_UPDATE_PROGRESS, count, size);
            showProgressDialog(count, size, msg);
            tvSourceSize.setText(String.format(Locale.CHINA, Text.FORMAT_CHAPTER_SOURCE_COUNT, EntityUtil.sourceSize(entity)));
        }
    }

    private void updateEntityInfo(EntityInfo info) {
        if (!entity.getTitle().equals(info.getTitle())) {
            return;
        }
        int index = entity.getInfoList().indexOf(info);
        if (index == -1) {
            EntityUtil.addInfo(entity, info);
        }
    }

    private boolean checkNotEmpty() {
        return !entity.getInfo().getChapterInfoList().isEmpty();
    }

    @Override
    protected void addTabs() {
        mTabs.clear();
        Map<String, List<ChapterInfo>> map = entity.getInfo().getChapterInfoMap();
        if ((map.size() == 1 && map.containsKey(Text.TEXT)) || map.isEmpty()) {
            llIndicator.setVisibility(View.GONE);
        } else {
            llIndicator.setVisibility(View.VISIBLE);
            for (String key : entity.getInfo().getChapterInfoMap().keySet()) {
                addTab(key);
            }
        }
    }

    @Override
    protected void addFragment(ArrayList<BaseFragment> fragments) {
        //设置默认fragment
        if (fragments.isEmpty()) {
            if (!getChildFragmentManager().getFragments().isEmpty()) {
                fragments.addAll((List<BaseFragment>) (List) getChildFragmentManager().getFragments());
            } else {
                fragments.add(new ChapterItemFragment());
            }
        }
        //需求fragment个数
        int mapSize = entity.getInfo().getChapterInfoMap().size();
        //当前fragment个数
        int fSize = fragments.size();
        if (mapSize > fSize) {
            //需要更多则新建fragment
            for (int i = 0; i < mapSize - fSize; i++) {
                ChapterItemFragment fragment = new ChapterItemFragment();
                fragments.add(fragment);
            }
        } else if (mapSize < fSize) {
            //需求更少则删除当前fragment
            if (mapSize > 0) {
                fragments.subList(mapSize, fSize).clear();
            } else {
                fragments.subList(1, fSize).clear();
            }
        }
    }

    public void itemLoading() {
        //禁止显示tab
        if (llIndicator != null) {
            llIndicator.setVisibility(View.GONE);
        }
        //滑动到第一个fragment
        mViewPager.setCurrentItem(0, false);
        //禁止手动滑动
        mViewPager.setSwipeable(false);
        //显示loading页面
        if (!fragments.isEmpty()) {
            fragments.get(0).showLoadingPage();
        }
    }

    @Override
    protected int getContentViewId() {
        return R.layout.fragment_chapter;
    }
}
