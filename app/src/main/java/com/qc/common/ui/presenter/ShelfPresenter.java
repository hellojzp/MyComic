package com.qc.common.ui.presenter;

import com.qc.common.en.data.Data;
import com.qc.common.ui.view.ShelfView;
import com.qc.common.util.DBUtil;
import com.qc.common.util.EntityUtil;
import com.qc.common.util.SourceUtil;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import io.reactivex.android.schedulers.AndroidSchedulers;
import okhttp3.Request;
import the.one.base.ui.presenter.BasePresenter;
import top.luqichuang.common.model.Entity;
import top.luqichuang.common.model.EntityInfo;
import top.luqichuang.common.model.Source;
import top.luqichuang.common.self.CommonCallback;
import top.luqichuang.common.util.NetUtil;

/**
 * @author LuQiChuang
 * @desc
 * @date 2021/6/9 18:50
 * @ver 1.0
 */
public class ShelfPresenter extends BasePresenter<ShelfView> {

    private boolean isAll = false;

    private int priority = 0;

    private boolean checkTime() {
        long time = (new Date()).getTime();
        if (time - Data.time < 60 * 60 * 1000) {
            Data.time = time;
            return true;
        } else {
            Data.time = time;
            return false;
        }
    }

    public void checkUpdate(List<Entity> entityList) {
        boolean flag = checkTime();
        for (Entity entity : entityList) {
            if (flag && !entity.getInfo().getChapterInfoList().isEmpty()) {
                ShelfView view = getView();
                AndroidSchedulers.mainThread().scheduleDirect(() -> {
                    if (view != null) {
                        view.checkUpdateComplete(null);
                    }
                });
                continue;
            }
            if (isAll) {
                for (EntityInfo info : entity.getInfoList()) {
                    checkUpdate(entity, info);
                }
            } else {
                checkUpdate(entity, entity.getInfo());
            }
        }
    }

    private void checkUpdate(Entity entity, EntityInfo info) {
        Source<EntityInfo> source = SourceUtil.getSource(entity.getSourceId());
        Request request = source.getDetailRequest(info.getDetailUrl());
        NetUtil.startLoad(request, new CommonCallback(source, Source.DETAIL) {
            @Override
            public void onFailure(String errorMsg) {
                onEnd(info.getTitle());
            }

            @Override
            public void onResponse(String html, Map<String, Object> map) {
                String curUpdateChapter = info.getUpdateChapter();
                SourceUtil.setInfoDetail(source, info, html, map);
                if (curUpdateChapter == null || !curUpdateChapter.equals(info.getUpdateChapter())) {
                    if (info.getUpdateChapter() != null) {
                        entity.setUpdate(true);
                        entity.setPriority(++priority);
                        EntityUtil.first(entity);
                    }
                }
                if (info.getChapterInfoList().isEmpty()) {
                    onEnd(info.getTitle());
                } else {
                    onEnd(null);
                }
                DBUtil.save(entity, DBUtil.SAVE_CUR);
            }

            private void onEnd(String title) {
                ShelfView view = getView();
                AndroidSchedulers.mainThread().scheduleDirect(() -> {
                    if (view != null) {
                        view.checkUpdateComplete(title);
                    }
                });
            }
        });
    }

    public void addEntityInfo(List<Entity> entityList, Source<EntityInfo> source) {
        for (Entity entity : entityList) {
            boolean exist = false;
            for (EntityInfo info : entity.getInfoList()) {
                if (info.getSourceId() == source.getSourceId()) {
                    exist = true;
                    break;
                }
            }
            if (exist) {
                addComplete();
            } else {
                String title = entity.getTitle();
                Request request = source.getSearchRequest(title);
                NetUtil.startLoad(request, new CommonCallback(source, Source.SEARCH) {
                    @Override
                    public void onFailure(String errorMsg) {
                        addComplete();
                    }

                    @Override
                    public void onResponse(String html, Map<String, Object> map) {
                        List<EntityInfo> infoList = SourceUtil.getInfoList(source, html);
                        for (EntityInfo info : infoList) {
                            if (Objects.equals(title, info.getTitle())) {
                                EntityUtil.addInfo(entity, info);
                                DBUtil.saveInfoData(info);
                                break;
                            }
                        }
                        addComplete();
                    }
                });
            }
        }
    }

    private void addComplete() {
        ShelfView view = getView();
        AndroidSchedulers.mainThread().scheduleDirect(() -> {
            if (view != null) {
                view.addEntityInfoComplete();
            }
        });
    }

    public void initPriority() {
        this.priority = 0;
    }

}
