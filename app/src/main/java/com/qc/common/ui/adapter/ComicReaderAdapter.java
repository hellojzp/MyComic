package com.qc.common.ui.adapter;

import android.widget.RelativeLayout;

import com.qc.common.en.data.Data;
import com.qc.common.self.ImageConfig;
import com.qc.common.util.ImageUtil;
import com.qc.common.util.SourceUtil;
import com.qc.mycomic.R;

import org.jetbrains.annotations.NotNull;

import the.one.base.adapter.TheBaseQuickAdapter;
import the.one.base.adapter.TheBaseViewHolder;
import top.luqichuang.common.model.Content;
import top.luqichuang.common.model.Entity;
import top.luqichuang.common.model.EntityInfo;
import top.luqichuang.common.model.Source;

/**
 * @author LuQiChuang
 * @desc
 * @date 2021/6/11 9:52
 * @ver 1.0
 */
public class ComicReaderAdapter extends TheBaseQuickAdapter<Content> {

    protected Entity entity;

    public ComicReaderAdapter() {
        super(R.layout.item_reader);
        this.entity = Data.getEntity();
    }

    @Override
    protected void convert(@NotNull TheBaseViewHolder holder, Content content) {
        RelativeLayout layout = holder.findView(R.id.imageRelativeLayout);
        ImageConfig config = ImageUtil.getReaderConfig(getContext(), content.getUrl(), layout);
        Source<EntityInfo> source = SourceUtil.getSource(entity.getSourceId());
        config.setHeaders(source.getImageHeaders());
        ImageUtil.loadImage(getContext(), config);
    }

}
