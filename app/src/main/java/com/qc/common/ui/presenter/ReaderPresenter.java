package com.qc.common.ui.presenter;

import com.qc.common.en.data.Text;
import com.qc.common.ui.view.ReaderView;
import com.qc.common.util.EntityUtil;
import com.qc.common.util.SourceUtil;

import java.util.List;
import java.util.Map;

import io.reactivex.android.schedulers.AndroidSchedulers;
import okhttp3.Request;
import the.one.base.ui.presenter.BasePresenter;
import top.luqichuang.common.model.ChapterInfo;
import top.luqichuang.common.model.Content;
import top.luqichuang.common.model.Entity;
import top.luqichuang.common.model.EntityInfo;
import top.luqichuang.common.model.Source;
import top.luqichuang.common.self.CommonCallback;
import top.luqichuang.common.util.NetUtil;

/**
 * @author LuQiChuang
 * @desc
 * @date 2021/6/10 22:30
 * @ver 1.0
 */
public class ReaderPresenter extends BasePresenter<ReaderView> {

    public void loadContentInfoList(Entity entity) {
        int chapterId = entity.getInfo().getCurChapterId();
        loadContentInfoList(entity, chapterId);
    }

    public void loadContentInfoList(Entity entity, int chapterId) {
        List<ChapterInfo> chapterInfoList = entity.getInfo().getChapterInfoList();
        int position = EntityUtil.getPosition(entity.getInfo(), chapterId);
        String chapterUrl = chapterInfoList.get(position).getChapterUrl();
        Source<EntityInfo> source = SourceUtil.getSource(entity.getSourceId());
        Request request = source.getContentRequest(chapterUrl);
        NetUtil.startLoad(request, new CommonCallback(source, Source.CONTENT) {
            @Override
            protected void initData(Map<String, Object> data) {
                super.initData(data);
                data.put("chapterId", chapterId);
            }

            @Override
            protected int getReloadNum() {
                return 3;
            }

            @Override
            public void onFailure(String errorMsg) {
                onEnd(null, errorMsg);
            }

            @Override
            public void onResponse(String html, Map<String, Object> map) {
                List<Content> list = SourceUtil.getContentList(source, html, chapterId, map);
                if (list.isEmpty()) {
                    onEnd(null, Text.CONTENT_FAIL);
                } else {
                    onEnd(list, null);
                }
            }

            private void onEnd(List<Content> list, String errorMsg) {
                ReaderView view = getView();
                AndroidSchedulers.mainThread().scheduleDirect(() -> {
                    if (view != null) {
                        view.loadReadContentComplete(list, chapterUrl, errorMsg);
                    }
                });
            }
        });
    }

}
