package com.qc.common.ui.fragment;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.RecyclerView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.qc.common.en.data.Data;
import com.qc.common.en.data.Text;
import com.qc.common.ui.adapter.RankAdapter;
import com.qc.common.ui.adapter.RankLeftAdapter;
import com.qc.common.ui.presenter.RankPresenter;
import com.qc.common.ui.view.RankView;
import com.qc.common.util.SourceUtil;
import com.qc.mycomic.R;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import the.one.base.ui.fragment.BaseListFragment;
import the.one.base.ui.presenter.BasePresenter;
import top.luqichuang.common.model.Entity;
import top.luqichuang.common.model.EntityInfo;
import top.luqichuang.common.model.Source;
import top.luqichuang.common.util.MapUtil;
import top.luqichuang.common.util.SourceHelper;

/**
 * @author LuQiChuang
 * @desc
 * @date 2021/6/11 11:27
 * @ver 1.0
 */
public class RankFragment extends BaseListFragment<Entity> implements RankView {

    private RankAdapter rankAdapter;

    private RankPresenter presenter;

    private Source<EntityInfo> source;

    private Map<String, String> map;

    private List<Entity> lastList = new ArrayList<>();

    private String url;

    private String loadUrl;

    public static RankFragment getInstance(int sourceId) {
        RankFragment fragment = new RankFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("sourceId", sourceId);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        int sourceId = (int) getArguments().get("sourceId");
        this.source = SourceUtil.getSource(sourceId);
        this.rankAdapter = new RankAdapter(R.layout.item_rank_right);
        this.presenter = new RankPresenter(source);
        super.onCreate(savedInstanceState);
    }

    @Override
    protected boolean isNeedAround() {
        return true;
    }

    @Override
    protected BaseQuickAdapter getAdapter() {
        return rankAdapter;
    }

    @Override
    protected void initView(View rootView) {
        super.initView(rootView);
        if (source != null && source.getRankMap() != null) {
            map = source.getRankMap();
            if (!map.isEmpty()) {
                url = MapUtil.getFirstValue(map);
            }
        }
        if (map != null && !map.isEmpty()) {
            View leftView = getView(R.layout.fragment_rank_left);
            List<String> items = MapUtil.getKeyList(map);
            RankLeftAdapter rankLeftAdapter = new RankLeftAdapter(R.layout.item_rank_left, items);
            RecyclerView leftRecyclerView = leftView.findViewById(R.id.recycleView);
            initRecycleView(leftRecyclerView, TYPE_LIST, rankLeftAdapter);
            leftRecyclerView.addItemDecoration(new DividerItemDecoration(_mActivity, DividerItemDecoration.VERTICAL));
            rankLeftAdapter.setOnItemClickListener((adapter, view, position) -> {
                rankLeftAdapter.setPosition(position);
                url = MapUtil.getValueByIndex(map, position);
                onFirstLoading();
            });
            flLeftLayout.addView(leftView);
        } else {
            showEmptyPage(Text.TIP_LOAD_FAIL);
        }
    }

    @Override
    protected void requestServer() {
        if (isFirst || isHeadFresh) {
            showLoadingPage();
            loadUrl = url;
        }
        if (isHeadFresh) {
            presenter.clearCache(url);
        }
        if (loadUrl != null) {
            presenter.load(loadUrl);
        } else {
            loadComplete(new ArrayList<>());
        }
    }

    @Override
    protected int getContentViewId() {
        return R.layout.layout_recycle_view;
    }

    @Override
    public void onItemClick(@NonNull BaseQuickAdapter<?, ?> adapter, @NonNull View view, int position) {
        Entity entity = (Entity) adapter.getData().get(position);
        Data.toStatus = Data.RANK_TO_CHAPTER;
        Data.setEntity(entity);
        startFragment(new ChapterFragment());
    }

    @Override
    public boolean onItemLongClick(@NonNull BaseQuickAdapter adapter, @NonNull View view, int position) {
        return false;
    }

    @Override
    public BasePresenter getPresenter() {
        return presenter;
    }

    @Override
    public void loadComplete(List<Entity> entityList) {
        if (entityList == null) {
            if (isFirst || isHeadFresh) {
                showErrorPage(Text.TIP_LOAD_FAIL, v -> onFirstLoading());
            } else {
                rankAdapter.getLoadMoreModule().loadMoreFail();
            }
            return;
        }
        if (entityList.isEmpty()) {
            if (isFirst || isHeadFresh) {
                showEmptyPage(Text.TIP_NO_CONTENT, v -> onFirstLoading());
            } else {
                adapter.getLoadMoreModule().loadMoreEnd();
            }
            return;
        }
        //创建新list以处理数据
        List<Entity> list = new ArrayList<>(entityList);
        if (!isFirst && !isHeadFresh) {
            list.removeAll(lastList);
        }
        onComplete(list);
        loadUrl = SourceHelper.getNextUrl(loadUrl);
        lastList = new ArrayList<>(entityList);
        showContentPage();
    }
}