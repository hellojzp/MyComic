package com.qc.common.ui.fragment;

import android.view.Gravity;
import android.view.View;

import com.qc.common.en.TextEnum;
import com.qc.common.en.data.Text;
import com.qc.common.util.EntityUtil;
import com.qc.mycomic.R;
import com.qmuiteam.qmui.alpha.QMUIAlphaImageButton;
import com.qmuiteam.qmui.qqface.QMUIQQFaceView;
import com.qmuiteam.qmui.util.QMUIColorHelper;
import com.qmuiteam.qmui.widget.popup.QMUIPopup;

import java.util.ArrayList;
import java.util.List;

import the.one.base.model.PopupItem;
import the.one.base.ui.fragment.BaseFragment;
import the.one.base.ui.fragment.BaseTitleTabFragment;
import the.one.base.util.QMUIPopupUtil;

/**
 * @author LuQiChuang
 * @desc
 * @date 2021/6/9 16:26
 * @ver 1.0
 */
public class ShelfFragment extends BaseTitleTabFragment {

    private QMUIQQFaceView mTitle;
    private QMUIAlphaImageButton mSettingIcon;
    private QMUIPopup mSettingPopup;

    private final String[] tabs = (String[]) TextEnum.SHELF_TAB_BARS.getValue();
    private final String[] menus = (String[]) TextEnum.SHELF_MENUS.getValue();

    @Override
    protected boolean isAdjustMode() {
        return true;
    }

    @Override
    protected boolean showElevation() {
        return true;
    }

    @Override
    protected boolean isFoldTitleBar() {
        return true;
    }

    @Override
    protected int getContentViewId() {
        return R.layout.layout_title_tab;
    }

    @Override
    protected void onScrollChanged(float percent) {
        mTitle.setTextColor(QMUIColorHelper.setColorAlpha(getColor(R.color.qmui_config_color_gray_1), percent));
        mSettingIcon.setAlpha(percent);
    }

    @Override
    protected void initView(View rootView) {
        super.initView(rootView);
        showLoadingPage();
        mTitle = mTopLayout.setTitle(MyHomeFragment.HOME_TAB_BARS[0]);
        mTopLayout.setNeedChangedWithTheme(false);
        mTopLayout.setTitleGravity(Gravity.CENTER);
        mTitle.setTextColor(getColor(R.color.qmui_config_color_gray_1));
        mTitle.getPaint().setFakeBoldText(true);

        mSettingIcon = mTopLayout.addRightImageButton(R.drawable.ic_baseline_menu_24, R.id.topbar_right_button1);
        mSettingIcon.setOnClickListener(v -> {
            showSettingPopup();
        });
        startInit();
    }

    @Override
    protected void onLazyInit() {
    }

    private void showSettingPopup() {
        if (null == mSettingPopup) {
            mSettingPopup = QMUIPopupUtil.createListPop(_mActivity, menus, (adapter, view, position) -> {
                ShelfItemFragment fragment = (ShelfItemFragment) fragments.get(INDEX);
                if (position == 0) {
                    fragment.startCheckUpdate();
                } else if (position == 1) {
                    String screenTitle = Text.SCREEN_TITLE;
                    String screenCancel = Text.SCREEN_TITLE_CANCEL;
                    if (screenTitle.equals(menus[position])) {
                        fragment.screen(true);
                        menus[1] = screenCancel;
                    } else {
                        fragment.screen(false);
                        menus[1] = screenTitle;
                    }
                    List items = new ArrayList<>();
                    for (String menu : menus) {
                        items.add(new PopupItem(menu));
                    }
                    adapter.setList(items);
                } else if (position == 2) {
                    fragment.importEntity();
                } else if (position == 3) {
                    fragment.addEntityInfo();
                } else if (position == 4) {
                }
                mSettingPopup.dismiss();
            });
        }
        mSettingPopup.show(mSettingIcon);
    }

    @Override
    protected void addTabs() {
        for (String tab : tabs) {
            addTab(tab);
        }
    }

    @Override
    protected void addFragment(ArrayList<BaseFragment> fragments) {
        fragments.add(ShelfItemFragment.getInstance(EntityUtil.STATUS_FAV));
        fragments.add(ShelfItemFragment.getInstance(EntityUtil.STATUS_HIS));
    }

}
