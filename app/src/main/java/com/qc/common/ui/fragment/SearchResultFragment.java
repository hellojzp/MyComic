package com.qc.common.ui.fragment;

import android.os.Bundle;
import android.view.Gravity;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.qc.common.en.SettingEnum;
import com.qc.common.en.data.Data;
import com.qc.common.en.data.Text;
import com.qc.common.ui.adapter.SearchAdapter;
import com.qc.common.ui.presenter.SearchPresenter;
import com.qc.common.ui.view.SearchView;
import com.qc.common.util.EntityUtil;
import com.qc.common.util.SettingUtil;
import com.qc.common.util.SourceUtil;
import com.qc.mycomic.R;
import com.qmuiteam.qmui.qqface.QMUIQQFaceView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import the.one.base.ui.fragment.BaseListFragment;
import the.one.base.ui.presenter.BasePresenter;
import the.one.base.util.QMUIDialogUtil;
import top.luqichuang.common.model.Entity;

/**
 * @author LuQiChuang
 * @desc
 * @date 2021/6/11 15:32
 * @ver 1.0
 */
public class SearchResultFragment extends BaseListFragment<Entity> implements SearchView {

    private SearchPresenter presenter = new SearchPresenter();

    private String searchString;
    private int count = 0;
    private int size = SourceUtil.size();
    private List<String> errorList = new ArrayList<>();

    public static SearchResultFragment getInstance(String searchString) {
        SearchResultFragment fragment = new SearchResultFragment();
        Bundle bundle = new Bundle();
        bundle.putString("searchString", searchString);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        this.searchString = (String) getArguments().get("searchString");
        super.onCreate(savedInstanceState);
    }

    @Override
    protected BaseQuickAdapter getAdapter() {
        return new SearchAdapter(R.layout.item_search);
    }

    @Override
    protected void initView(View rootView) {
        super.initView(rootView);
        showLoadingPage();

        QMUIQQFaceView mTitle = mTopLayout.setTitle(Text.SEARCH_RESULT + ":" + searchString);
        mTopLayout.setTitleGravity(Gravity.CENTER);
        mTitle.setTextColor(getColor(R.color.qmui_config_color_gray_1));
        mTitle.getPaint().setFakeBoldText(true);
        addTopBarBackBtn();
        requestServer();
    }

    @Override
    protected void onLazyInit() {
    }

    @Override
    protected void requestServer() {
        count = 0;
        presenter.search(searchString);
        showContentPage();
        initProgressDialog();
        String msg = String.format(Locale.CHINA, Text.FORMAT_SEARCH_PROGRESS, count, size);
        showProgressDialog(count, size, msg);
    }

    @Override
    protected int getContentViewId() {
        return R.layout.layout_recycle_view;
    }

    @Override
    public void onItemClick(@NonNull BaseQuickAdapter<?, ?> adapter, @NonNull View view, int position) {
        Entity entity = (Entity) adapter.getData().get(position);
        Data.toStatus = Data.RANK_TO_CHAPTER;
        Data.setEntity(entity);
        startFragment(new ChapterFragment());
    }

    @Override
    public boolean onItemLongClick(@NonNull BaseQuickAdapter adapter, @NonNull View view, int position) {
        return false;
    }

    @Override
    protected void initAdapter() {
        super.initAdapter();
        adapter.getLoadMoreModule().setOnLoadMoreListener(null);
    }

    @Override
    public BasePresenter getPresenter() {
        return presenter;
    }

    /**
     * @param list     list
     * @param sourceId sourceId
     * @return List<Entity>
     * @desc 为搜索结果排序：同名 > 源数量 > 其他
     */
    private List<Entity> updateList(List<Entity> list, int sourceId) {
        Map<Integer, Integer> map = new HashMap<>();
        List<Entity> nList = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            Entity entity = list.get(i);
            String title = entity.getTitle();
            if (title == null) {
                continue;
            }
            EntityUtil.changeInfo(entity, sourceId);
            if (title.equals(searchString)) {
                map.put(i, 1000 + entity.getInfoList().size());
            } else if (title.contains(searchString)) {
                map.put(i, 100 + entity.getInfoList().size());
            } else {
                map.put(i, entity.getInfoList().size());
            }
        }
        List<Map.Entry<Integer, Integer>> eList = new ArrayList<>();
        for (Map.Entry<Integer, Integer> entry : map.entrySet()) {
            int index = -1;
            for (int i = 0; i < eList.size(); i++) {
                Map.Entry<Integer, Integer> e = eList.get(i);
                if (entry.getValue() > e.getValue()) {
                    index = i;
                    break;
                }
            }
            if (index == -1) {
                eList.add(entry);
            } else {
                eList.add(index, entry);
            }
        }
        for (Map.Entry<Integer, Integer> entry : eList) {
            nList.add(list.get(entry.getKey()));
        }
        return nList;
    }

    @Override
    public void searchComplete(List<Entity> entityList, String sourceName) {
        if (sourceName != null) {
            errorList.add(sourceName);
        }
        if (++count == size) {
            int sourceId;
            if (Data.contentCode == Data.COMIC_CODE) {
                sourceId = (int) SettingUtil.getSettingKey(SettingEnum.DEFAULT_COMIC_SOURCE);
            } else if (Data.contentCode == Data.NOVEL_CODE) {
                sourceId = (int) SettingUtil.getSettingKey(SettingEnum.DEFAULT_NOVEL_SOURCE);
            } else {
                sourceId = (int) SettingUtil.getSettingKey(SettingEnum.DEFAULT_VIDEO_SOURCE);
            }
            List<Entity> list = updateList(entityList, sourceId);
            onComplete(list);
            hideProgressDialog();
            if (errorList.isEmpty()) {
                showSuccessTips(Text.SEARCH_COMPLETE);
            } else {
                StringBuilder tip = new StringBuilder();
                for (String s : errorList) {
                    tip.append(s).append("\n");
                }
                String msg = String.format(Locale.CHINA, Text.FORMAT_SEARCH_RESULT, errorList.size(), tip);
                QMUIDialogUtil.showSimpleDialog(getContext(), Text.SEARCH_RESULT, msg);
                errorList.clear();
            }
        } else {
            String msg = String.format(Locale.CHINA, Text.FORMAT_SEARCH_PROGRESS, count, size);
            showProgressDialog(count, size, msg);
        }
    }
}
