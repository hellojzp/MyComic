package com.qc.common.ui.presenter;

import com.qc.common.ui.view.ChapterView;
import com.qc.common.util.DBUtil;
import com.qc.common.util.SourceUtil;

import java.util.List;
import java.util.Map;

import io.reactivex.android.schedulers.AndroidSchedulers;
import okhttp3.Request;
import the.one.base.ui.presenter.BasePresenter;
import top.luqichuang.common.model.Entity;
import top.luqichuang.common.model.EntityInfo;
import top.luqichuang.common.model.Source;
import top.luqichuang.common.self.CommonCallback;
import top.luqichuang.common.util.NetUtil;

/**
 * @author LuQiChuang
 * @desc
 * @date 2021/6/10 17:52
 * @ver 1.0
 */
public class ChapterPresenter extends BasePresenter<ChapterView> {

    public void load(Entity entity) {
        Source<EntityInfo> source = SourceUtil.getSource(entity.getSourceId());
        EntityInfo info = entity.getInfo();
        Request request = source.getDetailRequest(info.getDetailUrl());
        NetUtil.startLoad(request, new CommonCallback(source, Source.DETAIL) {
            @Override
            public void onFailure(String errorMsg) {
                if (info.getSourceId() == entity.getInfo().getSourceId()) {
                    onEnd(errorMsg);
                }
            }

            @Override
            public void onResponse(String html, Map<String, Object> map) {
                SourceUtil.setInfoDetail(source, info, html, map);
                if (info.getSourceId() == entity.getInfo().getSourceId()) {
                    onEnd(null);
                }
                DBUtil.saveInfoData(info);
            }

            private void onEnd(String errorMsg) {
                ChapterView view = getView();
                AndroidSchedulers.mainThread().scheduleDirect(() -> {
                    if (view != null) {
                        view.loadComplete(errorMsg);
                    }
                });
            }
        });
    }

    public void updateSource(Entity entity) {
        List<Source<EntityInfo>> sourceList = SourceUtil.getSourceList();
        for (Source<EntityInfo> source : sourceList) {
            boolean exist = false;
            List<? extends EntityInfo> list = entity.getInfoList();
            for (EntityInfo info : list) {
                if (info.getSourceId() == source.getSourceId()) {
                    exist = true;
                    break;
                }
            }
            if (exist) {
                ChapterView view = getView();
                AndroidSchedulers.mainThread().scheduleDirect(() -> {
                    if (view != null) {
                        view.updateSourceComplete(null);
                    }
                });
                continue;
            }
            Request request = source.getSearchRequest(entity.getTitle());
            NetUtil.startLoad(request, new CommonCallback(source, Source.SEARCH) {
                @Override
                public void onFailure(String errorMsg) {
                    onEnd(null);
                }

                @Override
                public void onResponse(String html, Map<String, Object> map) {
                    List<EntityInfo> infoList = SourceUtil.getInfoList(source, html);
                    onEnd(infoList);
                }

                private void onEnd(List<EntityInfo> infoList) {
                    ChapterView view = getView();
                    AndroidSchedulers.mainThread().scheduleDirect(() -> {
                        if (view != null) {
                            view.updateSourceComplete(infoList);
                        }
                    });
                }
            });
        }
    }
}
