package com.qc.common.ui.fragment;

import android.content.Intent;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.RecyclerView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.qc.common.en.SettingEnum;
import com.qc.common.en.data.Data;
import com.qc.common.en.data.Text;
import com.qc.common.ui.activity.GsyVideoActivity;
import com.qc.common.ui.adapter.ChapterItemAdapter;
import com.qc.common.util.DBUtil;
import com.qc.common.util.EntityUtil;
import com.qc.common.util.SettingUtil;
import com.qc.mycomic.R;
import com.qmuiteam.qmui.util.QMUIDisplayHelper;

import java.util.ArrayList;
import java.util.List;

import the.one.base.ui.fragment.BaseListFragment;
import the.one.base.ui.presenter.BasePresenter;
import the.one.base.widge.decoration.SpacesItemDecoration;
import top.luqichuang.common.model.ChapterInfo;
import top.luqichuang.common.model.Entity;

/**
 * @author LuQiChuang
 * @desc
 * @date 2021/6/24 12:53
 * @ver 1.0
 */
public class ChapterItemFragment extends BaseListFragment<ChapterInfo> {

    private List<ChapterInfo> list;
    private Entity entity;

    private RecyclerView.ItemDecoration decorationList;
    private RecyclerView.ItemDecoration decorationGrid;

    public ChapterItemFragment() {
        this.entity = Data.getEntity();
        this.adapter = new ChapterItemAdapter();
    }

    @Override
    protected int getContentViewId() {
        return R.layout.layout_recycle_view;
    }

    @Override
    protected int setType() {
        return Data.isGrid ? TYPE_GRID : TYPE_LIST;
    }

    @Override
    protected int setColumn() {
        return 3;
    }

    @Override
    protected void initView(View rootView) {
        int space = QMUIDisplayHelper.dp2px(_mActivity, setSpacing());
        decorationList = new DividerItemDecoration(_mActivity, RecyclerView.VERTICAL);
        decorationGrid = new SpacesItemDecoration(setColumn(), adapter.getHeaderLayoutCount(), space);
        super.initView(rootView);
        mTopLayout.setVisibility(View.GONE);
        requestServer();
    }

    @Override
    protected void initRecycleView(RecyclerView recycleView, int type, BaseQuickAdapter adapter) {
        if (isNeedSpace()) {
            recycleView.removeItemDecoration(decorationList);
            recycleView.removeItemDecoration(decorationGrid);
            recycleView.addItemDecoration(decorationGrid);
        } else {
            recycleView.removeItemDecoration(decorationList);
            recycleView.removeItemDecoration(decorationGrid);
            recycleView.addItemDecoration(decorationList);
        }
        recycleView.setLayoutManager(getLayoutManager(type));
        if (null != getOnScrollListener())
            recycleView.addOnScrollListener(getOnScrollListener());
        recycleView.setAdapter(adapter);
    }

    @Override
    protected void initAdapter() {
        super.initAdapter();
        adapter.getLoadMoreModule().setOnLoadMoreListener(null);
    }

    public void changeAdapter() {
        showLoadingPage();
        adapter = new ChapterItemAdapter();
        initAdapter();
        initRecycleView(recycleView, setType(), adapter);
        requestServer();
    }

    @Override
    protected void onLazyInit() {
    }

    @Override
    protected BaseQuickAdapter getAdapter() {
        return adapter;
    }

    @Override
    protected void requestServer() {
        loadComplete();
    }

    @Override
    public void onRefresh() {
        page = 1;
        isHeadFresh = true;
        ChapterFragment fragment = (ChapterFragment) getParentFragment();
        if (fragment != null) {
            entity.getInfo().getChapterInfoMap().clear();
            entity.getInfo().getChapterInfoList().clear();
            fragment.itemLoading();
            fragment.requestServer();
        } else {
            requestServer();
        }
    }

    public void loadComplete() {
        if (entity != null && adapter != null) {
            if (list != null && !list.isEmpty()) {
                EntityUtil.updateChapterId(entity);
                onFirstComplete(list);
                adapter.notifyDataSetChanged();
            } else if (list != null) {
                showEmptyPage(Text.TIP_NO_CHAPTER);
            } else {
                showLoadingPage();
            }
        }
    }

    @Override
    public void onItemClick(@NonNull BaseQuickAdapter<?, ?> adapter, @NonNull View view,
                            int position) {
        startPosition(position);
    }

    @Override
    public boolean onItemLongClick(@NonNull BaseQuickAdapter adapter, @NonNull View view,
                                   int position) {
        return false;
    }

    @Override
    public BasePresenter getPresenter() {
        return null;
    }

    public void setList(List<ChapterInfo> list) {
        if (this.list == null) {
            this.list = new ArrayList<>();
        }
        this.list.clear();
        this.list.addAll(list);
        loadComplete();
    }

    public void updateData() {
        if (adapter != null) {
            adapter.notifyDataSetChanged();
        }
    }

    public void startPosition(int position) {
        startId(list.get(position).getId());
    }

    public void startId(int chapterId) {
        if (chapterId != entity.getInfo().getCurChapterId()) {
            Data.chapterNum = 0;
            entity.getInfo().setChapterNum(0);
        } else {
            Data.chapterNum = entity.getInfo().getChapterNum();
        }
        int sId;
        if (EntityUtil.checkChapterId(entity.getInfo(), chapterId)) {
            sId = chapterId;
        } else if (EntityUtil.checkChapterId(entity.getInfo(), entity.getInfo().getCurChapterId())) {
            sId = entity.getInfo().getCurChapterId();
        } else {
            sId = 0;
        }
        EntityUtil.initChapterId(entity.getInfo(), sId);
        start();
    }

    public void start() {
        updateData();
        Data.setChapterInfoList(this.list);
        if (Data.contentCode == Data.COMIC_CODE) {
            int readMode = (int) SettingUtil.getSettingKey(SettingEnum.READER_MODE);
            if (readMode == Data.READER_MODE_H_R) {
                startFragment(new ComicReaderFragment2());
            } else if (readMode == Data.READER_MODE_H_L) {
                startFragment(new ComicReaderFragment3());
            } else {
                startFragment(new ComicReaderFragment());
            }
        } else if (Data.contentCode == Data.NOVEL_CODE) {
            startFragment(new NovelReaderFragment());
        } else {
//            Intent intent = new Intent(_mActivity, VideoPlayerActivity.class);
            Intent intent = new Intent(_mActivity, GsyVideoActivity.class);
            startActivity(intent);
        }
        EntityUtil.first(entity);
        DBUtil.saveAll(entity);
    }
}
