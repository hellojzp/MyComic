package com.qc.common.en;


import com.qc.common.en.data.Data;

import java.util.LinkedHashMap;

import top.luqichuang.common.en.CSourceEnum;
import top.luqichuang.common.en.NSourceEnum;
import top.luqichuang.common.en.VSourceEnum;

/**
 * @author LuQiChuang
 * @desc
 * @date 2021/1/16 21:45
 * @ver 1.0
 */
public enum SettingEnum {

    PRELOAD_NUM("preloadNum", 10000),
    READ_CONTENT("readContent", Data.COMIC_CODE),
    IS_FULL_SCREEN("isFullScreen", true),
    IS_GRID("isGrid", true),
    NOVEL_FONT_SIZE("novelFontSize", 20),
    NOVEL_AUTO_SPEED("novelAutoSpeed", 4),
    VIDEO_PROGRESS("videoProgress", new LinkedHashMap<>()),
    DEFAULT_COMIC_SOURCE("defaultSource", CSourceEnum.BAO_ZI.ID),
    DEFAULT_NOVEL_SOURCE("defaultNSource", NSourceEnum.AI_YUE.ID),
    DEFAULT_VIDEO_SOURCE("defaultVSource", VSourceEnum.AI_YUN.ID),
    COMIC_SOURCE_OPEN("comicSourceOpen", CSourceEnum.getMAP().keySet()),
    NOVEL_SOURCE_OPEN("novelSourceOpen", NSourceEnum.getMAP().keySet()),
    VIDEO_SOURCE_OPEN("videoSourceOpen", VSourceEnum.getMAP().keySet()),
    COMIC_SOURCE_TOTAL("comicSourceTotal", CSourceEnum.getMAP().keySet()),
    NOVEL_SOURCE_TOTAL("novelSourceTotal", NSourceEnum.getMAP().keySet()),
    VIDEO_SOURCE_TOTAL("videoSourceTotal", VSourceEnum.getMAP().keySet()),
    READER_MODE("readerMode", Data.READER_MODE_V),
    ;

    public final String KEY;
    public final Object DEFAULT_VALUE;

    SettingEnum(String KEY, Object DEFAULT_VALUE) {
        this.KEY = KEY;
        this.DEFAULT_VALUE = DEFAULT_VALUE;
    }
}
