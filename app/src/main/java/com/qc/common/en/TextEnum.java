package com.qc.common.en;

import com.qc.common.en.data.Data;

/**
 * @author 18472
 * @desc
 * @date 2024/2/19 16:49
 * @ver 1.0
 */
public enum TextEnum {

    HOME_TAB_BARS(new String[]{
            "我的画架",
            "搜索漫画",
            "个人中心",
    }, new String[]{
            "我的书架",
            "搜索小说",
            "个人中心",
    }, new String[]{
            "我的番剧",
            "搜索番剧",
            "个人中心",
    }),
    SHELF_TAB_BARS(new String[]{
            "收藏" + Data.contentStr,
            "历史" + Data.contentStr,
    }),
    SHELF_MENUS(new String[]{
            "检查更新",
            "筛选" + Data.contentStr,
            "导入" + Data.contentStr,
            "批量添加源",
    }),
    SHELF_ITEM_CENTER(new String[]{
            "1、查看信息",
            "2、切换" + Data.contentStr + "源",
            "3、删除" + Data.contentStr + "",
    }),
    SHELF_ITEM_SCREEN(new String[]{
            "未读完" + Data.contentStr,
            "据标题筛选",
    }),
    CHAPTER_MENUS(new String[]{
            "更新" + Data.contentStr + "源",
            "查看信息",
            "访问源网站",
            "切换章节显示"
    }),
    ;

    private final Object VALUE;
    private final Object COMIC_VALUE;
    private final Object NOVEL_VALUE;
    private final Object VIDEO_VALUE;

    TextEnum(Object VALUE) {
        this(VALUE, null, null, null);
    }

    TextEnum(Object COMIC_VALUE, Object NOVEL_VALUE, Object VIDEO_VALUE) {
        this(null, COMIC_VALUE, NOVEL_VALUE, VIDEO_VALUE);
    }

    TextEnum(Object VALUE, Object COMIC_VALUE, Object NOVEL_VALUE, Object VIDEO_VALUE) {
        this.VALUE = VALUE;
        this.COMIC_VALUE = COMIC_VALUE;
        this.NOVEL_VALUE = NOVEL_VALUE;
        this.VIDEO_VALUE = VIDEO_VALUE;
    }

    public Object getValue() {
        if (VALUE != null) {
            return VALUE;
        }
        if (Data.contentCode == Data.COMIC_CODE) {
            return COMIC_VALUE;
        } else if (Data.contentCode == Data.NOVEL_CODE) {
            return NOVEL_VALUE;
        } else {
            return VIDEO_VALUE;
        }
    }
}
