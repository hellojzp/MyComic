package com.qc.common.en.data;

import com.qc.common.en.SettingEnum;
import com.qc.common.util.RestartUtil;
import com.qc.common.util.SettingUtil;

import java.util.List;

import the.one.base.util.SdCardUtil;
import top.luqichuang.common.model.ChapterInfo;
import top.luqichuang.common.model.Content;
import top.luqichuang.common.model.Entity;

/**
 * @author LuQiChuang
 * @desc
 * @date 2022/10/30 11:48
 * @ver 1.0
 */
public class Data {

    /*=============================================================================*/
    /* 常量 */
    /* App Path */
    public static final String SD_CARD_PATH = SdCardUtil.getNormalSDCardPath();
    public static final String DIR_APP_NAME = "/MyComic";
    public static final String DIR_IMG = "/Image";
    public static final String DIR_AUTO_SAVE = "/AutoBackup";
    public static final String DIR_SAVE = "/backup_comic.db";
    public static final String DB_FILE_NAME = "comic.db";

    /* 标识App显示的数据内容 */
    public static final int COMIC_CODE = 1;
    public static final int NOVEL_CODE = 2;
    public static final int VIDEO_CODE = 3;

    /* toStatus */
    public static final int NORMAL = 0;
    public static final int READER_TO_CHAPTER = 1;
    public static final int SEARCH_TO_CHAPTER = 2;
    public static final int RANK_TO_CHAPTER = 3;
    public static final int TO_IMPORT = 4;
    public static final int TO_IMPORT_SUCCESS = 5;

    /* SCREEN */
    public static final int SCREEN_0 = 0;
    public static final int SCREEN_1 = 1;

    /* ReaderMode */
    public static final int READER_MODE_V = 0;
    public static final int READER_MODE_H_R = 1;
    public static final int READER_MODE_H_L = 2;

    /*=============================================================================*/
    /* 公共静态变量 */
    public static String appPath = SD_CARD_PATH + DIR_APP_NAME;
    public static int toStatus = Data.NORMAL;
    public static boolean isLight = true;
    public static boolean isFull = (boolean) SettingUtil.getSettingKey(SettingEnum.IS_FULL_SCREEN);
    public static boolean isGrid = (boolean) SettingUtil.getSettingKey(SettingEnum.IS_GRID);
    public static int contentCode = (int) SettingUtil.getSettingKey(SettingEnum.READ_CONTENT);
    public static String contentStr = SettingUtil.getSettingDesc(SettingEnum.READ_CONTENT);
    public static int videoSpeed = 2;
    public static int chapterNum = 0;
    public static long time = 0;

    /*=============================================================================*/
    /* 私有静态变量 */
    private static Entity entity;
    private static Content content;
    private static List<ChapterInfo> chapterInfoList;

    public static Entity getEntity() {
        if (entity == null) {
            RestartUtil.restart();
        }
        return entity;
    }

    public static Content getContent() {
        if (content == null) {
            RestartUtil.restart();
        }
        return content;
    }

    public static List<ChapterInfo> getChapterInfoList() {
        if (chapterInfoList == null) {
            RestartUtil.restart();
        }
        return chapterInfoList;
    }

    public static void setEntity(Entity entity) {
        Data.entity = entity;
    }

    public static void setContent(Content content) {
        Data.content = content;
    }

    public static void setChapterInfoList(List<ChapterInfo> chapterInfoList) {
        Data.chapterInfoList = chapterInfoList;
    }
    /*=============================================================================*/

    public static String getAppPath() {
        return appPath;
    }

    public static String getImgPath() {
        return appPath + DIR_IMG;
    }

    public static String getAutoSavePath() {
        return appPath + DIR_AUTO_SAVE;
    }

    public static String getSavePathName() {
        return appPath + DIR_SAVE;
    }

}
