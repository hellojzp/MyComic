package com.qc.common.en.data;

/**
 * @author 18472
 * @desc
 * @date 2024/2/19 16:35
 * @ver 1.0
 */
public class Text {

    public static final String TOAST_GO_TO_SEARCH = "快去搜索" + Data.contentStr + "吧！";
    public static final String TOAST_LOAD_ING = "正在加载中...";

    public static final String TIP_NO_DATA = "没有" + Data.contentStr;
    public static final String TIP_NO_CHAPTER = "暂无" + Data.contentStr + "章节";
    public static final String TIP_NO_CONTENT = "暂无数据";
    public static final String TIP_LOAD_FAIL = "加载失败";
    public static final String TIP_CHAPTER_FAIL = "章节加载失败";
    public static final String TIP_SEARCH_COMPLETE = "搜索完成";
    public static final String TIP_INPUT_FAIL = "导入" + Data.contentStr + "失败";
    public static final String TIP_INPUT_TITLE = "请输入" + Data.contentStr + "标题";
    public static final String TIP_INPUT_URL = "请输入" + Data.contentStr + "主页url链接";
    public static final String TIP_SEARCH = "请输入搜索内容";
    public static final String TIP_NO_CHAPTER_PREV = "没有上一章";
    public static final String TIP_NO_CHAPTER_NEXT = "没有下一章";

    public static final String OPTION_TITLE = "选项";
    public static final String OPTION_YES = "确认";
    public static final String OPTION_CANCEL = "取消";
    public static final String OPTION_RETRY = "重试";
    public static final String EMPTY = "";

    public static final String TEXT = "正文";
    public static final String INFO = "查看信息";
    public static final String RANK = "排行榜";
    public static final String SEARCH = "搜索";
    public static final String SEARCH_RESULT = "搜索结果";
    public static final String SEARCH_COMPLETE = "搜索完成";
    public static final String SCREEN_TITLE = "筛选" + Data.contentStr;
    public static final String SCREEN_TITLE_CANCEL = "取消筛选";
    public static final String CHANGE_SOURCE = "切换" + Data.contentStr + "源";
    public static final String DELETE_TITLE = "删除" + Data.contentStr;
    public static final String DELETE_TIP = "是否删除该" + Data.contentStr + "？";
    public static final String UPDATE_COMPLETE = "检查更新完成";
    public static final String UPDATE_COMPLETE_RESULT = "检查更新结果";
    public static final String INPUT_TITLE = "导入" + Data.contentStr;
    public static final String INPUT_FAIL = "url解析失败！";
    public static final String CONTENT_FAIL = "解析失败！";
    public static final String SOURCE = "数据源";
    public static final String SOURCE_UPDATE_TIP = "是否更新" + Data.contentStr + "源？";
    public static final String SOURCE_CHANGE = "切换" + Data.contentStr + "源";
    public static final String ADD_COMPLETE = "添加完成";

    public static final String DETAIL = Data.contentStr + "详情";
    public static final String READ_START = "开始阅读";
    public static final String READ_CONTINUE = "继续阅读";
    public static final String FAV_YES = "已收藏";
    public static final String FAV_NO = "未收藏";

    public static final String TIME_SUN = "日间";
    public static final String TIME_MOON = "夜间";

    public static final String FORMAT_UPDATE_PROGRESS = "正在检查更新：%d/%d";
    public static final String FORMAT_UPDATE_RESULT = "检查更新完毕，失败数：%d\n%s";
    public static final String FORMAT_SEARCH_PROGRESS = "正在搜索：%d/%d";
    public static final String FORMAT_SEARCH_RESULT = "搜索完毕，失败" + Data.contentStr + "源数：%d\n%s";
    public static final String FORMAT_ADD_PROGRESS = "添加中：%d/%d";
    public static final String FORMAT_SOURCE_UPDATE_PROGRESS = "正在更新" + Data.contentStr + "源：%d/%d";
    public static final String FORMAT_CHAPTER_SOURCE_COUNT = "(%d)";
    public static final String FORMAT_CHAPTER_READ_NUM = "%s - %02d页";
    public static final String FORMAT_READER_INFO_CHAPTER = "%d章/%d章";


}
