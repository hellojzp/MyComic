package top.luqichuang.common;

import org.junit.Test;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Headers;
import okhttp3.Request;
import okhttp3.Response;
import top.luqichuang.common.model.Source;
import top.luqichuang.common.tst.BaseSourceTest;
import top.luqichuang.common.util.NetUtil;
import top.luqichuang.mycomic.model.ComicInfo;
import top.luqichuang.mycomic.source.BaoZi;

/**
 * @author LuQiChuang
 * @desc
 * @date 2021/7/9 17:24
 * @ver 1.0
 */
public class ComicTest extends BaseSourceTest {

    /**
     * 设置需要测试的漫画源
     *
     * @return Source<ComicInfo>
     */
    @Override
    protected Source<? extends ComicInfo> getSource() {
        return new BaoZi();
    }

    @Test
    @Override
    public void testRequest() {
        String title = "我的";
        String dUrl = "https://cn.baozimh.com/comic/kaijuyizuoshan-yulemeicuo";
        String cUrl = "https://cn.webmota.com/comic/chapter/yaoshenji-taxuedongman/0_657.html";
//        title = "妖神记";
//        title = "我的天劫女友";
//        title = "大王饶命";
//        title = "今天开始做明星";
//        title = "开局一座山";
//        title = "放开那个女巫";

//        autoTest(title);//测试对应漫画
        autoTest();//测试默认漫画

//        allTest();//测试所有漫画源

//        testSearchRequest();//网络请求 测试默认漫画名搜索
//        testSearchRequest(title);//网络请求 测试对应漫画名搜索
//        testSearch();//根据网络请求得到的本地文件测试搜索
//        testDetailRequest(dUrl);//网络请求 测试详情页
//        testDetail();//本地文件 测试详情页
//        testContentRequest(cUrl);//网络请求 测试内容页
//        testContent();//本地文件 测试内容页
//        testRankMap();//测试map
//        testRankRequest();//网络请求 测试排行页
//        testRank();//本地文件 测试排行页
//        autoTest();//一套组合拳从头测到尾

//        tstNet();
    }

    private void tstNet() {
        Request request = NetUtil.getRequest("https://www.sisimanhua.com/search/?keywords=%E6%88%91%E7%9A%84");

        NetUtil.startLoad(request, new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                System.err.println("failure=================");
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                Headers headers = response.headers();
                System.out.println("headers = " + headers);
            }
        });

        try {
            Thread.sleep(100000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
