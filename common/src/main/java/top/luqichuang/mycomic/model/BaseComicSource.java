package top.luqichuang.mycomic.model;

import java.util.HashMap;
import java.util.Map;

import top.luqichuang.common.en.CSourceEnum;
import top.luqichuang.common.model.Source;
import top.luqichuang.common.util.NetUtil;

/**
 * @author LuQiChuang
 * @desc
 * @date 2021/6/10 15:26
 * @ver 1.0
 */
public abstract class BaseComicSource implements Source<ComicInfo> {

    @Override
    public int getSourceType() {
        return COMIC;
    }

    public abstract CSourceEnum getCSourceEnum();

    @Override
    public final int getSourceId() {
        return getCSourceEnum().ID;
    }

    @Override
    public final String getSourceName() {
        return getCSourceEnum().NAME;
    }

    @Override
    public Map<String, String> getImageHeaders() {
        Map<String, String> headers = new HashMap<>();
        headers.put("Referer", getIndex());
        headers.put("User-Agent", NetUtil.USER_AGENT_WEB);
        return headers;
    }
}
