package top.luqichuang.mycomic.source;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import okhttp3.Request;
import top.luqichuang.common.en.CSourceEnum;
import top.luqichuang.common.jsoup.JsoupNode;
import top.luqichuang.common.jsoup.JsoupStarter;
import top.luqichuang.common.model.ChapterInfo;
import top.luqichuang.common.model.Content;
import top.luqichuang.common.util.NetUtil;
import top.luqichuang.common.util.SourceHelper;
import top.luqichuang.common.util.StringUtil;
import top.luqichuang.mycomic.model.BaseComicSource;
import top.luqichuang.mycomic.model.ComicInfo;

/**
 * @author LuQiChuang
 * @desc
 * @date 2022/6/1 11:50
 * @ver 1.0
 */
public class BaoZi extends BaseComicSource {

    /**
     * 设置对应枚举
     *
     * @return CSourceEnum
     */
    @Override
    public CSourceEnum getCSourceEnum() {
        return CSourceEnum.BAO_ZI;
    }

    /**
     * 设置主页url
     *
     * @return String
     */
    @Override
    public String getIndex() {
        return "https://cn.baozimh.com";
    }

    /**
     * 根据搜索关键词拼凑出搜索url，返回request
     *
     * @param searchString searchString
     * @return Request
     */
    @Override
    public Request getSearchRequest(String searchString) {
        String url = getIndex() + "/search?q=" + searchString;
        return NetUtil.getRequest(url);
    }

    /**
     * 为应对某些多次访问才可以获取数据的网页
     * 调用时间在获取request(getSearchRequest...)之前
     *
     * @param html 获取的网页源码
     * @param tag  访问页面标识(Source.SEARCH DETAIL...)
     * @param data 访问页面后存留的信息
     * @param map  给后续调用保存的主要信息
     * @return Request
     */
    @Override
    public Request buildRequest(String html, String tag, Map<String, Object> data, Map<String, Object> map) {
        if (CONTENT.equals(tag)) {
            JsoupNode node = new JsoupNode(html);
            String nextUrl = node.href("div.comic-chapter>div.next_chapter a");
            String text = node.ownText("div.comic-chapter>div.next_chapter a");
            try {
                List<String> list;
                if (map.get("list") != null) {
                    list = new ArrayList<>((List<String>) map.get("list"));
                } else {
                    list = new ArrayList<>();
                }
                Elements elements = node.getElements("ul.comic-contain amp-img");
                for (Element element : elements) {
                    node.init(element);
                    String url = node.src("amp-img");
                    String server = "https://s2.baozimh.com/";
                    if (url != null) {
                        String s = StringUtil.match("(.*?)://(.*?)/(.+)", url, 3);
                        if (s != null) {
                            url = server + s;
                        }
                    }
                    if (!list.contains(url)) {
                        list.add(url);
                    }
                }
                map.put("list", list);
                if (nextUrl != null && "点击进入下一页".equals(text)) {
                    return NetUtil.getRequest(nextUrl);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return super.buildRequest(html, tag, data, map);
    }

    /**
     * 返回搜索页面的所有漫画链表
     *
     * @param html html
     * @return List<ComicInfo>
     */
    @Override
    public List<ComicInfo> getInfoList(String html) {
        //重写JsoupStarter的dealElement方法
        //设置漫画的名称、作者等信息
        JsoupStarter<ComicInfo> starter = new JsoupStarter<ComicInfo>() {
            @Override
            protected ComicInfo dealElement(JsoupNode node) {
                String title = node.title("a");
                String author = node.ownText("a.comics-card__info small");
                String updateTime = null;
                String updateChapter = null;
                String imgUrl = node.src("amp-img");
                String detailUrl = getIndex() + node.href("a");
                return new ComicInfo(getSourceId(), title, author, detailUrl, imgUrl, updateTime, updateChapter);
            }
        };
        //startElements方法 返回漫画链表
        return starter.startElements(html, "div.comics-card");
    }

    @Override
    public void setInfoDetail(ComicInfo info, String html, Map<String, Object> map) {
        JsoupStarter<ChapterInfo> starter = new JsoupStarter<ChapterInfo>() {
            //重写isDESC方法 设置章节顺序
            @Override
            protected boolean isDESC() {
                return false;
            }

            //设置漫画基本信息
            @Override
            protected void dealInfo(JsoupNode node) {
                String title = node.ownText("h1.comics-detail__title");
                String imgUrl = node.src("div.de-info__box amp-img");
                String author = node.ownText("h2.comics-detail__author");
                String intro = node.ownText("p.comics-detail__desc");
                String updateStatus = node.ownText("div.tag-list span");
                String updateTime = node.ownText("div.supporting-text em");
                info.setDetail(title, imgUrl, author, updateTime, updateStatus, intro);
            }

            //设置漫画章节信息
            @Override
            protected ChapterInfo dealElement(JsoupNode node) {
                String title = node.ownText("span");
                String chapterUrl = getIndex() + node.href("a");
                return new ChapterInfo(title, chapterUrl);
            }
        };
        //使dealInfo方法生效
        starter.startInfo(html);
        //使dealElement方法生效
        //将list存入info中
        List<ChapterInfo> list = starter.startElements(html, "div.pure-g[id] div.comics-chapters");
        if (list.isEmpty()) {
            starter = new JsoupStarter<ChapterInfo>() {
                @Override
                protected ChapterInfo dealElement(JsoupNode node) {
                    String title = node.ownText("span");
                    String chapterUrl = getIndex() + node.href("a");
                    return new ChapterInfo(title, chapterUrl);
                }
            };
            list = starter.startElements(html, "div.l-box div.comics-chapters");
        }
        SourceHelper.initChapterInfoList(info, list);
    }

    /**
     * 根据获取的内容页html解析出对应的图片url
     * 不同网页一般解析方式不同
     *
     * @param html      html
     * @param chapterId 章节ID
     * @param map       buildRequest传入的漫画信息
     * @return List<Content>
     */
    @Override
    public List<Content> getContentList(String html, int chapterId, Map<String, Object> map) {
        List<String> list = null;
        try {
            list = (List<String>) map.get("list");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return SourceHelper.getContentList(list, chapterId);
    }

    /**
     * 设置排行榜的 名称--url map
     *
     * @return Map<String, String>
     */
    @Override
    public Map<String, String> getRankMap() {
        //截取网页一段html 解析出对应map
        String html = "<div style=\"margin-top: 60px;\" data-v-3cb735e6=\"\">\t<div class=\"classify-nav\" data-v-3cb735e6=\"\">\t\t<div class=\"nav\" data-v-3cb735e6=\"\"><a href=\"/classify?type=all&amp;region=cn&amp;state=all&amp;filter=%2a\"\t\t\t\tclass=\"item\" data-v-3cb735e6=\"\">国漫\t\t\t</a><a href=\"/classify?type=all&amp;region=jp&amp;state=all&amp;filter=%2a\" class=\"item\"\t\t\t\tdata-v-3cb735e6=\"\">日本\t\t\t</a><a href=\"/classify?type=all&amp;region=kr&amp;state=all&amp;filter=%2a\" class=\"item\"\t\t\t\tdata-v-3cb735e6=\"\">韩国\t\t\t</a><a href=\"/classify?type=all&amp;region=en&amp;state=all&amp;filter=%2a\" class=\"item\"\t\t\t\tdata-v-3cb735e6=\"\">欧美\t\t\t</a></div>\t</div>\t<div class=\"classify-nav\" data-v-3cb735e6=\"\">\t\t<div class=\"nav pure-form\" data-v-3cb735e6=\"\">\t\t\t<a href=\"/classify?type=all&amp;region=all&amp;state=serial&amp;filter=%2a\" class=\"item\"\t\t\t\tdata-v-3cb735e6=\"\">连载中\t\t\t</a><a href=\"/classify?type=all&amp;region=all&amp;state=pub&amp;filter=%2a\" class=\"item\"\t\t\t\tdata-v-3cb735e6=\"\">已完结\t\t\t</a>\t\t</div>\t</div>\t<div class=\"classify-nav\" data-v-3cb735e6=\"\">\t\t<div class=\"nav\" data-v-3cb735e6=\"\"><a href=\"/classify?type=lianai&amp;region=all&amp;state=all&amp;filter=%2a\"\t\t\t\tclass=\"item\" data-v-3cb735e6=\"\">恋爱\t\t\t</a><a href=\"/classify?type=chunai&amp;region=all&amp;state=all&amp;filter=%2a\" class=\"item\"\t\t\t\tdata-v-3cb735e6=\"\">纯爱\t\t\t</a><a href=\"/classify?type=gufeng&amp;region=all&amp;state=all&amp;filter=%2a\" class=\"item\"\t\t\t\tdata-v-3cb735e6=\"\">古风\t\t\t</a><a href=\"/classify?type=yineng&amp;region=all&amp;state=all&amp;filter=%2a\" class=\"item\"\t\t\t\tdata-v-3cb735e6=\"\">异能\t\t\t</a><a href=\"/classify?type=xuanyi&amp;region=all&amp;state=all&amp;filter=%2a\" class=\"item\"\t\t\t\tdata-v-3cb735e6=\"\">悬疑\t\t\t</a><a href=\"/classify?type=juqing&amp;region=all&amp;state=all&amp;filter=%2a\" class=\"item\"\t\t\t\tdata-v-3cb735e6=\"\">剧情\t\t\t</a><a href=\"/classify?type=kehuan&amp;region=all&amp;state=all&amp;filter=%2a\" class=\"item\"\t\t\t\tdata-v-3cb735e6=\"\">科幻\t\t\t</a><a href=\"/classify?type=qihuan&amp;region=all&amp;state=all&amp;filter=%2a\" class=\"item\"\t\t\t\tdata-v-3cb735e6=\"\">奇幻\t\t\t</a><a href=\"/classify?type=xuanhuan&amp;region=all&amp;state=all&amp;filter=%2a\" class=\"item\"\t\t\t\tdata-v-3cb735e6=\"\">玄幻\t\t\t</a><a href=\"/classify?type=chuanyue&amp;region=all&amp;state=all&amp;filter=%2a\" class=\"item\"\t\t\t\tdata-v-3cb735e6=\"\">穿越\t\t\t</a><a href=\"/classify?type=mouxian&amp;region=all&amp;state=all&amp;filter=%2a\" class=\"item\"\t\t\t\tdata-v-3cb735e6=\"\">冒险\t\t\t</a><a href=\"/classify?type=tuili&amp;region=all&amp;state=all&amp;filter=%2a\" class=\"item\"\t\t\t\tdata-v-3cb735e6=\"\">推理\t\t\t</a><a href=\"/classify?type=wuxia&amp;region=all&amp;state=all&amp;filter=%2a\" class=\"item\"\t\t\t\tdata-v-3cb735e6=\"\">武侠\t\t\t</a><a href=\"/classify?type=gedou&amp;region=all&amp;state=all&amp;filter=%2a\" class=\"item\"\t\t\t\tdata-v-3cb735e6=\"\">格斗\t\t\t</a><a href=\"/classify?type=zhanzheng&amp;region=all&amp;state=all&amp;filter=%2a\" class=\"item\"\t\t\t\tdata-v-3cb735e6=\"\">战争\t\t\t</a><a href=\"/classify?type=rexie&amp;region=all&amp;state=all&amp;filter=%2a\" class=\"item\"\t\t\t\tdata-v-3cb735e6=\"\">热血\t\t\t</a><a href=\"/classify?type=gaoxiao&amp;region=all&amp;state=all&amp;filter=%2a\" class=\"item\"\t\t\t\tdata-v-3cb735e6=\"\">搞笑\t\t\t</a><a href=\"/classify?type=danuzhu&amp;region=all&amp;state=all&amp;filter=%2a\" class=\"item\"\t\t\t\tdata-v-3cb735e6=\"\">大女主\t\t\t</a><a href=\"/classify?type=dushi&amp;region=all&amp;state=all&amp;filter=%2a\" class=\"item\"\t\t\t\tdata-v-3cb735e6=\"\">都市\t\t\t</a><a href=\"/classify?type=zongcai&amp;region=all&amp;state=all&amp;filter=%2a\" class=\"item\"\t\t\t\tdata-v-3cb735e6=\"\">总裁\t\t\t</a><a href=\"/classify?type=hougong&amp;region=all&amp;state=all&amp;filter=%2a\" class=\"item\"\t\t\t\tdata-v-3cb735e6=\"\">后宫\t\t\t</a><a href=\"/classify?type=richang&amp;region=all&amp;state=all&amp;filter=%2a\" class=\"item\"\t\t\t\tdata-v-3cb735e6=\"\">日常\t\t\t</a><a href=\"/classify?type=hanman&amp;region=all&amp;state=all&amp;filter=%2a\" class=\"item\"\t\t\t\tdata-v-3cb735e6=\"\">韩漫\t\t\t</a><a href=\"/classify?type=shaonian&amp;region=all&amp;state=all&amp;filter=%2a\" class=\"item\"\t\t\t\tdata-v-3cb735e6=\"\">少年\t\t\t</a><a href=\"/classify?type=qita&amp;region=all&amp;state=all&amp;filter=%2a\" class=\"item\"\t\t\t\tdata-v-3cb735e6=\"\">其它\t\t\t</a></div>\t</div>\t<div class=\"classify-nav\" data-v-3cb735e6=\"\">\t\t<div class=\"nav\" data-v-3cb735e6=\"\"><a href=\"/classify?type=all&amp;region=all&amp;state=all&amp;filter=ABCD\"\t\t\t\tclass=\"item\" data-v-3cb735e6=\"\">ABCD\t\t\t</a><a href=\"/classify?type=all&amp;region=all&amp;state=all&amp;filter=EFGH\" class=\"item\"\t\t\t\tdata-v-3cb735e6=\"\">EFGH\t\t\t</a><a href=\"/classify?type=all&amp;region=all&amp;state=all&amp;filter=IJKL\" class=\"item\"\t\t\t\tdata-v-3cb735e6=\"\">IJKL\t\t\t</a><a href=\"/classify?type=all&amp;region=all&amp;state=all&amp;filter=MNOP\" class=\"item\"\t\t\t\tdata-v-3cb735e6=\"\">MNOP\t\t\t</a><a href=\"/classify?type=all&amp;region=all&amp;state=all&amp;filter=QRST\" class=\"item\"\t\t\t\tdata-v-3cb735e6=\"\">QRST\t\t\t</a><a href=\"/classify?type=all&amp;region=all&amp;state=all&amp;filter=UVW\" class=\"item\"\t\t\t\tdata-v-3cb735e6=\"\">UVW\t\t\t</a><a href=\"/classify?type=all&amp;region=all&amp;state=all&amp;filter=XYZ\" class=\"item\"\t\t\t\tdata-v-3cb735e6=\"\">XYZ\t\t\t</a><a href=\"/classify?type=all&amp;region=all&amp;state=all&amp;filter=0-9\" class=\"item\"\t\t\t\tdata-v-3cb735e6=\"\">0-9\t\t\t</a></div>\t</div></div>";
        Map<String, String> map = new LinkedHashMap<>();
        JsoupNode node = new JsoupNode(html);
        Elements elements = node.getElements("a");
        for (Element element : elements) {
            node.init(element);
            map.put(node.ownText("a"), getIndex() + node.href("a") + "&page=1");
        }
        return map;
    }

    /**
     * 返回排行榜的漫画链表
     *
     * @param html html
     * @return List<ComicInfo>
     */
    @Override
    public List<ComicInfo> getRankInfoList(String html) {
        return getInfoList(html);
    }
}
