package top.luqichuang.mycomic.source;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import okhttp3.Request;
import top.luqichuang.common.en.CSourceEnum;
import top.luqichuang.common.jsoup.JsoupNode;
import top.luqichuang.common.jsoup.JsoupStarter;
import top.luqichuang.common.model.ChapterInfo;
import top.luqichuang.common.model.Content;
import top.luqichuang.common.util.NetUtil;
import top.luqichuang.common.util.SourceHelper;
import top.luqichuang.common.util.StringUtil;
import top.luqichuang.mycomic.model.BaseComicSource;
import top.luqichuang.mycomic.model.ComicInfo;

/**
 * @author 18472
 * @desc
 * @date 2023/12/24 10:41
 * @ver 1.0
 */
public class BaiManWu extends BaseComicSource {

    @Override
    public CSourceEnum getCSourceEnum() {
        return CSourceEnum.BAI_MAN_WU;
    }

    @Override
    public String getIndex() {
        return "https://www.ql1.net";
    }

    @Override
    public Request getSearchRequest(String searchString) {
        String url = String.format("%s/search/-------.html?wd=%s&submit=", getIndex(), searchString);
        Map<String, String> map = new HashMap<>();
//        map.put("Cookie", "cf_clearance=yzc7t0urW4zIsJk6lb51ePn_0j5DhFrtmELx61fCH48-1704604587-0-2-dc970cb.17b51d43.f4195163-150.0.0");
//        map.put("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/119.0.0.0 Safari/537.36");
        return NetUtil.getRequestByHeader(url, map);
    }

    @Override
    public Request buildRequest(String html, String tag, Map<String, Object> data, Map<String, Object> map) {
        if (CONTENT.equals(tag) && map.isEmpty()) {
            String url = StringUtil.match("var txt_url=\"(.*?)\"; ", html);
            map.put("url", url);
            return NetUtil.getRequest(url);
        }
        return super.buildRequest(html, tag, data, map);
    }

    @Override
    public List<ComicInfo> getInfoList(String html) {
        JsoupStarter<ComicInfo> starter = new JsoupStarter<ComicInfo>() {
            @Override
            protected ComicInfo dealElement(JsoupNode node) {
                String title = node.ownText("h3.title a");
                String author = node.ownText("div.detail p:eq(2)");
                String updateTime;
                String updateChapter = null;
                String imgUrl = node.attr("a.lazyload", "data-original");
                String detailUrl = getIndex() + node.href("a");
                try {
                    updateTime = node.ownText("div.detail p:eq(3)");
                    String[] ss = updateTime.split(" ");
                    updateTime = ss[ss.length - 1];
                } catch (Exception e) {
                    updateTime = null;
                }
                return new ComicInfo(getSourceId(), title, author, detailUrl, imgUrl, updateTime, updateChapter);
            }
        };
        return starter.startElements(html, "ul.stui-vodlist__media li");
    }

    @Override
    public void setInfoDetail(ComicInfo info, String html, Map<String, Object> map) {
        JsoupStarter<ChapterInfo> starter = new JsoupStarter<ChapterInfo>() {
            @Override
            protected boolean isDESC() {
                return false;
            }

            @Override
            protected void dealInfo(JsoupNode node) {
                String title = node.ownText("h1.title");
                String imgUrl = node.attr("img.lazyload", "data-original");
                String author = node.ownText("p.data a", 1);
                String intro = node.ownText("p.col-pd");
                String updateStatus = null;
                String updateTime = node.ownText("p.hidden-sm");
                info.setDetail(title, imgUrl, author, updateTime, updateStatus, intro);
            }

            @Override
            protected ChapterInfo dealElement(JsoupNode node) {
                String title = node.ownText("a");
                String chapterUrl = node.href("a");
                return new ChapterInfo(title, chapterUrl);
            }
        };
        starter.startInfo(html);
        SourceHelper.initChapterInfoList(info, starter.startElements(html, "ul.stui-content__playlist li"));
    }

    @Override
    public List<Content> getContentList(String html, int chapterId, Map<String, Object> map) {
        String[] urls = StringUtil.matchArray("src=\"(.*?)\"", html);
        return SourceHelper.getContentList(urls, chapterId);
    }

    @Override
    public Map<String, String> getRankMap() {
        String html = "<ul class=\"stui-header__menu\"><li class=\"hidden-xs\"><a href=\"/fenlei/2-1.html\">漫画更新</a></li><li class=\"hidden-xs\"><a href=\"/fenlei/4-1.html\">漫画大全</a></li><li class=\"hidden-xs\"><a href=\"/fenlei/3-1.html\">更多漫画</a></li></ul>";
        Map<String, String> map = new LinkedHashMap<>();
        JsoupNode node = new JsoupNode(html);
        Elements elements = node.getElements("a");
        for (Element element : elements) {
            node.init(element);
            map.put(node.ownText("a"), getIndex() + node.href("a"));
        }
        return map;
    }

    @Override
    public List<ComicInfo> getRankInfoList(String html) {
        JsoupStarter<ComicInfo> starter = new JsoupStarter<ComicInfo>() {
            @Override
            protected ComicInfo dealElement(JsoupNode node) {
                String title = node.ownText("h4.title a");
                String author = null;
                String updateTime = node.ownText("p.hidden-xs");
                String updateChapter = node.ownText("span.pic-text");
                String imgUrl = node.attr("a.lazyload", "data-original");
                String detailUrl = getIndex() + node.href("a");
                return new ComicInfo(getSourceId(), title, author, detailUrl, imgUrl, updateTime, updateChapter);
            }
        };
        return starter.startElements(html, "ul.stui-vodlist li");
    }
}
