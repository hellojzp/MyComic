package top.luqichuang.mycomic.source;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import okhttp3.Request;
import top.luqichuang.common.en.CSourceEnum;
import top.luqichuang.common.jsoup.JsoupNode;
import top.luqichuang.common.jsoup.JsoupStarter;
import top.luqichuang.common.model.ChapterInfo;
import top.luqichuang.common.model.Content;
import top.luqichuang.common.util.DecryptUtil;
import top.luqichuang.common.util.NetUtil;
import top.luqichuang.common.util.SourceHelper;
import top.luqichuang.common.util.StringUtil;
import top.luqichuang.mycomic.model.BaseComicSource;
import top.luqichuang.mycomic.model.ComicInfo;

/**
 * @author LuQiChuang
 * @desc
 * @date 2020/8/12 15:25
 * @ver 1.0
 */
public class TengXun extends BaseComicSource {

    @Override
    public CSourceEnum getCSourceEnum() {
        return CSourceEnum.TENG_XUN;
    }

    @Override
    public String getIndex() {
        return "https://ac.qq.com";
    }

    @Override
    public Request getSearchRequest(String searchString) {
        searchString = "https://ac.qq.com/Comic/searchList?search=" + searchString;
        return NetUtil.getRequest(searchString);
    }

    @Override
    public List<ComicInfo> getInfoList(String html) {
        JsoupStarter<ComicInfo> starter = new JsoupStarter<ComicInfo>() {
            @Override
            protected ComicInfo dealElement(JsoupNode node) {
                String title = node.title("a");
                String author = null;
                String updateTime = null;
                String updateChapter = node.ownText("h3");
                String imgUrl = node.attr("img", "data-original");
                String detailUrl = getIndex() + node.href("a");
                return new ComicInfo(getSourceId(), title, author, detailUrl, imgUrl, updateTime, updateChapter);
            }
        };
        return starter.startElements(html, "ul.mod_book_list li");
    }

    @Override
    public void setInfoDetail(ComicInfo info, String html, Map<String, Object> map) {
        JsoupStarter<ChapterInfo> starter = new JsoupStarter<ChapterInfo>() {

            @Override
            protected boolean isDESC() {
                return false;
            }

            @Override
            protected void dealInfo(JsoupNode node) {
                String title = node.ownText("h2.works-intro-title.ui-left strong");
                String imgUrl = node.src("div.works-cover.ui-left img");
                String author = node.ownText("div.works-intro span.first em");
                String intro = node.ownText("div.works-intro p.works-intro-short");
                String updateStatus = node.ownText("div.works-intro label.works-intro-status");
                String updateTime = node.ownText("ul.ui-left span.ui-pl10");
                info.setDetail(title, imgUrl, author, updateTime, updateStatus, intro);
            }

            @Override
            protected ChapterInfo dealElement(JsoupNode node) {
                String title = node.ownText("a");
                String chapterUrl = getIndex() + node.href("a");
                return new ChapterInfo(title, chapterUrl);
            }
        };
        starter.startInfo(html);
        SourceHelper.initChapterInfoList(info, starter.startElements(html, "ol.chapter-page-all span.works-chapter-item"));
    }

    @Override
    public List<Content> getContentList(String html, int chapterId, Map<String, Object> map) {
        String raw = StringUtil.match("DATA.*=.*'(.*?)',", html);
        String nonce = StringUtil.matchLast("window\\[.*?\\] *=(.*?);", html);
        if (nonce != null) {
            String[] docs = nonce.split("\\(\\)");
            for (String doc : docs) {
                String tmp = StringUtil.match("(\\(.*document.*\\)\\.toString)", doc);
                if (tmp == null) {
                    tmp = StringUtil.match("(\\(.*window.*\\)\\.toString)", doc);
                }
                if (tmp != null) {
                    nonce = nonce.replace(tmp + "()", "0");
                }
            }
            nonce = DecryptUtil.exeJsCode(nonce);
        }
        String data = DecryptUtil.exeJsFunction(getJsCode(), "decode", raw, nonce);
        List<String> urlList = null;
        if (data != null) {
            data = DecryptUtil.decryptBase64(data);
            if (data != null) {
                data = data.replaceAll("\\\\", "");
                urlList = StringUtil.matchList("pid(.*?)\"url\":\"(.*?)\"", data, 2);
            }
        }
        return SourceHelper.getContentList(urlList, chapterId);
    }

    private String getJsCode() {
        return "function decode(T, N) {\n" +
                "\tvar len, locate, str;\n" +
                "\tT = T.split('');\n" +
                "\tN = N.match(/\\d+[a-zA-Z]+/g);\n" +
                "\tlen = N.length;\n" +
                "\twhile (len--) {\n" +
                "\t\tlocate = parseInt(N[len]) & 255;\n" +
                "\t\tstr = N[len].replace(/\\d+/g, '');\n" +
                "\t\tT.splice(locate, str.length)\n" +
                "\t}\n" +
                "\tT = T.join('');\n" +
                "\treturn T;\n" +
                "}";
    }

    @Override
    public Map<String, String> getRankMap() {
        Map<String, String> map = new LinkedHashMap<>();
        map.put("飙升榜", "https://m.ac.qq.com/rank/index?type=rise&page=1");
        map.put("畅销榜", "https://m.ac.qq.com/rank/index?type=pay&page=1");
        map.put("新作榜", "https://m.ac.qq.com/rank/index?type=new&page=1");
        map.put("真香榜", "https://m.ac.qq.com/rank/index?type=hot&page=1");
        String html = "<a href=\"javascript:void(0)\"title=\"付费\"id=\"vip/2\">付费</a><a href=\"javascript:void(0)\"title=\"免费\"id=\"vip/1\">免费</a><a href=\"javascript:void(0)\"title=\"连载\"id=\"finish/1\">连载</a><a href=\"javascript:void(0)\"title=\"完结\"id=\"finish/2\">完结</a><a href=\"javascript:void(0)\"title=\"恋爱\"id=\"theme/105\">恋爱</a><a href=\"javascript:void(0)\"title=\"玄幻\"id=\"theme/101\">玄幻</a><a href=\"javascript:void(0)\"title=\"异能\"id=\"theme/103\">异能</a><a href=\"javascript:void(0)\"title=\"恐怖\"id=\"theme/110\">恐怖</a><a href=\"javascript:void(0)\"title=\"剧情\"id=\"theme/106\">剧情</a><a href=\"javascript:void(0)\"title=\"科幻\"id=\"theme/108\">科幻</a><a href=\"javascript:void(0)\"title=\"悬疑\"id=\"theme/112\">悬疑</a><a href=\"javascript:void(0)\"title=\"奇幻\"id=\"theme/102\">奇幻</a><a href=\"javascript:void(0)\"title=\"冒险\"id=\"theme/104\">冒险</a><a href=\"javascript:void(0)\"title=\"犯罪\"id=\"theme/111\">犯罪</a><a href=\"javascript:void(0)\"title=\"动作\"id=\"theme/109\">动作</a><a href=\"javascript:void(0)\"title=\"日常\"id=\"theme/113\">日常</a><a href=\"javascript:void(0)\"title=\"竞技\"id=\"theme/114\">竞技</a><a href=\"javascript:void(0)\"title=\"武侠\"id=\"theme/115\">武侠</a><a href=\"javascript:void(0)\"title=\"历史\"id=\"theme/116\">历史</a><a href=\"javascript:void(0)\"title=\"战争\"id=\"theme/117\">战争</a>";
        JsoupNode node = new JsoupNode(html);
        Elements elements = node.getElements("a");
        for (Element element : elements) {
            node.init(element);
            String url = String.format("%s/Comic/all/%s/search/hot/page/1", getIndex(), node.attr("a", "id"));
            map.put(node.ownText("a"), url);
        }
        return map;
    }

    @Override
    public List<ComicInfo> getRankInfoList(String html) {
        List<ComicInfo> list = new ArrayList<>();
        JsoupStarter<ComicInfo> starter = new JsoupStarter<ComicInfo>() {
            @Override
            protected void dealInfo(JsoupNode node) {
                node.addElement("div.top3-box-item1");
                node.addElement("div.top3-box-item2");
                node.addElement("div.top3-box-item3");
                for (Element element : node.getElements()) {
                    node.init(element);
                    String title = node.ownText("strong.comic-title");
                    String author = null;
                    String updateTime = null;
                    String updateChapter = node.ownText("small.comic-update");
                    String imgUrl = node.src("img");
                    String detailUrl = getIndex() + node.href("a");
                    try {
                        if (detailUrl.contains("comic/index")) {
                            detailUrl = detailUrl.replace("comic/index", "Comic/comicInfo");
                        }
                    } catch (Exception ignored) {
                    }
                    ComicInfo comicInfo = new ComicInfo(getSourceId(), title, author, detailUrl, imgUrl, updateTime, updateChapter);
                    list.add(comicInfo);
                }
            }

            @Override
            protected ComicInfo dealElement(JsoupNode node) {
                String title = node.ownText("strong.comic-title");
                String author = null;
                String updateTime = null;
                String updateChapter = node.ownText("small.comic-update");
                String imgUrl = node.src("img");
                String detailUrl = getIndex() + node.href("a");
                try {
                    if (detailUrl.contains("comic/index")) {
                        detailUrl = detailUrl.replace("comic/index", "Comic/comicInfo");
                    }
                } catch (Exception ignored) {
                }
                return new ComicInfo(getSourceId(), title, author, detailUrl, imgUrl, updateTime, updateChapter);
            }
        };
        starter.startInfo(html);
        list.addAll(starter.startElements(html, "li.comic-item"));
        if (!list.isEmpty()) {
            return list;
        } else {
            return new JsoupStarter<ComicInfo>() {
                @Override
                protected ComicInfo dealElement(JsoupNode node) {
                    String title = node.title("h3.ret-works-title a");
                    String author = node.title("p.ret-works-author");
                    String updateTime = null;
                    String updateChapter = null;
                    String imgUrl = node.attr("img", "data-original");
                    String detailUrl = getIndex() + node.href("a");
                    return new ComicInfo(getSourceId(), title, author, detailUrl, imgUrl, updateTime, updateChapter);
                }
            }.startElements(html, "ul.clearfix li");
        }
    }
}
