package top.luqichuang.mycomic.source;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import okhttp3.Request;
import top.luqichuang.common.en.CSourceEnum;
import top.luqichuang.common.jsoup.JsoupNode;
import top.luqichuang.common.jsoup.JsoupStarter;
import top.luqichuang.common.model.ChapterInfo;
import top.luqichuang.common.model.Content;
import top.luqichuang.common.util.NetUtil;
import top.luqichuang.common.util.SourceHelper;
import top.luqichuang.common.util.StringUtil;
import top.luqichuang.mycomic.model.BaseComicSource;
import top.luqichuang.mycomic.model.ComicInfo;

/**
 * @author LuQiChuang
 * @desc
 * @date 2022/11/19 13:44
 * @ver 1.0
 */
public class PinYue extends BaseComicSource {

    @Override
    public CSourceEnum getCSourceEnum() {
        return CSourceEnum.PIN_YUE;
    }

    @Override
    public String getIndex() {
        return "https://www.pinmh.com";
    }

    @Override
    public Request getSearchRequest(String searchString) {
        String url = String.format("%s/search/?keywords=%s", getIndex(), searchString);
        return NetUtil.getRequest(url);
    }

    @Override
    public List<ComicInfo> getInfoList(String html) {
        JsoupStarter<ComicInfo> starter = new JsoupStarter<ComicInfo>() {
            @Override
            protected ComicInfo dealElement(JsoupNode node) {
                String title = node.title("a");
                String author = node.ownText("p.auth");
                String updateTime = null;
                String updateChapter = node.ownText("p.newPage");
                String imgUrl = node.src("img");
                String detailUrl = node.href("a");
                return new ComicInfo(getSourceId(), title, author, detailUrl, imgUrl, updateTime, updateChapter);
            }
        };
        return starter.startElements(html, "li.list-comic");
    }

    @Override
    public void setInfoDetail(ComicInfo info, String html, Map<String, Object> map) {
        JsoupStarter<ChapterInfo> starter = new JsoupStarter<ChapterInfo>() {
            @Override
            protected boolean isDESC() {
                return false;
            }

            @Override
            protected void dealInfo(JsoupNode node) {
                String title = node.ownText("div.comic_deCon h1");
                String imgUrl = node.src("div.comic_i_img img");
                String author = node.ownText("ul.comic_deCon_liO li");
                String intro = node.ownText("p.comic_deCon_d");
                String updateStatus = node.ownText("ul.comic_deCon_liO a");
                String updateTime = null;
                String updateChapter = null;
                author = StringUtil.remove(author, "作者：");
                info.setDetail(title, imgUrl, author, updateTime, updateStatus, intro);
            }

            @Override
            protected ChapterInfo dealElement(JsoupNode node) {
                String title = node.ownText("span.list_con_zj");
                String chapterUrl = getIndex() + node.href("a");
                return new ChapterInfo(title, chapterUrl);
            }
        };
        starter.startInfo(html);
        SourceHelper.initChapterInfoList(info, starter.startElements(html, "ul#chapter-list-1 li"));
    }

    @Override
    public List<Content> getContentList(String html, int chapterId, Map<String, Object> map) {
        String[] urls = null;
        String chapterImages = StringUtil.match("chapterImages = \\[(.*?)\\];", html);
        if (chapterImages != null) {
            chapterImages = chapterImages.replace("\"", "").replace("\\", "");
            urls = chapterImages.split(",");
        }
        return SourceHelper.getContentList(urls, chapterId);
    }

    @Override
    public Map<String, String> getRankMap() {
        String html = "<div class=\"wrap_ph_l con_left\"><ul class=\"ph_l_li\"><li class=\"active\"><a href=\"/rank/popularity/\">人气榜</a></li><li class=\"\"><a href=\"/rank/click/\">点击榜</a></li><li class=\"\"><a href=\"/rank/subscribe/\">订阅榜</a></li><li class=\"\"><a href=\"/rank/comment/\">评论榜</a></li><li class=\"\"><a href=\"/rank/criticism/\">吐槽榜</a></li></ul><ul class=\"ph_l_li\"><li class=\"\"><a href=\"/rank/mofa/\">魔法漫画</a></li><li class=\"\"><a href=\"/rank/shaonian/\">少年漫画</a></li><li class=\"\"><a href=\"/rank/shaonv/\">少女漫画</a></li><li class=\"\"><a href=\"/rank/qingnian/\">青年漫画</a></li><li class=\"\"><a href=\"/rank/gaoxiao/\">搞笑漫画</a></li><li class=\"\"><a href=\"/rank/kehuan/\">科幻漫画</a></li><li class=\"\"><a href=\"/rank/rexue/\">热血漫画</a></li><li class=\"\"><a href=\"/rank/maoxian/\">冒险漫画</a></li><li class=\"\"><a href=\"/rank/wanjie/\">完结漫画</a></li></ul></div>";
        Map<String, String> map = new LinkedHashMap<>();
        JsoupNode node = new JsoupNode(html);
        Elements elements = node.getElements("a");
        for (Element element : elements) {
            node.init(element);
            map.put(node.ownText("a"), getIndex() + node.href("a"));
        }
        return map;
    }

    @Override
    public List<ComicInfo> getRankInfoList(String html) {
        JsoupStarter<ComicInfo> starter = new JsoupStarter<ComicInfo>() {
            @Override
            protected ComicInfo dealElement(JsoupNode node) {
                String title = node.title("div.con_li_content");
                String author = node.ownText("li");
                String updateTime = null;
                String updateChapter = null;
                String imgUrl = node.src("img");
                String detailUrl = node.href("a");
                return new ComicInfo(getSourceId(), title, author, detailUrl, imgUrl, updateTime, updateChapter);
            }
        };
        return starter.startElements(html, "div.ph_r_con_li_c");
    }
}