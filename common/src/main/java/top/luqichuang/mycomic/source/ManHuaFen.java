package top.luqichuang.mycomic.source;

import java.util.List;
import java.util.Map;

import okhttp3.Request;
import top.luqichuang.common.en.CSourceEnum;
import top.luqichuang.common.model.Content;
import top.luqichuang.mycomic.model.BaseComicSource;
import top.luqichuang.mycomic.model.ComicInfo;

/**
 * @author LuQiChuang
 * @desc
 * @date 2020/8/12 15:25
 * @ver 1.0
 */
@Deprecated
public class ManHuaFen extends BaseComicSource {

    @Override
    public CSourceEnum getCSourceEnum() {
        return CSourceEnum.MAN_HUA_FEN;
    }

    @Override
    public String getIndex() {
        return "https://m.manhuafen.com";
    }

    @Override
    public boolean isValid() {
        return false;
    }

    @Override
    public Request getSearchRequest(String searchString) {
        return null;
    }

    @Override
    public List<ComicInfo> getInfoList(String html) {
        return null;
    }

    @Override
    public void setInfoDetail(ComicInfo info, String html, Map<String, Object> map) {

    }

    @Override
    public List<Content> getContentList(String html, int chapterId, Map<String, Object> map) {
        return null;
    }

    @Override
    public Map<String, String> getRankMap() {
        return null;
    }

    @Override
    public List<ComicInfo> getRankInfoList(String html) {
        return null;
    }
}
