package top.luqichuang.mycomic.source;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import okhttp3.Request;
import top.luqichuang.common.en.CSourceEnum;
import top.luqichuang.common.jsoup.JsoupNode;
import top.luqichuang.common.jsoup.JsoupStarter;
import top.luqichuang.common.model.ChapterInfo;
import top.luqichuang.common.model.Content;
import top.luqichuang.common.util.NetUtil;
import top.luqichuang.common.util.SourceHelper;
import top.luqichuang.common.util.StringUtil;
import top.luqichuang.mycomic.model.BaseComicSource;
import top.luqichuang.mycomic.model.ComicInfo;

/**
 * @author 18472
 * @desc
 * @date 2024/1/7 15:37
 * @ver 1.0
 */
public class GoDa extends BaseComicSource {

    @Override
    public CSourceEnum getCSourceEnum() {
        return CSourceEnum.GO_DA;
    }

    @Override
    public String getIndex() {
        return "https://godamanga.site";
    }

    @Override
    public Request getSearchRequest(String searchString) {
        String url = String.format("%s/s/%s", getIndex(), searchString);
        return NetUtil.getRequest(url);
    }

    @Override
    public Request buildRequest(String html, String tag, Map<String, Object> data, Map<String, Object> map) {
        if (DETAIL.equals(tag) && map.isEmpty()) {
            JsoupNode node = new JsoupNode(html);
            map.put("title", node.ownText("h1.mb-2.text-xl"));
            map.put("imgUrl", node.src("div#MangaCard img"));
            map.put("author", node.text("div.text-small"));
            map.put("intro", node.ownText("p.text-medium"));
            map.put("updateStatus", node.ownText("h1.mb-2 span"));
            map.put("updateTime", node.ownText("div#chapterlists span.italic"));
            String url = (String) data.get("url");
            url = url.replace("/manga/", "/chapterlist/");
            return NetUtil.getRequest(url);
        }
        return super.buildRequest(html, tag, data, map);
    }

    @Override
    public List<ComicInfo> getInfoList(String html) {
        JsoupStarter<ComicInfo> starter = new JsoupStarter<ComicInfo>() {
            @Override
            protected ComicInfo dealElement(JsoupNode node) {
                String title = node.ownText("h3.cardtitle");
                String author = null;
                String updateTime = null;
                String updateChapter = null;
                String imgUrl = node.src("img");
                String detailUrl = getIndex() + node.href("a");
                return new ComicInfo(getSourceId(), title, author, detailUrl, imgUrl, updateTime, updateChapter);
            }
        };
        return starter.startElements(html, "div.cardlist div.pb-2");
    }

    @Override
    public void setInfoDetail(ComicInfo info, String html, Map<String, Object> map) {
        JsoupStarter<ChapterInfo> starter = new JsoupStarter<ChapterInfo>() {
            @Override
            protected boolean isDESC() {
                return false;
            }

            @Override
            protected void dealInfo(JsoupNode node) {
                String title = (String) map.get("title");
                String imgUrl = (String) map.get("imgUrl");
                String author = (String) map.get("author");
                String intro = (String) map.get("intro");
                String updateStatus = (String) map.get("updateStatus");
                String updateTime = (String) map.get("updateTime");
                author = StringUtil.remove(author, " ");
                author = StringUtil.remove(author, "作者：");
                info.setDetail(title, imgUrl, author, updateTime, updateStatus, intro);
            }

            @Override
            protected ChapterInfo dealElement(JsoupNode node) {
                String title = node.ownText("span.chaptertitle");
                String chapterUrl = node.href("a");
                return new ChapterInfo(title, chapterUrl);
            }
        };
        starter.startInfo(html);
        SourceHelper.initChapterInfoList(info, starter.startElements(html, "div#chapterlists div.chapteritem"));
    }

    @Override
    public List<Content> getContentList(String html, int chapterId, Map<String, Object> map) {
        List<String> urlList = new ArrayList<>();
        JsoupNode node = new JsoupNode(html);
        node.remove("img.lazypreload");
        Elements elements = node.getElements("div.touch-manipulation>div");
        for (Element element : elements) {
            node.init(element);
            urlList.add(node.src("img"));
        }
        return SourceHelper.getContentList(urlList, chapterId);
    }

    @Override
    public Map<String, String> getRankMap() {
        String html = "<div class=\"mb-unit-xs\"><div class=\"flex flex-row gap-x-2 pt-3 items-center gap-y-2\"><h2 class=\"text-small pr-1 flex-shrink-0\">漫畫類型</h2><div class=\" overflow-x-auto gap-unit-xs flex scrollbar-hide\"><a href=\"/manga\"><button type=\"button\"class=\"abutton tap-highlight-transparent  bg-default-100 abuttonsm\"data-astro-cid-ljkzbozy=\"\">全部</button></a><a href=\"/manga-genre/kr\"><button type=\"button\"class=\"abutton tap-highlight-transparent  bg-default-100 abuttonsm\"data-astro-cid-ljkzbozy=\"\">韩漫</button></a><a href=\"/manga-genre/hots\"><button type=\"button\"class=\"abutton tap-highlight-transparent  bg-default-100 abuttonsm\"data-astro-cid-ljkzbozy=\"\">热门漫画</button></a><a href=\"/manga-genre/cn\"><button type=\"button\"class=\"abutton tap-highlight-transparent  bg-default-100 abuttonsm\"data-astro-cid-ljkzbozy=\"\">国漫</button></a><a href=\"/manga-genre/qita\"><button type=\"button\"class=\"abutton tap-highlight-transparent  bg-default-100 abuttonsm\"data-astro-cid-ljkzbozy=\"\">其他</button></a><a href=\"/manga-genre/jp\"><button type=\"button\"class=\"abutton tap-highlight-transparent  bg-default-100 abuttonsm\"data-astro-cid-ljkzbozy=\"\">日漫</button></a><a href=\"/manga-genre/ou-mei\"><button type=\"button\"class=\"abutton tap-highlight-transparent  bg-default-100 abuttonsm\"data-astro-cid-ljkzbozy=\"\">欧美</button></a></div></div><div class=\"flex flex-row  gap-x-unit-xs py-unit-md\"><h2 class=\"text-small pr-1 flex-shrink-0\">熱門標籤</h2><div class=\" overflow-x-auto gap-unit-xs flex scrollbar-hide\"><a href=\"/manga-tag/fuchou\"><button type=\"button\"class=\"abutton tap-highlight-transparent  bg-default-100 abuttonsm\"key=\"0\"data-astro-cid-ljkzbozy=\"\">#复仇</button></a><a href=\"/manga-tag/gufeng\"><button type=\"button\"class=\"abutton tap-highlight-transparent  bg-default-100 abuttonsm\"key=\"1\"data-astro-cid-ljkzbozy=\"\">#古风</button></a><a href=\"/manga-tag/qihuan\"><button type=\"button\"class=\"abutton tap-highlight-transparent  bg-default-100 abuttonsm\"key=\"2\"data-astro-cid-ljkzbozy=\"\">#奇幻</button></a><a href=\"/manga-tag/nixi\"><button type=\"button\"class=\"abutton tap-highlight-transparent  bg-default-100 abuttonsm\"key=\"3\"data-astro-cid-ljkzbozy=\"\">#逆袭</button></a><a href=\"/manga-tag/yineng\"><button type=\"button\"class=\"abutton tap-highlight-transparent  bg-default-100 abuttonsm\"key=\"4\"data-astro-cid-ljkzbozy=\"\">#异能</button></a><a href=\"/manga-tag/zhaixiang\"><button type=\"button\"class=\"abutton tap-highlight-transparent  bg-default-100 abuttonsm\"key=\"5\"data-astro-cid-ljkzbozy=\"\">#宅向</button></a><a href=\"/manga-tag/chuanyue\"><button type=\"button\"class=\"abutton tap-highlight-transparent  bg-default-100 abuttonsm\"key=\"6\"data-astro-cid-ljkzbozy=\"\">#穿越</button></a><a href=\"/manga-tag/rexue\"><button type=\"button\"class=\"abutton tap-highlight-transparent  bg-default-100 abuttonsm\"key=\"7\"data-astro-cid-ljkzbozy=\"\">#热血</button></a><a href=\"/manga-tag/chunai\"><button type=\"button\"class=\"abutton tap-highlight-transparent  bg-default-100 abuttonsm\"key=\"8\"data-astro-cid-ljkzbozy=\"\">#纯爱</button></a><a href=\"/manga-tag/xitong\"><button type=\"button\"class=\"abutton tap-highlight-transparent  bg-default-100 abuttonsm\"key=\"9\"data-astro-cid-ljkzbozy=\"\">#系统</button></a><a href=\"/manga-tag/zhongsheng\"><button type=\"button\"class=\"abutton tap-highlight-transparent  bg-default-100 abuttonsm\"key=\"10\"data-astro-cid-ljkzbozy=\"\">#重生</button></a><a href=\"/manga-tag/maoxian\"><button type=\"button\"class=\"abutton tap-highlight-transparent  bg-default-100 abuttonsm\"key=\"11\"data-astro-cid-ljkzbozy=\"\">#冒险</button></a><a href=\"/manga-tag/lingyi\"><button type=\"button\"class=\"abutton tap-highlight-transparent  bg-default-100 abuttonsm\"key=\"12\"data-astro-cid-ljkzbozy=\"\">#灵异</button></a><a href=\"/manga-tag/danvzhu\"><button type=\"button\"class=\"abutton tap-highlight-transparent  bg-default-100 abuttonsm\"key=\"13\"data-astro-cid-ljkzbozy=\"\">#大女主</button></a><a href=\"/manga-tag/juqing\"><button type=\"button\"class=\"abutton tap-highlight-transparent  bg-default-100 abuttonsm\"key=\"14\"data-astro-cid-ljkzbozy=\"\">#剧情</button></a><a href=\"/manga-tag/lianai\"><button type=\"button\"class=\"abutton tap-highlight-transparent  bg-default-100 abuttonsm\"key=\"15\"data-astro-cid-ljkzbozy=\"\">#恋爱</button></a><a href=\"/manga-tag/xuanhuan\"><button type=\"button\"class=\"abutton tap-highlight-transparent  bg-default-100 abuttonsm\"key=\"16\"data-astro-cid-ljkzbozy=\"\">#玄幻</button></a><a href=\"/manga-tag/nvshen\"><button type=\"button\"class=\"abutton tap-highlight-transparent  bg-default-100 abuttonsm\"key=\"17\"data-astro-cid-ljkzbozy=\"\">#女神</button></a><a href=\"/manga-tag/kehuan\"><button type=\"button\"class=\"abutton tap-highlight-transparent  bg-default-100 abuttonsm\"key=\"18\"data-astro-cid-ljkzbozy=\"\">#科幻</button></a><a href=\"/manga-tag/mohuan\"><button type=\"button\"class=\"abutton tap-highlight-transparent  bg-default-100 abuttonsm\"key=\"19\"data-astro-cid-ljkzbozy=\"\">#魔幻</button></a><a href=\"/manga-tag/tuili\"><button type=\"button\"class=\"abutton tap-highlight-transparent  bg-default-100 abuttonsm\"key=\"20\"data-astro-cid-ljkzbozy=\"\">#推理</button></a><a href=\"/manga-tag/lieqi\"><button type=\"button\"class=\"abutton tap-highlight-transparent  bg-default-100 abuttonsm\"key=\"21\"data-astro-cid-ljkzbozy=\"\">#猎奇</button></a><a href=\"/manga-tag/zhiyu\"><button type=\"button\"class=\"abutton tap-highlight-transparent  bg-default-100 abuttonsm\"key=\"22\"data-astro-cid-ljkzbozy=\"\">#治愈</button></a><a href=\"/manga-tag/doushi\"><button type=\"button\"class=\"abutton tap-highlight-transparent  bg-default-100 abuttonsm\"key=\"23\"data-astro-cid-ljkzbozy=\"\">#都市</button></a><a href=\"/manga-tag/yixing\"><button type=\"button\"class=\"abutton tap-highlight-transparent  bg-default-100 abuttonsm\"key=\"24\"data-astro-cid-ljkzbozy=\"\">#异形</button></a><a href=\"/manga-tag/qingchun\"><button type=\"button\"class=\"abutton tap-highlight-transparent  bg-default-100 abuttonsm\"key=\"25\"data-astro-cid-ljkzbozy=\"\">#青春</button></a><a href=\"/manga-tag/mori\"><button type=\"button\"class=\"abutton tap-highlight-transparent  bg-default-100 abuttonsm\"key=\"26\"data-astro-cid-ljkzbozy=\"\">#末日</button></a><a href=\"/manga-tag/xuanyi\"><button type=\"button\"class=\"abutton tap-highlight-transparent  bg-default-100 abuttonsm\"key=\"27\"data-astro-cid-ljkzbozy=\"\">#悬疑</button></a><a href=\"/manga-tag/xiuxian\"><button type=\"button\"class=\"abutton tap-highlight-transparent  bg-default-100 abuttonsm\"key=\"28\"data-astro-cid-ljkzbozy=\"\">#修仙</button></a><a href=\"/manga-tag/zhandou\"><button type=\"button\"class=\"abutton tap-highlight-transparent  bg-default-100 abuttonsm\"key=\"29\"data-astro-cid-ljkzbozy=\"\">#战斗</button></a></div></div></div>";
        Map<String, String> map = new LinkedHashMap<>();
        JsoupNode node = new JsoupNode(html);
        Elements elements = node.getElements("a");
        for (Element element : elements) {
            node.init(element);
            map.put(node.ownText("a button"), getIndex() + node.href("a") + "/page/1");
        }
        return map;
    }

    @Override
    public List<ComicInfo> getRankInfoList(String html) {
        return getInfoList(html);
    }
}
