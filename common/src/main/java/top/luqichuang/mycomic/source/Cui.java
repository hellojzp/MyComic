package top.luqichuang.mycomic.source;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import okhttp3.Request;
import top.luqichuang.common.en.CSourceEnum;
import top.luqichuang.common.json.JsonNode;
import top.luqichuang.common.json.JsonStarter;
import top.luqichuang.common.jsoup.JsoupNode;
import top.luqichuang.common.jsoup.JsoupStarter;
import top.luqichuang.common.model.ChapterInfo;
import top.luqichuang.common.model.Content;
import top.luqichuang.common.util.NetUtil;
import top.luqichuang.common.util.SourceHelper;
import top.luqichuang.mycomic.model.BaseComicSource;
import top.luqichuang.mycomic.model.ComicInfo;

/**
 * @author 18472
 * @desc
 * @date 2024/1/28 15:08
 * @ver 1.0
 */
public class Cui extends BaseComicSource {

    @Override
    public CSourceEnum getCSourceEnum() {
        return CSourceEnum.CUI;
    }

    @Override
    public String getIndex() {
        return "https://www.cuiman.com";
    }

    @Override
    public Request getSearchRequest(String searchString) {
        String url = String.format("%s/index.php/search?key=%s", getIndex(), searchString);
        return NetUtil.getRequest(url);
    }

    @Override
    public Request buildRequest(String html, String tag, Map<String, Object> data, Map<String, Object> map) {
        if (DETAIL.equals(tag)) {
            if (map.isEmpty()) {
                Map<String, String> nameMap = new HashMap<>();
                Map<String, String> htmlMap = new HashMap<>();
                JsoupNode node = new JsoupNode(html);
                Elements elements = node.getElements("ul.source_list > li");
                for (Element element : elements) {
                    node.init(element);
                    String id = node.attr("li", "data-mid");
                    String name = node.ownText("li a");
                    String url = String.format("%s/index.php/api/comic/index?mid=%s", getIndex(), id);
                    nameMap.put(url, name);
                    htmlMap.put(url, null);
                }
                map.put("nameMap", nameMap);
                map.put("htmlMap", htmlMap);
            }
            Map<String, String> htmlMap = (Map<String, String>) map.get("htmlMap");
            if (htmlMap != null && !htmlMap.isEmpty()) {
                String url = (String) data.get("url");
                if (htmlMap.containsKey(url)) {
                    htmlMap.put(url, html);
                }
                for (Map.Entry<String, String> entry : htmlMap.entrySet()) {
                    if (entry.getValue() == null) {
                        return NetUtil.getRequest(entry.getKey());
                    }
                }
            }
        }
        return super.buildRequest(html, tag, data, map);
    }

    @Override
    public List<ComicInfo> getInfoList(String html) {
        JsoupStarter<ComicInfo> starter = new JsoupStarter<ComicInfo>() {
            @Override
            protected ComicInfo dealElement(JsoupNode node) {
                String title = node.ownText("li.title a");
                String author = null;
                String updateTime = null;
                String updateChapter = node.ownText("span");
                String imgUrl = node.src("img");
                String detailUrl = getIndex() + node.href("a");
                return new ComicInfo(getSourceId(), title, author, detailUrl, imgUrl, updateTime, updateChapter);
            }
        };
        return starter.startElements(html, "div.cy_list_mh ul");
    }

    @Override
    public void setInfoDetail(ComicInfo info, String html, Map<String, Object> map) {
        JsonStarter<ChapterInfo> starter = new JsonStarter<ChapterInfo>() {
            @Override
            protected void dealData(JsonNode node) {
                String title = node.string("comic_name");
                String imgUrl = node.string("comic_pic");
                String author = node.string("comic_author");
                String intro = node.string("comic_content");
                String updateStatus = node.string("comic_serialize");
                String updateTime = node.string("comic_addtime");
                info.setDetail(title, imgUrl, author, updateTime, updateStatus, intro);
            }

            @Override
            protected ChapterInfo dealDataList(JsonNode node) {
                String title = node.string("name");
                String chapterUrl = getIndex() + node.string("piclink");
                return new ChapterInfo(title, chapterUrl);
            }
        };
        List<ChapterInfo> list = new ArrayList<>();
        Map<String, String> htmlMap = (Map<String, String>) map.get("htmlMap");
        Map<String, String> nameMap = (Map<String, String>) map.get("nameMap");
        Map<String, Integer> sizeMap = new LinkedHashMap<>();
        if (htmlMap != null && !htmlMap.isEmpty()) {
            for (Map.Entry<String, String> entry : htmlMap.entrySet()) {
                String url = entry.getKey();
                String h = entry.getValue();
                List<ChapterInfo> l = starter.startDataList(h, "data", "comic_chapter_list");
                sizeMap.put(nameMap.get(url), l.size());
                list.addAll(l);
            }
        }
        starter.startData(html, "data");
        SourceHelper.initChapterInfoList(info, list);
        SourceHelper.initChapterInfoMap(info, sizeMap);
    }

    @Override
    public List<Content> getContentList(String html, int chapterId, Map<String, Object> map) {
        List<String> urlList = new ArrayList<>();
        JsoupNode node = new JsoupNode(html);
        Elements elements = node.getElements("div.rd-article-wr img");
        for (Element element : elements) {
            node.init(element);
            urlList.add(node.attr("img", "data-original"));
        }
        return SourceHelper.getContentList(urlList, chapterId);
    }

    @Override
    public Map<String, String> getRankMap() {
        String html = "<div class=\"cy_list_l\"><div class=\"cy_tag\"><span>分类</span><ul><li class=\"btn\"><a href=\"/category/\">全部</a></li><li class=\"\"><a href=\"/category/list/1\">国产漫画</a></li><li class=\"\"><a href=\"/category/list/2\">日本漫画</a></li><li class=\"\"><a href=\"/category/list/3\">韩国漫画</a></li><li class=\"\"><a href=\"/category/list/4\">欧美漫画</a></li></ul></div><div class=\"cy_tag\"><span>标签</span><ul><li class=\"\"><a href=\"/category/tags/168\">少年漫画</a></li><li class=\"\"><a href=\"/category/tags/183\">少女漫画</a></li><li class=\"\"><a href=\"/category/tags/197\">青年漫画</a></li><li class=\"\"><a href=\"/category/tags/214\">真人漫画</a></li><li class=\"\"><a href=\"/category/tags/211\">玄幻科幻</a></li><li class=\"\"><a href=\"/category/tags/212\">冒险热血</a></li><li class=\"\"><a href=\"/category/tags/213\">耽美爱情</a></li><li class=\"\"><a href=\"/category/tags/215\">爆笑</a></li><li class=\"\"><a href=\"/category/tags/216\">生活</a></li><li class=\"\"><a href=\"/category/tags/221\">修真</a></li><li class=\"\"><a href=\"/category/tags/218\">神魔</a></li><li class=\"\"><a href=\"/category/tags/219\">日常</a></li><li class=\"\"><a href=\"/category/tags/220\">总裁</a></li><li class=\"\"><a href=\"/category/tags/217\">灵异</a></li><li class=\"\"><a href=\"/category/tags/222\">精品</a></li><li class=\"\"><a href=\"/category/tags/223\">战斗</a></li><li class=\"\"><a href=\"/category/tags/224\">漫改</a></li><li class=\"\"><a href=\"/category/tags/225\">新作</a></li><li class=\"\"><a href=\"/category/tags/226\">神仙</a></li><li class=\"\"><a href=\"/category/tags/227\">改编</a></li><li class=\"\"><a href=\"/category/tags/228\">校园</a></li><li class=\"\"><a href=\"/category/tags/229\">治愈</a></li><li class=\"\"><a href=\"/category/tags/230\">霸总</a></li><li class=\"\"><a href=\"/category/tags/231\">爆更</a></li><li class=\"\"><a href=\"/category/tags/470\">科技n</a></li><li class=\"\"><a href=\"/category/tags/471\">格鬥</a></li><li class=\"\"><a href=\"/category/tags/472\">熱血</a></li><li class=\"\"><a href=\"/category/tags/473\">戀愛</a></li><li class=\"\"><a href=\"/category/tags/474\">少年漫画</a></li><li class=\"\"><a href=\"/category/tags/475\">少女漫画</a></li><li class=\"\"><a href=\"/category/tags/476\">青年漫画</a></li><li class=\"\"><a href=\"/category/tags/477\">儿童漫画</a></li><li class=\"\"><a href=\"/category/tags/499\">冒险热血</a></li><li class=\"\"><a href=\"/category/tags/500\">耽美爱情</a></li><li class=\"\"><a href=\"/category/tags/501\">悬疑</a></li><li class=\"\"><a href=\"/category/tags/502\">武侠</a></li><li class=\"\"><a href=\"/category/tags/504\">玄幻科幻</a></li><li class=\"\"><a href=\"/category/tags/505\">生活</a></li><li class=\"\"><a href=\"/category/tags/506\">其他</a></li><li class=\"\"><a href=\"/category/tags/507\">武侠格斗</a></li><li class=\"\"><a href=\"/category/tags/508\">侦探推理</a></li><li class=\"\"><a href=\"/category/tags/509\">生活漫画</a></li><li class=\"\"><a href=\"/category/tags/510\">游戏竞技</a></li><li class=\"\"><a href=\"/category/tags/511\">惊悚</a></li><li class=\"\"><a href=\"/category/tags/512\">现代</a></li><li class=\"\"><a href=\"/category/tags/513\">西幻</a></li><li class=\"\"><a href=\"/category/tags/514\">純愛</a></li><li class=\"\"><a href=\"/category/tags/515\">後宮</a></li><li class=\"\"><a href=\"/category/tags/516\">劇情</a></li><li class=\"\"><a href=\"/category/tags/517\">喜剧</a></li><li class=\"\"><a href=\"/category/tags/518\">日常</a></li><li class=\"\"><a href=\"/category/tags/519\">亲情</a></li><li class=\"\"><a href=\"/category/tags/520\">养成</a></li><li class=\"\"><a href=\"/category/tags/521\">欢喜</a></li><li class=\"\"><a href=\"/category/tags/522\">北欧</a></li><li class=\"\"><a href=\"/category/tags/523\">彩虹</a></li><li class=\"\"><a href=\"/category/tags/524\">腹黑</a></li><li class=\"\"><a href=\"/category/tags/525\">古装</a></li><li class=\"\"><a href=\"/category/tags/526\">美少女</a></li><li class=\"\"><a href=\"/category/tags/527\">连载中</a></li><li class=\"\"><a href=\"/category/tags/528\">韩国</a></li><li class=\"\"><a href=\"/category/tags/529\">咚漫</a></li><li class=\"\"><a href=\"/category/tags/531\">原创</a></li><li class=\"\"><a href=\"/category/tags/532\">异形</a></li><li class=\"\"><a href=\"/category/tags/533\">偶像</a></li><li class=\"\"><a href=\"/category/tags/534\">歌舞</a></li><li class=\"\"><a href=\"/category/tags/535\">悬疑灵异</a></li><li class=\"\"><a href=\"/category/tags/536\">宅斗</a></li><li class=\"\"><a href=\"/category/tags/537\">武侠仙侠</a></li><li class=\"\"><a href=\"/category/tags/538\">宅向</a></li><li class=\"\"><a href=\"/category/tags/539\">青春</a></li><li class=\"\"><a href=\"/category/tags/540\">西幻</a></li><li class=\"\"><a href=\"/category/tags/541\">冒险</a></li><li class=\"\"><a href=\"/category/tags/542\">恋爱</a></li><li class=\"\"><a href=\"/category/tags/543\">都市</a></li><li class=\"\"><a href=\"/category/tags/544\">其它</a></li><li class=\"\"><a href=\"/category/tags/545\">战斗</a></li><li class=\"\"><a href=\"/category/tags/546\">其他</a></li><li class=\"\"><a href=\"/category/tags/547\">灵异</a></li><li class=\"\"><a href=\"/category/tags/548\">科幻</a></li><li class=\"\"><a href=\"/category/tags/549\">纯爱</a></li><li class=\"\"><a href=\"/category/tags/550\">现代</a></li><li class=\"\"><a href=\"/category/tags/551\">总裁</a></li><li class=\"\"><a href=\"/category/tags/552\">推理</a></li><li class=\"\"><a href=\"/category/tags/553\">职场</a></li><li class=\"\"><a href=\"/category/tags/554\">剧情</a></li><li class=\"\"><a href=\"/category/tags/555\">校园</a></li><li class=\"\"><a href=\"/category/tags/556\">穿越</a></li><li class=\"\"><a href=\"/category/tags/557\">逆袭</a></li><li class=\"\"><a href=\"/category/tags/558\">古风</a></li><li class=\"\"><a href=\"/category/tags/559\">玄幻</a></li><li class=\"\"><a href=\"/category/tags/560\">热血</a></li><li class=\"\"><a href=\"/category/tags/561\">权谋</a></li><li class=\"\"><a href=\"/category/tags/562\">正能量</a></li><li class=\"\"><a href=\"/category/tags/563\">复仇</a></li><li class=\"\"><a href=\"/category/tags/564\">悬疑</a></li><li class=\"\"><a href=\"/category/tags/565\">奇幻</a></li><li class=\"\"><a href=\"/category/tags/566\">搞笑</a></li><li class=\"\"><a href=\"/category/tags/567\">日常</a></li><li class=\"\"><a href=\"/category/tags/568\">大女主</a></li><li class=\"\"><a href=\"/category/tags/569\">亲情</a></li><li class=\"\"><a href=\"/category/tags/570\">战争</a></li><li class=\"\"><a href=\"/category/tags/571\">脑洞</a></li><li class=\"\"><a href=\"/category/tags/572\">社会</a></li><li class=\"\"><a href=\"/category/tags/573\">重生</a></li><li class=\"\"><a href=\"/category/tags/574\">怪物</a></li><li class=\"\"><a href=\"/category/tags/575\">女神</a></li><li class=\"\"><a href=\"/category/tags/576\">多世界</a></li><li class=\"\"><a href=\"/category/tags/577\">异能</a></li><li class=\"\"><a href=\"/category/tags/578\">治愈</a></li><li class=\"\"><a href=\"/category/tags/579\">浪漫</a></li><li class=\"\"><a href=\"/category/tags/580\">魔幻</a></li><li class=\"\"><a href=\"/category/tags/581\">惊悚</a></li><li class=\"\"><a href=\"/category/tags/582\">妖怪</a></li><li class=\"\"><a href=\"/category/tags/583\">励志</a></li><li class=\"\"><a href=\"/category/tags/584\">美食</a></li><li class=\"\"><a href=\"/category/tags/585\">暗黑</a></li><li class=\"\"><a href=\"/category/tags/586\">系统</a></li><li class=\"\"><a href=\"/category/tags/587\">恶搞</a></li><li class=\"\"><a href=\"/category/tags/588\">机甲</a></li><li class=\"\"><a href=\"/category/tags/589\">猎奇</a></li><li class=\"\"><a href=\"/category/tags/590\">武侠</a></li><li class=\"\"><a href=\"/category/tags/591\">格斗</a></li><li class=\"\"><a href=\"/category/tags/592\">虐心</a></li><li class=\"\"><a href=\"/category/tags/593\">精品</a></li><li class=\"\"><a href=\"/category/tags/594\">娱乐圈</a></li><li class=\"\"><a href=\"/category/tags/595\">生活</a></li><li class=\"\"><a href=\"/category/tags/596\">高甜</a></li><li class=\"\"><a href=\"/category/tags/597\">神仙</a></li><li class=\"\"><a href=\"/category/tags/598\">修仙</a></li><li class=\"\"><a href=\"/category/tags/599\">新作</a></li><li class=\"\"><a href=\"/category/tags/600\">末日</a></li><li class=\"\"><a href=\"/category/tags/601\">宫斗</a></li><li class=\"\"><a href=\"/category/tags/602\">后宫</a></li><li class=\"\"><a href=\"/category/tags/603\">游戏竞技</a></li><li class=\"\"><a href=\"/category/tags/604\">豪快</a></li><li class=\"\"><a href=\"/category/tags/605\">神魔</a></li><li class=\"\"><a href=\"/category/tags/606\">萌系</a></li><li class=\"\"><a href=\"/category/tags/607\">架空世界</a></li><li class=\"\"><a href=\"/category/tags/608\">图谱</a></li><li class=\"\"><a href=\"/category/tags/609\">历史</a></li><li class=\"\"><a href=\"/category/tags/610\">霸总</a></li><li class=\"\"><a href=\"/category/tags/611\">电竞</a></li><li class=\"\"><a href=\"/category/tags/612\">游戏</a></li><li class=\"\"><a href=\"/category/tags/613\">虚拟世界</a></li><li class=\"\"><a href=\"/category/tags/614\">少年</a></li><li class=\"\"><a href=\"/category/tags/615\">烧脑</a></li><li class=\"\"><a href=\"/category/tags/616\">体育</a></li><li class=\"\"><a href=\"/category/tags/617\">修真</a></li><li class=\"\"><a href=\"/category/tags/618\">动作</a></li><li class=\"\"><a href=\"/category/tags/619\">性转</a></li><li class=\"\"><a href=\"/category/tags/620\">吸血</a></li><li class=\"\"><a href=\"/category/tags/621\">宠物</a></li><li class=\"\"><a href=\"/category/tags/622\">神豪</a></li><li class=\"\"><a href=\"/category/tags/623\">漫改</a></li><li class=\"\"><a href=\"/category/tags/624\">竞技</a></li><li class=\"\"><a href=\"/category/tags/625\">御姐</a></li><li class=\"\"><a href=\"/category/tags/626\">日更</a></li><li class=\"\"><a href=\"/category/tags/627\">运动</a></li><li class=\"\"><a href=\"/category/tags/628\">丧尸</a></li><li class=\"\"><a href=\"/category/tags/629\">家庭</a></li><li class=\"\"><a href=\"/category/tags/630\">架空</a></li><li class=\"\"><a href=\"/category/tags/631\">萝莉</a></li><li class=\"\"><a href=\"/category/tags/632\">宫廷东方</a></li><li class=\"\"><a href=\"/category/tags/633\">江湖</a></li><li class=\"\"><a href=\"/category/tags/634\">真人</a></li><li class=\"\"><a href=\"/category/tags/635\">异世界</a></li><li class=\"\"><a href=\"/category/tags/636\">小说改编</a></li><li class=\"\"><a href=\"/category/tags/637\">防疫</a></li><li class=\"\"><a href=\"/category/tags/638\">氪金</a></li><li class=\"\"><a href=\"/category/tags/639\">唯美</a></li><li class=\"\"><a href=\"/category/tags/640\">宫廷西方</a></li><li class=\"\"><a href=\"/category/tags/641\">名著</a></li><li class=\"\"><a href=\"/category/tags/642\">撒糖</a></li><li class=\"\"><a href=\"/category/tags/643\">穿越女神</a></li><li class=\"\"><a href=\"/category/tags/644\">快穿</a></li><li class=\"\"><a href=\"/category/tags/645\">国漫</a></li><li class=\"\"><a href=\"/category/tags/646\">古风神仙</a></li><li class=\"\"><a href=\"/category/tags/647\">机战</a></li><li class=\"\"><a href=\"/category/tags/648\">反套路</a></li><li class=\"\"><a href=\"/category/tags/649\">古风机甲</a></li><li class=\"\"><a href=\"/category/tags/650\">战斗怪物</a></li><li class=\"\"><a href=\"/category/tags/651\">诡异</a></li><li class=\"\"><a href=\"/category/tags/652\">府邸</a></li><li class=\"\"><a href=\"/category/tags/653\">氪金迪化</a></li><li class=\"\"><a href=\"/category/tags/654\">重生复仇</a></li><li class=\"\"><a href=\"/category/tags/655\">金手指</a></li><li class=\"\"><a href=\"/category/tags/656\">现言</a></li><li class=\"\"><a href=\"/category/tags/657\">穿越重生</a></li><li class=\"\"><a href=\"/category/tags/658\">恐怖</a></li><li class=\"\"><a href=\"/category/tags/659\">穿越系统</a></li><li class=\"\"><a href=\"/category/tags/660\">系统末日</a></li><li class=\"\"><a href=\"/category/tags/661\">系统娱乐圈</a></li><li class=\"\"><a href=\"/category/tags/662\">萌娃</a></li><li class=\"\"><a href=\"/category/tags/663\">穿书</a></li><li class=\"\"><a href=\"/category/tags/664\">幻想，言情</a></li><li class=\"\"><a href=\"/category/tags/665\">强强</a></li><li class=\"\"><a href=\"/category/tags/666\">宠兽</a></li><li class=\"\"><a href=\"/category/tags/667\">言情</a></li><li class=\"\"><a href=\"/category/tags/668\">玄幻，言情</a></li><li class=\"\"><a href=\"/category/tags/669\">古言</a></li><li class=\"\"><a href=\"/category/tags/670\">穿越神仙</a></li><li class=\"\"><a href=\"/category/tags/671\">迪化</a></li><li class=\"\"><a href=\"/category/tags/672\">无节操</a></li><li class=\"\"><a href=\"/category/tags/673\">系统女神</a></li><li class=\"\"><a href=\"/category/tags/674\">古风大女主</a></li><li class=\"\"><a href=\"/category/tags/675\">大女主女神</a></li><li class=\"\"><a href=\"/category/tags/676\">宫斗宅斗</a></li><li class=\"\"><a href=\"/category/tags/677\">种田</a></li><li class=\"\"><a href=\"/category/tags/678\">无敌流</a></li><li class=\"\"><a href=\"/category/tags/679\">仙侠</a></li><li class=\"\"><a href=\"/category/tags/680\">古言，脑洞，</a></li><li class=\"\"><a href=\"/category/tags/681\">少女</a></li><li class=\"\"><a href=\"/category/tags/682\">校园日常</a></li><li class=\"\"><a href=\"/category/tags/683\">百合</a></li><li class=\"\"><a href=\"/category/tags/684\">爆笑</a></li><li class=\"\"><a href=\"/category/tags/685\">克苏鲁</a></li><li class=\"\"><a href=\"/category/tags/686\">双男主</a></li><li class=\"\"><a href=\"/category/tags/687\">战斗热血</a></li><li class=\"\"><a href=\"/category/tags/688\">玄幻脑洞</a></li><li class=\"\"><a href=\"/category/tags/689\">游戏体育</a></li><li class=\"\"><a href=\"/category/tags/690\">畅销</a></li><li class=\"\"><a href=\"/category/tags/691\">直播</a></li><li class=\"\"><a href=\"/category/tags/692\">悬疑，灵异</a></li><li class=\"\"><a href=\"/category/tags/693\">古装</a></li><li class=\"\"><a href=\"/category/tags/694\">权谋修仙</a></li><li class=\"\"><a href=\"/category/tags/695\">悬疑，脑洞</a></li><li class=\"\"><a href=\"/category/tags/696\">都市脑洞</a></li><li class=\"\"><a href=\"/category/tags/697\">大女主复仇</a></li><li class=\"\"><a href=\"/category/tags/698\">团宠</a></li><li class=\"\"><a href=\"/category/tags/699\">非人类</a></li></ul></div><div class=\"cy_tag hide\"><span>类型</span><ul><li class=\"\"><a href=\"/category/theme/32\">故事漫画</a></li><li class=\"\"><a href=\"/category/theme/33\">轻小说</a></li><li class=\"\"><a href=\"/category/theme/34\">四格</a></li><li class=\"\"><a href=\"/category/theme/35\">绘本</a></li><li class=\"\"><a href=\"/category/theme/36\">单幅</a></li><li class=\"\"><a href=\"/category/theme/37\">条漫</a></li></ul></div><div class=\"cy_tag hide\"><span>品质</span><ul><li class=\"\"><a href=\"/category/quality/38\">签约</a></li><li class=\"\"><a href=\"/category/quality/39\">精品</a></li><li class=\"\"><a href=\"/category/quality/40\">热门</a></li><li class=\"\"><a href=\"/category/quality/41\">新手</a></li></ul></div><div class=\"cy_tag hide\"><span>地区</span><ul><li class=\"\"><a href=\"/category/city/42\">内地</a></li><li class=\"\"><a href=\"/category/city/43\">港台</a></li><li class=\"\"><a href=\"/category/city/44\">韩国</a></li><li class=\"\"><a href=\"/category/city/45\">日本</a></li><li class=\"\"><a href=\"/category/city/134\">欧美</a></li></ul></div><div class=\"cy_tag hide\"><span>版权</span><ul><li class=\"\"><a href=\"/category/copyright/46\">首发</a></li><li class=\"\"><a href=\"/category/copyright/47\">独家</a></li></ul></div><div class=\"cy_tag\"><span>资费</span><ul><li class=\"\"><a href=\"/category/pay/1\">免费</a></li><li class=\"\"><a href=\"/category/pay/2\">付费</a></li><li class=\"\"><a href=\"/category/pay/3\">VIP</a></li></ul></div><div class=\"cy_tag\"><span>进度</span><ul><li class=\"\"><a href=\"/category/finish/1\">连载</a></li><li class=\"\"><a href=\"/category/finish/2\">完结</a></li></ul></div></div>";
        Map<String, String> map = new LinkedHashMap<>();
        JsoupNode node = new JsoupNode(html);
        Elements elements = node.getElements("a");
        for (Element element : elements) {
            node.init(element);
            map.put(node.ownText("a"), getIndex() + node.href("a") + "/page/1");
        }
        return map;
    }

    @Override
    public List<ComicInfo> getRankInfoList(String html) {
        return getInfoList(html);
    }
}
