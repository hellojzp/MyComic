package top.luqichuang.common.en;

import java.util.LinkedHashMap;
import java.util.Map;

import top.luqichuang.common.model.EntityInfo;
import top.luqichuang.common.model.Source;
import top.luqichuang.mycomic.source.AiYouMan;
import top.luqichuang.mycomic.source.BL;
import top.luqichuang.mycomic.source.BaiManWu;
import top.luqichuang.mycomic.source.BaoZi;
import top.luqichuang.mycomic.source.BiliBili;
import top.luqichuang.mycomic.source.Cui;
import top.luqichuang.mycomic.source.DaShu;
import top.luqichuang.mycomic.source.Du;
import top.luqichuang.mycomic.source.DuShi;
import top.luqichuang.mycomic.source.GoDa;
import top.luqichuang.mycomic.source.HaoMan6;
import top.luqichuang.mycomic.source.KaiXin;
import top.luqichuang.mycomic.source.KuManWu;
import top.luqichuang.mycomic.source.Lai;
import top.luqichuang.mycomic.source.MH118;
import top.luqichuang.mycomic.source.MH118W;
import top.luqichuang.mycomic.source.MH1234;
import top.luqichuang.mycomic.source.MH160;
import top.luqichuang.mycomic.source.MH6;
import top.luqichuang.mycomic.source.ManHuaFen;
import top.luqichuang.mycomic.source.ManHuaTai;
import top.luqichuang.mycomic.source.MiTui;
import top.luqichuang.mycomic.source.OH;
import top.luqichuang.mycomic.source.PinYue;
import top.luqichuang.mycomic.source.PuFei;
import top.luqichuang.mycomic.source.QiMiao;
import top.luqichuang.mycomic.source.QiXi;
import top.luqichuang.mycomic.source.QianWei;
import top.luqichuang.mycomic.source.SiSi;
import top.luqichuang.mycomic.source.TengXun;

/**
 * @author LuQiChuang
 * @desc
 * @date 2021/1/11 23:51
 * @ver 1.0
 */
public enum CSourceEnum {
    MI_TUI(1, "米推漫画", new MiTui()),
    MAN_HUA_FEN(2, "漫画粉", new ManHuaFen()),
    PU_FEI(3, "扑飞漫画", new PuFei()),
    TENG_XUN(4, "腾讯动漫", new TengXun()),
    BILI_BILI(5, "哔哩哔哩", new BiliBili()),
    OH(6, "OH漫画", new OH()),
    MAN_HUA_TAI(7, "漫画台", new ManHuaTai()),
    MH_118(8, "118漫画", new MH118()),
    DU(9, "独漫画", new Du()),
    BL(10, "BL漫画", new BL()),
    AI_YOU_MAN(11, "爱优漫", new AiYouMan()),
    MH_1234(12, "1234漫画", new MH1234()),
    MH_118_2(13, "118漫画[2]", new MH118W()),
    QI_MIAO(14, "奇妙漫画", new QiMiao()),
    DA_SHU(15, "大树漫画", new DaShu()),
    SI_SI(16, "思思漫画", new SiSi()),
    BAO_ZI(17, "包子漫画", new BaoZi()),
    QI_XI(18, "七夕漫画", new QiXi()),
    KU_MAN_WU(19, "酷漫屋", new KuManWu()),
    HAO_MAN_6(20, "好漫6", new HaoMan6()),
    DU_SHI(21, "都市漫画", new DuShi()),
    QIAN_WEI(22, "前未漫画", new QianWei()),
    PIN_YUE(23, "品悦漫画", new PinYue()),
    LAI(24, "来漫画", new Lai()),
    MH_6(25, "6漫画", new MH6()),
    MH_160(26, "160漫画", new MH160()),
    BAI_MAN_WU(27, "百漫屋", new BaiManWu()),
    GO_DA(28, "GoDa漫画", new GoDa()),
    KAI_XIN(29, "开心漫画", new KaiXin()),
    CUI(30, "催漫画", new Cui()),
    ;

    private static final Map<Integer, Source<? extends EntityInfo>> MAP = new LinkedHashMap<>();

    static {
        for (CSourceEnum value : values()) {
            if (value.SOURCE.isValid()) {
                MAP.put(value.ID, value.SOURCE);
            }
        }
    }

    public static Map<Integer, Source<? extends EntityInfo>> getMAP() {
        return MAP;
    }

    public final int ID;
    public final String NAME;
    public final Source<? extends EntityInfo> SOURCE;

    CSourceEnum(int id, String name, Source<? extends EntityInfo> source) {
        this.ID = id;
        this.NAME = name;
        this.SOURCE = source;
    }
}
