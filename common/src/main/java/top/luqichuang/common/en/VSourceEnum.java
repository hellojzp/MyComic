package top.luqichuang.common.en;

import java.util.LinkedHashMap;
import java.util.Map;

import top.luqichuang.common.model.EntityInfo;
import top.luqichuang.common.model.Source;
import top.luqichuang.myvideo.source.AiYun;
import top.luqichuang.myvideo.source.BiliBili;
import top.luqichuang.myvideo.source.FengChe;
import top.luqichuang.myvideo.source.FengChe2;
import top.luqichuang.myvideo.source.MiLiMiLi;
import top.luqichuang.myvideo.source.YingHua;
import top.luqichuang.myvideo.source.YingHua2;

/**
 * @author LuQiChuang
 * @desc
 * @date 2021/6/22 23:47
 * @ver 1.0
 */
public enum VSourceEnum {
    YING_HUA(1, "樱花动漫", new YingHua()),
    MILI_MILI(2, "米粒米粒", new MiLiMiLi()),
    FENG_CHE(3, "风车动漫", new FengChe()),
    YING_HUA_2(4, "樱花动漫[2]", new YingHua2()),
    BILI_BILI(5, "哔哩哔哩", new BiliBili()),
    FENG_CHE_2(6, "风车动漫[2]", new FengChe2()),
    AI_YUN(7, "爱云影视", new AiYun()),
    ;

    private static final Map<Integer, Source<? extends EntityInfo>> MAP = new LinkedHashMap<>();

    static {
        for (VSourceEnum value : values()) {
            if (value.SOURCE.isValid()) {
                MAP.put(value.ID, value.SOURCE);
            }
        }
    }

    public static Map<Integer, Source<? extends EntityInfo>> getMAP() {
        return MAP;
    }

    public final int ID;
    public final String NAME;
    public final Source<? extends EntityInfo> SOURCE;

    VSourceEnum(int id, String name, Source<? extends EntityInfo> source) {
        this.ID = id;
        this.NAME = name;
        this.SOURCE = source;
    }
}
