package top.luqichuang.common.model;

import java.util.List;
import java.util.Map;

import okhttp3.Request;
import top.luqichuang.common.util.NetUtil;

/**
 * @author LuQiChuang
 * @desc
 * @date 2021/6/10 14:54
 * @ver 1.0
 */
public interface Source<T extends EntityInfo> {

    //代表数据源不同页面
    String SEARCH = "search";       //搜索页面
    String DETAIL = "detail";       //详情页面
    String CONTENT = "content";     //阅读页面
    String RANK = "rank";           //排行榜页面

    //SourceType
    int COMIC = 0;
    int NOVEL = 1;
    int VIDEO = 2;

    /**
     * 获取数据源类型
     *
     * @return int
     */
    int getSourceType();

    /**
     * 获取资源id
     *
     * @return int
     */
    int getSourceId();

    /**
     * 获取资源名称
     *
     * @return String
     */
    String getSourceName();

    /**
     * 获取主页网址
     *
     * @return String
     */
    String getIndex();

    /**
     * 获取数据源是否有效
     * 默认true为有效 失效返回false
     *
     * @return boolean
     */
    default boolean isValid() {
        return true;
    }

    /**
     * 设置网页编码方式
     * 默认为utf8 根据具体网页编码更改
     *
     * @param tag 不同页面 SEARCH、DETAIL、CONTENT、RANK
     * @return String 页面编码
     */
    default String getCharsetName(String tag) {
        return "UTF-8";
    }

    /**
     * 返回搜索页面的request
     *
     * @param searchString 搜索关键词
     * @return Request
     */
    Request getSearchRequest(String searchString);

    /**
     * 返回详情页的request
     *
     * @param detailUrl 详情页url
     * @return Request
     */
    default Request getDetailRequest(String detailUrl) {
        return NetUtil.getRequest(detailUrl);
    }

    /**
     * 返回阅读页的request
     *
     * @param contentUrl 阅读页request
     * @return Request
     */
    default Request getContentRequest(String contentUrl) {
        return NetUtil.getRequest(contentUrl);
    }

    /**
     * 返回排行榜页面的request
     *
     * @param rankUrl 排行榜页面request
     * @return Request
     */
    default Request getRankRequest(String rankUrl) {
        return NetUtil.getRequest(rankUrl);
    }

    /**
     * 为应对某些多次访问才可以获取数据的网页
     * 调用时间在获取request(getSearchRequest...)之前
     *
     * @param html 获取的网页源码
     * @param tag  访问页面标识(SEARCH DETAIL...)
     * @param data 访问页面后存留的信息
     * @param map  给后续调用保存的主要信息
     * @return Request
     */
    default Request buildRequest(String html, String tag, Map<String, Object> data, Map<String, Object> map) {
        return null;
    }

    /**
     * 解析searchRequest得到的搜索页面
     * 返回获取的数据源链表
     * 用于软件搜索页面
     *
     * @param html 搜索页面源码
     * @return List<T> 数据源链表
     */
    List<T> getInfoList(String html);

    /**
     * 解析detailRequest得到的详情页面
     * 将解析的数据填充在info中
     *
     * @param info 根据搜索得到的初步info，本方法继续填充信息
     * @param html 详情页面源码
     * @param map  保存需要多次访问页面得到的信息
     * @return void
     */
    void setInfoDetail(T info, String html, Map<String, Object> map);

    /**
     * 解析contentRequest得到的阅读页面
     * 返回阅读内容链表
     *
     * @param html      阅读页面源码
     * @param chapterId 章节id
     * @param map       保存需要多次访问页面得到的信息
     * @return List<Content>
     */
    List<Content> getContentList(String html, int chapterId, Map<String, Object> map);

    /**
     * 返回排行榜页面的对应的 名称--url 的map
     * 可手动添加
     *
     * @return Map<String, String>
     */
    Map<String, String> getRankMap();

    /**
     * 解析rankRequest得到的排行页面
     * 返回搜索到的数据链表
     *
     * @param html html
     * @return List<T>
     */
    List<T> getRankInfoList(String html);

    /**
     * 返回某些数据源解析需要用到的imageHeaders
     *
     * @return Map<String, String>
     */
    Map<String, String> getImageHeaders();
}
